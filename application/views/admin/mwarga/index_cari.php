<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>				
				<a href="{base_url}admin/mwarga" class="btn" data-toggle="tooltip" title="" data-original-title="Back"> <i class="fa fa-refresh"></i></a>
			</li>
			
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
				<?php echo form_open('admin/mwarga/filter','class="form-horizontal" id="form-work"') ?>
		<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">NIK,NAMA,NO.KK</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="nama" name="nama" value="{nama}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Kepala Keluarga</label>
						<div class="col-md-8">
							<select id="status_kk" name="status_kk" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($status_kk == 999 ? 'selected':'')?>>Semua Warga</option>
								<option value="1" <?=($status_kk == '1' ? 'selected' : '')?>>Kepala Keluarga</option>
								<option value="0" <?=($status_kk == '2' ? 'selected' : '')?>>Bukan Kepala Keluarga</option>
							</select>
						</div>
					</div>
					<div class="form-group"  style="margin-bottom: 5px;">
						  <label class="col-md-4 control-label" for="example-daterange1">Range Umur</label>
						  <div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd/mm/yyyy">
								<input class="form-control number" type="text" id="tanggaldari" name="tanggaldari" placeholder="From" value="{tanggaldari}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control number" type="text" id="tanggalsampai" name="tanggalsampai" placeholder="To" value="{tanggalsampai}">
							</div>
						  </div>
					</div>
					<div class="form-group"  style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Domisili</label>
						<div class="col-md-8">
							<select id="status_warga_id" name="status_warga_id" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Pekerjaan">
								<option value="999" <?=($status_warga_id == '999' ? '':'')?>>Semua Warga</option>
								<?foreach($list_status as $row){?>
									<option value="<?=$row->id?>" <?=($status_warga_id == $row->id ? 'selected':'')?>><?=$row->status_warga?></option>
								<?}?>
							</select>
						</div>
					</div>
					
			</div>
			<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Alamat</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="alamat" name="alamat" value="{alamat}">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Jenis Kelamin</label>
						<div class="col-md-8">
							<select id="jenis_kelamin" name="jenis_kelamin" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($jenis_kelamin == '999' ? 'selected':'')?>>Semua Jenis Kelamin</option>
								<option value="Laki-Laki" <?=($jenis_kelamin == 'Laki-Laki' ? 'selected' : '')?>>Laki-Laki</option>
								<option value="Perempuan" <?=($jenis_kelamin == 'Perempuan' ? 'selected' : '')?>>Perempuan</option>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 10px;">
						<label class="col-md-4 control-label" for="">Pekerjaan</label>
						<div class="col-md-8">
							<select id="idpekerjaan" name="idpekerjaan" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Pekerjaan">
								<option value="999" <?=($idpekerjaan == '999' ? '':'')?>>SEMUA PEKERJAAN</option>
								<?foreach($list_pekerjaan as $row){?>
									<option value="<?=$row->id?>" <?=($idpekerjaan == $row->id ? 'selected':'')?>><?=$row->pekerjaan?></option>
								<?}?>
							</select>
						</div>
					</div>
			  
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-4">
							<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;" value="1"><i class="fa fa-filter"></i> Filter</button>
						</div><div class="col-md-4">
							<button class="btn btn-info text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;" value="2"><i class="fa fa-download"></i> Download</button>
						</div>
					</div>
				<?php echo form_close() ?>
			</div>
		</div>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">#</th>
						<th width="10%">No KK</th>
						<th width="10%">NIK</th>
						<th width="20%">Nama</th>
						<th width="30%">Alamat</th>
						<th width="10%">Status</th>
						<th width="10%">Lahir / Umur</th>					
					</tr>
				</thead>
				<tbody>
					<?
					$i=$this->uri->segment(4);
					foreach($list_warga as $row){
						$i=$i+1;
						?>
						<tr>
						<td><?=$i?></td>
						<td><?=$row->nokk?></td>
						<td><?=$row->nik?></td>
						<td><?=$row->nama.' ('.substr($row->jenis_kelamin,0,1).')'?></td>
						<td><?=$row->alamat_lengkap?></td>
						<td><?=$row->domisili?></td>
						<td><?=human_date_short($row->tgl_lahir).'<br>'.number_format($row->umur,1).' Th'?></td>
						</tr>
					<?}?>
				</tbody>
			</table>
			<? if ($list_warga){?>
			<div class="text-left">
				{pagination}
			</div>
			<?}?>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>


