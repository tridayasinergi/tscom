<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/menu" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/menu/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="parent_id">Parent <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select class="form-control js-select2" id="parent_id" name="parent_id" size="1">
						<option value="" <?=($parent_id=='0'?'selected':'')?>>-TOP MENU-</option>
						<?foreach($list_parent as $row){?>
						<option value="<?=$row->id?>" <?=($parent_id==$row->id?'selected':'')?>><?=TreeView($row->level, $row->menu);?></option>
						<?}?>
					</select>
				</div>							
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="menu">Menu<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="menu" id="menu" placeholder="" value="{menu}" />
					
				</div>	
				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="menu">URL<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="url" id="url" placeholder="" value="{url}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="urutan">Urutan Tampil<span class="text-danger">*</span></label>
				<div class="col-md-2">
					<input type="text" class="form-control input-sm" name="urutan" id="urutan" placeholder="" value="{urutan}" />
					
				</div>	
				
			</div>
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/menu" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
$('#parent_id').select2();
</script>
