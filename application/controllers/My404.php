<?php

class My404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		// print_r('SII');exit();
        $this->output->set_status_header('404');
        $data['content'] = 'page404'; // View name
        $this->load->view('page404');//loading in my template
    }
 }


/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
?>