<?php

class Read extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('page_model','page_model');
	}
	function index($id=0)
	{
		$row = $this->page_model->get_news($id);
		if(isset($row->id)){
			$data = array(
					'id' => $row->id,
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					'updated_at' => $row->updated_at,
					'img' => $row->img,
					);	
			
			$data['news_list'] =$this->page_model->news_list();			
			$data['kamar_list'] =$this->page_model->kamar_list();		
			$data['first_title'] = 'Page Detail';		
			$data['second_title'] = 'Page';		
			$data['template'] = 'news_detail';			
		
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		}else{
			$this->session->set_flashdata('error',true);
			$this->session->set_flashdata('message_flash','Data Tidak Ditemukan.');
			redirect('berita','location');
		}
	}	
	function detail($id){
		
	}
	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */