<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/mwarga/add_surat" class="btn" data-toggle="tooltip" title="" data-original-title=""> <i class="fa fa-plus"></i></a>
			</li>
			<li>
				<a href="{base_url}admin/mwarga/surat" class="btn"><i class="fa fa-reply"></i></a>
			</li>			
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('admin/mwarga/simpan_surat',array('id'=>'form1','method'=>'post','class'=>'js-validation-bootstrap  form-horizontal push-10-t')); ?>
			<div class="form-group">
				
				<label class="col-md-2 control-label" for="kode_surat">Jenis Surat </label>
				<div class="col-md-4">
					<select name="kode_surat" tabindex="1" id="kode_surat" data-placeholder="Pekerjaan " class="form-control  input-sm">
						<option value="" <?=($kode_surat==''?'selected':'')?>>- Jenis Surat -</option>
						<?foreach($list_kode_surat as $row){?>
						<option value="<?=$row->kode_surat?>" <?=($kode_surat==$row->kode_surat?'selected':'')?>><?=$row->nama_surat?></option>
						<?}?>
					</select>
				</div>
				<div class="col-md-4">
					<input type="text" readonly class="form-control input-sm" name="ket_surat" id="ket_surat" placeholder="" value="{ket_surat}" />					
					<input type="hidden" class="form-control input-sm" name="no_urut" id="no_urut" placeholder="" value="{no_urut}" />					
					<input type="hidden" class="form-control input-sm" name="tahun" id="tahun" placeholder="" value="{tahun}" />					
					<input type="hidden" class="form-control input-sm" name="bulan" id="bulan" placeholder="" value="{bulan}" />					
					<input type="hidden" class="form-control input-sm" name="rw" id="rw" placeholder="" value="{rw}" />					
					<input type="hidden" class="form-control input-sm" name="rt" id="rt" placeholder="" value="{rt}" />					
					<input type="hidden" class="form-control input-sm" name="kecamatan" id="kecamatan" placeholder="" value="{kecamatan}" />					
					<input type="hidden" class="form-control input-sm" name="desa" id="desa" placeholder="" value="{desa}" />					
					<input type="hidden" class="form-control input-sm" name="nama_rt" id="nama_rt" placeholder="" value="{nama_rt}" />					
					<input type="hidden" class="form-control input-sm" name="nama_rw" id="nama_rw" placeholder="" value="{nama_rw}" />					
					<input type="hidden" class="form-control input-sm" name="idrt" id="idrt" placeholder="" value="{idrt}" />					
					<input type="hidden" class="form-control input-sm" name="idrw" id="idrw" placeholder="" value="{idrw}" />					
					<input type="hidden" class="form-control input-sm" name="nik" id="nik" placeholder="" value="{nik}" />					
					<input type="hidden" class="form-control input-sm" name="nama" id="nama" placeholder="" value="{nama}" />					
					<input type="hidden" class="form-control input-sm" name="ttd_rt" id="ttd_rt" placeholder="" value="{ttd_rt}" />					
					<input type="hidden" class="form-control input-sm" name="ttd_rw" id="ttd_rw" placeholder="" value="{ttd_rw}" />				
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="idwarga">Nama Warga <span class="text-danger">*</span></label>
				<div class="col-md-8">
					<select name="idwarga"  id="idwarga" tabindex="1" data-placeholder="Identitas Warga " class="form-control  input-sm">
						<? if($idwarga){?>
							<option value="{idwarga}"><?=$nik.' - '.$nama.'.'?></option>
						<?}else{?>
							<option value=""></option>
						<?}?>
					</select>
				</div>				
			</div>
			
			<div class="form-group">
					
				<label class="col-md-2 control-label" for="jenis_kelamin">No KK<span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" readonly class="form-control input-sm" name="nokk" id="nokk" placeholder="" value="{nokk}" />
				</div>
				<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="jenis_kelamin" name="jenis_kelamin" size="1">
						<option value="" <?=($jenis_kelamin==''?'selected':'')?>>- Jenis Kelamin -</option>
						<option value="Laki-Laki" <?=($jenis_kelamin=='Laki-Laki'?'selected':'')?>>Laki-Laki</option>
						<option value="Perempuan" <?=($jenis_kelamin=='Perempuan'?'selected':'')?>>Perempuan</option>
						
					</select>
				</div>
				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="tempat_lahir">Tempat Lahir <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="tempat_lahir" id="tempat_lahir" placeholder="" value="{tempat_lahir}" />					
				</div>	
				<label class="col-md-2 control-label" for="tgl_lahir">Tgl Lahir <span class="text-danger">*</span></label>				
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="tgl_lahir" id="tgl_lahir" placeholder="" value="{tgl_lahir}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idagama">Agama <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select name="idagama" tabindex="1" id="idagama" data-placeholder="Agama " class="form-control  input-sm">
						<option value="" <?=($idagama==''?'selected':'')?>>- Agama -</option>
						<?foreach($list_agama as $row){?>
						<option value="<?=$row->id?>" <?=($idagama==$row->id?'selected':'')?>><?=$row->agama?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="warga_negara">Kewarganegaraan <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="warga_negara" name="warga_negara" size="1">
						<option value="" <?=($warga_negara==''?'selected':'')?>>-Belum diisi-</option>
						<option value="WNI" <?=($warga_negara=='WNI'?'selected':'')?>>WNI</option>
						<option value="WNA" <?=($warga_negara=='WNA'?'selected':'')?>>WNA</option>
					</select>
				</div>			
			</div>
			
			<div class="form-group">
				
				<label class="col-md-2 control-label" for="idpekerjaan">Pekerjaan </label>
				<div class="col-md-4">
					<select name="idpekerjaan" tabindex="1" id="idpekerjaan" data-placeholder="Pekerjaan " class="form-control  input-sm">
						<option value="" <?=($idpekerjaan==''?'selected':'')?>>- Pekerjaan -</option>
						<?foreach($list_pekerjaan as $row){?>
						<option value="<?=$row->id?>" <?=($idpekerjaan==$row->id?'selected':'')?>><?=$row->pekerjaan?></option>
						<?}?>
					</select>
				</div>				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat_lengkap">Alamat Lengkap <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" id="alamat_lengkap" name="alamat_lengkap" rows="3" placeholder="Alamat Lengkap..">{alamat_lengkap}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="keperluan">Keperluan<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" id="keperluan" name="keperluan" rows="3" placeholder="Keperluan">{keperluan}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="st_lanjut" id="st_lanjut" placeholder="" value="{st_lanjut}" />					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" id="btn_simpan" type="submit">Simpan</button>
					<a href="{base_url}admin/mwarga/surat" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

