<section class="section-padding blog-details">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-8">
				<div class="row">
					<div class="col-12">
						<!-- article -->
						<article class="post">
							<div class="post-wrapper">
								<div class="blog-img animate-img mb-xl-20">
									<a href="#">
										<img src="{berita_path}<?=$img_news?>" class="img-fluid image-fit" alt="{title}">
									</a>
									
									<div class="post-date">
										<a href="#"><?=tsi_date($created_at)?></a>
									</div>
									
								</div>
								<div class="blog-meta">
									
									<div class="post-meta">
										<div class="post-author mb-xl-20">
											<div class="author-img">
												<a href="blog-single.html">
													<img src="{assets_path}tsi.png" class="rounded-circle" alt="#">
												</a>
											</div>
											<span class="text-theme fs-14">By <a href="blog-single.html" class="text-theme fw-500"><?=$author_ref?></a></span>
										</div>
										<div class="post-content mb-xl-20">
											<span class="text-custom-blue fs-14"><i class="fas fa-eye mr-1"></i> <a href="#" class="text-theme"><?=$jml_klik?></a></span>
											<span class="text-custom-blue fs-14"><i class="fas fa-tags"></i><a href="#" class="post-date"> <?=$kategori_berita?></a></span>
										</div>
									</div>
									<div class="blog-title">
										<h2><a href="#" class="text-theme fw-600"><?=$title?></a></h2>
									</div>
									<div class="blog-content">
										<?=$content?>
									</div>
								</div>
							</div>
						</article>
						<!-- article -->
						<hr>
						<!-- tags & social -->
						<div class="post-details-tags-social mb-xl-20">
							<div class="row">
								<div class="col-lg-8 col-md-6">
									<div class="tags-box">
										<span class="fs-18 text-light-white"><i class="fas fa-tags"></i></span>
										<div class="tags">
											<a href="#"><?=$kategori_berita?></a>
										<?foreach ($list_tag_berita as $r){?>
											<a href="#"><?=$r->tag?></a>
										<?}?>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-6" hidden>
									<div class="social-media-box">
										<ul class="custom">
											<li><a href="" class="fb"><i class="fab fa-facebook-f"></i> </a> </li>
											<li><a href="https://wa.me/6285221927225?text=Isi Pesan" class="fb"><i class="fab fa-whatsapp"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- tags & social -->
						<!-- post author -->
						
						
						<!-- comments -->
					</div>
				</div>
					
			</div>
			<? $this->load->view('inc_right') ?>
	</div>
  </div>
</section>