<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtags extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mtags_model','mtags_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Tags';
		$data['template']    = 'admin/mtags/index_list';
		$data['ajax_url'] = 'admin/mtags/getDataList'; 
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\mtags'));
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	function get_index(){
	
		$iddistributor     		= $this->input->post('iddistributor');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *FROM mtags
			) as T ";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/mtags/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->tag;
			
			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">ACTIVE</span>';
			}else{
				$row[] = '<span class="label label-danger">DELETED</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Aktifkan</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function getDataList()
    {
        $query = $this->mtags_model->data_list(0);
        $this->output->set_output($query);
    }
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Tags - Addnew');
		$data['id'] = "";
		$data['tag'] = "";
		$data['publish'] = "0";
		$data['error'] = "";
		$data['edit'] = "0";
		
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\mtags'));
		
		$data['title'] = 'Tags - Input Baru';
		$data['template'] = 'admin/mtags/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->mtags_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/mtags/editor/$id/");				
			}
		}else{		
			if ($this->mtags_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/mtags/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'mtags/manage';
		$data['list_tipe'] = $this->mtags_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->mtags_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'mtags',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Tags",'mtags',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Tags - Edit');
			if($row = $this->mtags_model->get_mtags($id))
			{
				$data['id'] = $id;
				$data['parent_id'] = $row->parent_id;
				$data['urutan'] = $row->urutan;
				$data['url'] = $row->url;
				$data['mtags'] = $row->mtags;
				$data['edit'] = 1;
				
			}
			$data['list_parent'] = $this->mtags_model->list_parent();
		
		// print_r($data);exit();
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\mtags'));
		$data['error'] = '';
		$data['title'] = 'Tags - Edit';
		$data['template'] = 'admin/mtags/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->mtags_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/mtags");
		}
	}
}
?>