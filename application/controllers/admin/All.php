<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All extends CI_Controller {

	/**
	 * Member controller.
	 * Developer @gunalirezqimauludi
	 */

	function __construct()
	  {
			parent::__construct();
			
	  }

	function s2_client() {
		$cari 	= $this->input->post('search');
		// $cari 	= 'perni';
		
		$data = $this->all_model->s2_client($cari);
		$this->output->set_output(json_encode($data));
	}
	function s2_kota() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_kota($cari);
		$this->output->set_output(json_encode($data));
	}
	function s2_warga() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_warga($cari);
		$this->output->set_output(json_encode($data));
	}
	function find_warga() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->find_warga($cari);
		$this->output->set_output(json_encode($data));
	}
	function find_detail($id)
	{
		$arr = $this->all_model->find_detail($id);
		$this->output->set_output(json_encode($arr));
	}
	

	function find_kecamatan($idkota) {
		$arr['detail'] = $this->all_model->find_kecamatan($idkota);
		$this->output->set_output(json_encode($arr));

	}
	function find_desa($idkecamatan) {
		$arr['detail'] = $this->all_model->find_desa($idkecamatan);
		$this->output->set_output(json_encode($arr));

	}
	function find_rw($iddesa) {
		$arr['detail'] = $this->all_model->find_rw($iddesa);
		$this->output->set_output(json_encode($arr));

	}
	function find_rt($idrw) {
		$arr['detail'] = $this->all_model->find_rt($idrw);
		$this->output->set_output(json_encode($arr));

	}
	
	function s2_member() {
		$cari 	= $this->input->post('search');
		$data = $this->all_model->s2_member($cari);
		$this->output->set_output(json_encode($data));
	}
}
