<?php

class Info_model extends CI_Model {
	function __construct()
	{
		
   	}
	
	
	function add_record() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$this->title   	        = $this->input->post('title'); 
		$this->sinopsis   	    = $this->input->post('sinopsis'); 
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->publish   		= 0; 
		$this->created_by   	= $this->session->userdata('user_id'); 
		$this->author   		= $this->input->post('author'); 
		if($this->upload_img_info (false)){ 
			$this->db->insert('minfo', $this);
			return true;
		}else{
			// print_r('sini');
			return false;
		}
	}
	function upload_img_info ($update = false){
		// print_r(isset($_FILES['img_info']));exit();
		if(isset($_FILES['img_info'])){
			if($_FILES['img_info']['name'] != ''){
				// $config['upload_path'] = './assets/images/info/original/';
				$config['upload_path'] = './assets/upload/info/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img_info']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img_info')){
					$image_upload = $this->upload->data();
					$this->img_info  = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/info/'.$this->img_info );			
					$this->myimage->resize(1920,800);
					$this->myimage->save('./assets/upload/info/'.$this->img_info );
					$this->myimage->resize(798,370);
					$this->myimage->save('./assets/upload/info/thumbs/'.$this->img_info );
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				// print_r('sini');
				return true;
			}
		}else{
			
				print_r('Sahabat');exit();
			return false;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img_info );exit();
		if(file_exists('./assets/upload/info/'.$row->img_info ) && $row->img_info  !='') {
			unlink('./assets/upload/info/'.$row->img_info );
			unlink('./assets/upload/info/thumbs/'.$row->img_info );
		}
		
	}
	function detail()
    {
		// $this->db->where('id',$id);
		$query = $this->db->get('minfo');
        return $query->row_array();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$id                         = $this->input->post('id'); 
		$this->nama_perusahaan   	= $this->input->post('nama_perusahaan'); 
		$this->email   	            = $this->input->post('email'); 
		$this->telepon   	        = $this->input->post('telepon'); 
		$this->fax   		        = $this->input->post('fax');
		$this->cs1_judul   		    = $this->input->post('cs1_judul');
		$this->cs1   		        = $this->input->post('cs1');
		$this->cs2_judul   		    = $this->input->post('cs2_judul');
		$this->cs2   		        = $this->input->post('cs2');
		$this->ig_judul   		    = $this->input->post('ig_judul');
		$this->ig   		        = $this->input->post('ig');
		$this->fb   		        = $this->input->post('fb');
		$this->fb_judul   		    = $this->input->post('fb_judul');
		$this->youtube_judul   		= $this->input->post('youtube_judul');
		$this->youtube   		    = $this->input->post('youtube');
		$this->info1   		        = replace_image_url_asset(replace_p($this->input->post('info1',false)));
		$this->info2   		        = replace_image_url_asset(replace_p($this->input->post('info2',false)));
		$this->info3   		        = replace_image_url_asset(replace_p($this->input->post('info3',false)));
		// if($this->upload_img_info (true)){ 
			$this->db->where('id',$id);
			$this->db->update('minfo',$this);
			return true;
		// }else{
			// return false;
		// }
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('minfo',array('publish' => $status));
		return true;
	}
	
	
}
