<?php

class Content extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//permission_basic_admin($this->session);
		$this->load->model('content_model','content_model');
		$this->load->model('berita_model','berita_model');
		$this->load->model('kamar_detail_model');
	}
	function kontak($success = 0){
        $this->load->library('recaptcha');
		$row = $this->content_model->get_kontak();
		if(isset($row->id)){
			$data = array(
					'id'        => $row->id,
					'alamat'    => $row->alamat,
					'kota'      => $row->kota,
					'telepon'   => $row->telepon,
					'fax'       => $row->fax,
					'email'     => $row->email,
					'wa'        => $row->wa,
					'error'     => 0,
					'success'   => $success,
					'contact_name'      => '',
					'contact_phone'     => '',
					'contact_email'     => '',
					'contact_subject'   => '',
					'contact_message'   => '',
					);	
			$data['news_list'] = $this->berita_model->news_list();			
			$data['recaptcha'] = $this->recaptcha->render($this->lang->lang());
			$data['template'] = 'kontak';			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
	}
    function send_contact(){
        $this->load->library('recaptcha');
        $captcha_answer = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($captcha_answer);

        if($response['success']) {
            $this->load->library('email');
			$this->email->set_newline("\r\n");

			$this->email->from('noreply@hotelsabang.com');
			$this->email->reply_to($this->input->post('contact_email'), $this->input->post('contact_phone'));
			$this->email->to('sales@hotelsabang.com');
			$this->email->cc('arefun.safety@gmail.com');

			$this->email->subject('Contact Form');
			
			
			$data_email = array ('contact_name'      => $this->input->post('contact_name'),
                                 'contact_phone'     => $this->input->post('contact_phone'),
                                 'contact_email'     => $this->input->post('contact_email'),
                                 'contact_subject'   => $this->input->post('contact_subject'),
                                 'contact_message'   => $this->input->post('contact_message'),
								);
			$message = $this->parser->parse_string($message,$data_email,true);
            $messsage = "<p>Feedback Form</p>
                         <p><strong>Nama</strong></p>
                         <p>".$this->input->post('contact_name')."</p>
                         <p><strong>No. Telepon</strong></p>
                         <p>".$this->input->post('contact_phone')."</p>
                         <p><strong>Email</strong></p>
                         <p>".$this->input->post('contact_email')."</p>
                         <p><strong>Subject</strong></p>
                         <p>".$this->input->post('contact_subject')."</p>
                         <p><strong>Pesan</strong></p>
                         <p>".$this->input->post('contact_message')."</p>
                         <p>Terima Kasih.</p>
                         <p>Salam,</p>
                         <p>hotelsabang.com</p>";
            
			$this->email->message($message);
			$this->email->send();
        
            redirect('conten/kontak/1');
        }else{
            $row = $this->content_model->get_kontak();
            if(isset($row->id)){
                $data = array(
                        'id'        => $row->id,
                        'alamat'    => $row->alamat,
                        'kota'      => $row->kota,
                        'telepon'   => $row->telepon,
                        'fax'       => $row->fax,
                        'email'     => $row->email,
                        'wa'        => $row->wa,
                        'error'     => 1,
                        'success'   => 0,
                        'contact_name'      => $this->input->post('contact_name'),
                        'contact_phone'     => $this->input->post('contact_phone'),
                        'contact_email'     => $this->input->post('contact_email'),
                        'contact_subject'   => $this->input->post('contact_subject'),
                        'contact_message'   => $this->input->post('contact_message'),
                        );	
                $data['news_list'] = $this->berita_model->news_list();			
                $data['recaptcha'] = $this->recaptcha->render($this->lang->lang());
                $data['template'] = 'kontak';			
                $data = array_merge($data,basic_info());
                $this->parser->parse('index',$data);
            
            }
        }
    
    }
    
	function maps(){
		$row = $this->content_model->get_kontak();
		if(isset($row->id)){
			$data = array(
					'id' => $row->id,
					'alamat' => $row->alamat,
					'kota' => $row->kota,
					'telepon' => $row->telepon,
					'fax' => $row->fax,
					'email' => $row->email,
					);	
			$data['news_list'] =$this->berita_model->news_list();			
			$data['template'] = 'peta';			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
	}
	function about(){
		$row = $this->content_model->get_about();
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			$data['template'] = 'about';
            
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
	}
	
	function index()
	{
		$data = array();	
		$data['first_title'] = 'Berita Detail';		
		$data['second_title'] = 'Berita';		
		$data['template'] = 'berita';			
		$data['news_list'] =$this->content_model->news_list();			
		$data['kamar_list'] =$this->berita_model->kamar_list();			
		
	
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}	
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */