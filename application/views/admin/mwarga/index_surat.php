<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>				
				<a href="{base_url}admin/mwarga/add_surat" class="btn" data-toggle="tooltip" title="" data-original-title="Buat Surat"> <i class="fa fa-plus"></i></a>
			</li>
			
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
				<?php echo form_open('admin/mwarga/surat','class="form-horizontal" id="form-work"') ?>
		<div class="row">
				<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">NIK,NAMA,NO.SURAT</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="nosurat" name="nosurat" value="{nosurat}">
						</div>
					</div>
					
					<div class="form-group"  style="margin-bottom: 5px;">
						  <label class="col-md-4 control-label" for="example-daterange1">Tanggal</label>
						  <div class="col-md-8">
							<div class="input-daterange input-group" data-date-format="dd-mm-yyyy">
								<input class="form-control" type="text" id="tgl_dari" name="tgl_dari" placeholder="From" value="{tgl_dari}">
								<span class="input-group-addon"><i class="fa fa-chevron-right"></i></span>
								<input class="form-control" type="text" id="tgl_sampai" name="tgl_sampai" placeholder="To" value="{tgl_sampai}">
							</div>
						  </div>
					</div>
					<div class="form-group"  style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Jenis Surat</label>
						<div class="col-md-8">
							<select id="kode_surat" name="kode_surat" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Surat">
								<option value="999" <?=($kode_surat == '999' ? '':'')?>>Semua Surat</option>
								<?foreach($list_kode_surat as $row){?>
									<option value="<?=$row->kode_surat?>" <?=($kode_surat == $row->kode_surat ? 'selected':'')?>><?=$row->nama_surat?></option>
								<?}?>
							</select>
						</div>
					</div>
					
			</div>
			<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">RT</label>
						<div class="col-md-8">
							<select id="idrt" name="idrt" class="js-select2 form-control" style="width: 100%;" data-placeholder="Semua Surat">
								<option value="999" <?=($idrt == '999' ? '':'')?>>Semua Surat</option>
								<?foreach($list_rt as $row){?>
									<option value="<?=$row->idrt?>" <?=($idrt == $row->idrt ? 'selected':'')?>><?=$row->rt?></option>
								<?}?>
							</select>
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for="">Status</label>
						<div class="col-md-8">
							<select id="status" name="status" class="js-select2 form-control" style="width: 100%;" data-placeholder="Pilih Opsi">
								<option value="999" <?=($status == '999' ? 'selected':'')?>>Semua Surat</option>
								<option value="0" <?=($status == '0' ? 'selected' : '')?>>On Progress</option>
								<option value="1" <?=($status == '1' ? 'selected' : '')?>>Selesai</option>
							</select>
						</div>
					</div>
					
					<div class="form-group" style="margin-bottom: 5px;">
						<label class="col-md-4 control-label" for=""></label>
						<div class="col-md-8">
							<button class="btn btn-success text-uppercase" type="submit" name="button" style="font-size:13px;width:100%;float:right;" value="1"><i class="fa fa-filter"></i> Filter</button>
						</div>
						
						
					</div>
				<?php echo form_close() ?>
			</div>
		</div>
		<br>
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">#</th>
						<th width="10%">No Surat</th>
						<th width="10%">Tanggal</th>
						<th width="20%">Nama</th>
						<th width="20%">Alamat</th>
						<th width="15%">Keperluan</th>
						<th width="10%">Status</th>
						<th width="10%">Action</th>					
					</tr>
				</thead>
				<tbody>
					<?
					$i=$this->uri->segment(4);
					foreach($list_surat as $row){
						$i=$i+1;
						?>
						<tr>
						<td><?=$i?></td>
						<td><?=$row->ket_surat?></td>
						<td><?=human_date_short($row->input_date)?></td>
						<td><?=$row->nik.'<br>'.$row->nama.' ('.substr($row->jenis_kelamin,0,1).')'?></td>
						<td><?=$row->alamat_lengkap?></td>
						<td><?=$row->keperluan?></td>
						<td><?=onprogress($row->acc_rw)?></td>
						<td>
							<?if ($row->publish){?>
							<div class="btn-group">
							<button data-toggle="dropdown" class="btn btn-primary" aria-expanded="true">Action <span class="caret"></span></button>
								<ul class="dropdown-menu">
									
									<?if ($row->acc_rw=='0'){?>
										<li><a href="{base_url}admin/mwarga/editor_surat/<?=$row->id?>">Edit</a></li>
										<li><a href="{base_url}admin/mwarga/acc_surat/<?=$row->id?>">Acc RW</a></li>
										<li><a href="{base_url}admin/mwarga/delete_surat/<?=$row->id?>" onclick="return confirm('Anda Yakin Akan Menghapus?')">Delete</a></li>
									<?}else{?>
										<li><a href="{base_url}admin/mwarga/print_surat/<?=$row->id?>"  target="_blank">Cetak</a></li>
									<?}?>
									
								</ul>
							</div>
							<?}else{?>
								<span class="label label-danger text-uppercase" data-toggle="tooltip" title="ID : <?=$row->id?>">DIHAPUS</span>
							<?}?>
						</td>
						</tr>
					<?}?>
				</tbody>
			</table>
			<? if ($list_surat){?>
			<div class="text-left">
				{pagination} 
			</div>
			<?}?>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>


