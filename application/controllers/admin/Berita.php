<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('berita_model','berita_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['list_kategori'] 			= $this->berita_model->list_tipe();
		$data['list_author'] 			= $this->berita_model->list_author();
		$data['title'] 			= 'Berita';
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("BERITA",'#'),
								array("List",'admin\berita')
								);
		$data['template'] = 'admin/berita/index';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
		$content     		= $this->input->post('content');
		$title     		= $this->input->post('title');
		$kategori     		= $this->input->post('kategori');
		$publish     		= $this->input->post('publish');
		$author     		= $this->input->post('author');
		
		$where='';
		if ($content !=''){
			$where .=" AND M.content LIKE '%".$content."%'";
		}
		if ($title !=''){
			$where .=" AND M.title LIKE '%".$title."%'";
		}
		if ($kategori !='#'){
			$where .=" AND M.kategori = '".$kategori."'";
		}
		if ($publish !='#'){
			$where .=" AND M.publish = '".$publish."'";
		}
		if ($author !='#'){
			$where .=" AND M.author = '".$author."'";
		}
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT M.id,M.title,M.created_at,M.publish,K.kategori as nama_kategori,A.author_ref 
				from news M
				LEFT JOIN kategori K ON M.kategori=K.id
				LEFT JOIN mauthor A ON A.id=M.author
				WHERE M.id IS NOT NULL ".$where."
			) as T ORDER BY id DESC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/berita/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->title;
            $row[] = HumanDateShort($r->created_at);
            $row[] = $r->nama_kategori;
            $row[] = $r->author_ref;

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function getDataList()
    {
        $query = $this->berita_model->data_list(0);
        $this->output->set_output($query);
    }
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Berita - Addnew');
		$data['id'] = "";
		$data['kategori'] = "";
		$data['title'] = "";
		$data['img_news'] = "";
		$data['sinopsis'] = "";
		$data['content'] = "";
		// $data['created_at'] = "";
		$data['publish'] = "0";
		$data['updated_at'] = "";
		$data['error'] = "";
		$data['created_at'] = date('d-m-Y');
		$data['edit'] = "0";
		$data['list_tag'] = $this->berita_model->list_tag();
		$data['list_tipe'] = $this->berita_model->list_tipe();
		$data['list_author'] 			= $this->berita_model->list_author();
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("TAMBAH",'#'),
								array("NEWS",'admin\berita'));
		$data['judul'] = 'Berita - Input Baru';
		$data['template'] = 'admin/berita/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		// print_r($this->input->post());exit();
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->berita_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/berita/editor/$id/");				
			}
		}else{		
			if ($this->berita_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/berita/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'berita/manage';
		$data['list_tipe'] = $this->berita_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->berita_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'admin\berita',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("berita",'admin\berita',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'berita - Edit');
			if($row = $this->berita_model->get_news($id))
			{
				$data['id'] = $id;
				$data['sinopsis'] = $row->sinopsis;
				$data['content'] = $row->content;
				$data['img_news'] = $row->img_news ;
				$data['kategori'] = $row->kategori;
				$data['author'] = $row->author;
				$data['title'] = $row->title;
				$data['created_at'] = date_format(date_create($row->created_at),'d-m-Y');
				// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
				$data['edit'] = 1;
				// print_r($data['img_news']);exit();
			}
		$data['list_tag'] = $this->berita_model->list_tag($id);
		// print_r($data);exit();
		$data['list_tipe'] = $this->berita_model->list_tipe();
		$data['list_author'] 			= $this->berita_model->list_author();
		if ($id){
			$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("NEWS",'admin\berita'));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'admin\berita',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['judul'] = 'NEWS - Edit';
		$data['template'] = 'admin/berita/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->berita_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/berita");
		}
	}
}
?>