var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false},
        {"data": "kategori", searchable : true, orderable: false, className : 'not_search'},
        {"data": "publish", searchable : true, orderable: false},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
        action += '<li><a href="'+ site_url +'admin/menu/editor/' + aData['id'] + '">Edit</a></li>';
		
		if (aData['publish'] =='0'){
			action += '<li><a href="'+ site_url +'admin/menu/update_status/' + aData['id'] + '/1">Published</a></li>';			
			action += '<li><a href="'+ site_url +'admin/menu/update_status/' + aData['id'] + '/2">Delete</a></li>';			
		}else if (aData['publish'] =='1'){
			action += '<li><a href="'+ site_url +'admin/menu/update_status/' + aData['id'] + '/0">Unpublish</a></li>';	
			action += '<li><a href="'+ site_url +'admin/menu/update_status/' + aData['id'] + '/2">Delete</a></li>';			
		}else if (aData['publish'] =='2'){
			action += '<li><a href="'+ site_url +'admin/menu/update_status/' + aData['id'] + '/1">Pulihkan</a></li>';
		}
		
        action += '</ul></div>';										
        action += '</div>';
        // $("td:eq(2)", nRow).html(gambar(aData['img']));
        $("td:eq(2)", nRow).html(cek_status(aData['publish']));
        $("td:eq(3)", nRow).html(action);
        return nRow;            
    }        
})
function cek_status($status)
{	
	if ($status=='0'){
		return '<span class="label label-default">Belum Publish</span>';	
	}else if($status=='1'){
		return '<span class="label label-success">Sudah Publish</span>';	
	}else if($status=='2'){
		return '<span class="label label-danger">Dihapus</span>';	
	}
	// return $status;
		
}
function gambar($img)
{	
	
	return '<img src="'+site_url+'assets/upload/menu/'+$img+'" id="output_img" class="img-polaroid"  style="width:200px;height:80px;"/>';	
			
}

$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
