jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
$(document).ready(function(){
	$('.number').number(true, 2);	
	$("#idkecamatan").select2();
});
$("#idkota").change(function(){
	if ($(this).val()!=''){
		$('#idkecamatan')
		.find('option')
		.remove()
		.end()
		.append('<option value="">- Silahkan Pilih Kecamatan -</option>');
		$.ajax({
			url: site_url+'admin/All/find_kecamatan/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idkecamatan')
				.append('<option value="' + item.id + '">' + item.kecamatan + '</option>');
			});
			}
		});
	}

});
	
$("#idkota").select2({
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'admin/All/s2_kota/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.kota + ' - ('+item.propinsi+')',
						id: item.idkota
					}
				})
			};
		}
	}
});
