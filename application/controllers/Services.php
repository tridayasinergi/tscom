<?php

class Services extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//permission_basic_admin($this->session);
		$this->load->model('about_us_model','about_us_model');
		$this->load->model('home_model','home_model');
		$this->load->model('services_model','services_model');
		
	}
	function index(){
			$data=array();
			$data['services_list']=$this->home_model->services_list();
			$data['project_list']=$this->home_model->project_all_list();
			$data['template'] = 'services';
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
	}

	function detail($id,$url){
		$row = $this->about_us_model->get_service_detail($id);
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['image_list'] = $this->services_model->get_services_gallery($id);
			$data['services_list']=$this->home_model->services_list($id);
			$data['template'] = 'service_detail';
			$data['menu'] = 'menu.we';
            // print_r($data);exit();
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
		
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */