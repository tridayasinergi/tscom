<div class="subheader section-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="breadcrumb-wrapper">
					<div class="page-title">
						<h1 class="text-theme fw-500">{title}</h1>
					</div>
					<ul class="custom breadcrumb">
						<li>
							<a href="{base_url}">Home - </a>
						</li>
						<li class="active">
							{title}
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="section-padding blog-details">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- article -->
				<article class="post">
					<div class="post-wrapper">
						
						<div class="blog-meta">
							<?foreach($list_kategori as $row){?>
							<div class="section-header">
								<div class="section-heading">
									<h3><a class="text-custom-blue" href="#"><?=$row->nama_kategori?></a></h3>
								</div>
							</div>
							<div class="row">
								<?
									$q="SELECT * FROM `files` WHERE kategori_id='".$row->id."' AND publish='1' ORDER BY id asc";
									$list_detail=$this->db->query($q)->result();
								?>
							<?foreach($list_detail as $r){?>
								<div class="col-lg-3 col-md-6">
									<div class="property-grid-box mb-xl-20">
										<div class="property-grid-wrapper">
											<div class="property-img animate-img"><a href="#"> <img class="img-fluid full-width" alt="#" src="{files_path}<?=$r->image_path?>" onerror="this.onerror=null;pagespeed.lazyLoadImages.loadIfVisibleAndMaybeBeacon(this);"> </a>
												<div class="property-type"> <a class="property-tag-2 bg-custom-blue text-custom-white" href="#"><?=$r->ukuran?></a></div>
												<div class="property-address bg-custom-blue"><a href="{files_path}<?=$r->file_path?>" target="_blank"><?=$r->keterangan?></a></div>
											</div>
											<div class="property-grid-footer"><a class="text-theme fs-14 fw-600" href="{files_path}<?=$r->file_path?>" target="_blank">Download </a></div>
										</div>
									</div>
								</div>
							<?}?>
								
							</div>
							<?}?>
						</div>
					</div>
				</article>
				<!-- article -->
				<hr>
				
			</div>
		</div>
	</div>
</section>