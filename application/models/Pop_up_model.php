<?php

class Pop_up_model extends CI_Model {
	function __construct()
	{
		
   	}
	
	function add_record() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$this->judul    	    = $this->input->post('judul'); 		
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->prioritas   	    = $this->input->post('prioritas'); 
		$this->tgl_1   		    = Format_YMD($this->input->post('tgl_1'));
		$this->tgl_2   		    = Format_YMD($this->input->post('tgl_2'));
		$this->tangal_created   = date('Y-m-d H:i:s');
		$this->publish   		= 0; 
		$this->db->insert('pop_up', $this);
		return true;
	}
	
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('pop_up');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$id                     = $this->input->post('id'); 
		$this->judul   	        = $this->input->post('judul'); 		
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->prioritas   	    = $this->input->post('prioritas'); 
		$this->tgl_1   		    = Format_YMD($this->input->post('tgl_1'));
		$this->tgl_2   		    = Format_YMD($this->input->post('tgl_2'));
		$this->tangal_created   = date('Y-m-d H:i:s');
		$this->db->where('id',$id);
		$this->db->update('pop_up',$this);
		return true;
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('pop_up',array('publish' => $status));
		return true;
	}
	

	
	function get_pop_up($id)
	{
		$this->db->select('pop_up.*');
		$this->db->from('pop_up');
		$this->db->where('pop_up.id',$id);
		$query = $this->db->get();
		return $query->row_array();
	}
	
	
	
}
