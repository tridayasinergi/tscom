var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false, className : 'not_search'},
        {"data": "propinsi", searchable : true, orderable: true},
        {"data": "kota", searchable : true, orderable: true},
        {"data": "kecamatan", searchable : true, orderable: true},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
        action += '<li><a href="'+ site_url +'admin/mkecamatan/editor/' + aData['id'] + '">Edit</a></li>';
		if (user_tipe_id <=4){
        action += '<li><a href="'+ site_url +'admin/mkecamatan/delete/' + aData['id'] + '">Delete</a></li>';
        }
		
		action += '</ul></div>'							
								
        action += '</div>';
		if (user_tipe_id <= 5){
			if (user_tipe_id == 5){
				if (idkecamatan_session == aData['id']){
					$("td:eq(4)", nRow).html(action);			
				}
				
			}else{
				$("td:eq(4)", nRow).html(action);			
			}
		}
		
        return nRow;            
    }        
})
function cek_status($status)
{	
	if ($status=='1'){
		return '<i class="si si-check fa-2x"></i>';	
	}else{
		return '<i class="si si-close fa-2x"></i>';	
	}
	// return $status;
		
}

$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
