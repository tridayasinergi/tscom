jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
$(document).ready(function(){
	$('.number').number(true, 2);	
	$("#idkk").select2();
	$("#idkecamatan").select2();
	$("#iddesa").select2();
	$("#idrw").select2();
	$("#idrt").select2();
	$("#idekonomi").select2();
	$("#idpekerjaan").select2();
	$("#status_nikah").select2();
	$("#idagama").select2();
	$("#jenis_kelamin").select2();
	$("#status_warga_id").select2();
	$("#idpendidikan").select2();
	$("#status_kk").select2();
	$(document).on("click","#btn_simpan",function(e){
		if(!validasi_save_final()) return false;		
	});
	$('#form1').on('submit', function(e) {		
		var form = this;
		// e.preventDefault();
		// e.stopPropagation(); 
		// swal({
			// title: "Lanjutkan Input",
			// text: "'Ya' dengan KK Baru, 'Tidak' Untuk Document Baru",
			// type: "warning",
			// showCancelButton: true,
			// confirmButtonColor: '#96d25f',
			// confirmButtonText: 'Ya',
			// cancelButtonText: "Tidak",
			// closeOnConfirm: true,
			// closeOnCancel: true
		// },
		// function(isConfirm) {
			// alert('SINI BROOOO');
			// if (isConfirm) {
			  // $(e.currentTarget).trigger(e.type, {'send': true});
			// }
		// }); 
		// alert($("#edit").val());
		if ($("#edit").val()=='0'){
			a=confirm('Lanjutkan Input Dengan No. KK Sama ?');			
			if (a) {
				$("#st_lanjut").val(1);
			}else{
				$("#st_lanjut").val(0);
			}
		}
		
	});
});
function allowSubmit(){
				alert('SINI BROOOO');
			
	swal({
			title: "Lanjutkan Input",
			text: "'Ya' dengan KK Baru, 'Tidak' Untuk Document Baru",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#96d25f',
			confirmButtonText: 'Ya',
			cancelButtonText: "Tidak",
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function(isConfirm) {
			if (isConfirm) {
			  $(e.currentTarget).trigger(e.type, {'send': true});
			}
		}); 
}
function validasi_save_final(){
	// alert($("#status_kk").val());
	// alert($("#idkota").val());
	if ($("#nokk").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi KK", "error");
		return false;	
	}
	if ($("#status_kk").val()=='0' ){
		sweetAlert("Maaf...", "Silahkan Isi Hubungan Keluarga", "error");
		return false;	
	}
	if ($("#idkk").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Nama Kepala Keluarga", "error");
		return false;	
	}
	if ($("#nik").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi NIK", "error");
		return false;	
	}
	if ($("#valid_nik").val()=='0' ){
		sweetAlert("Maaf...", "Nik Duplicate", "error");
		return false;	
	}
	if ($("#nama").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Nama", "error");
		return false;	
	}
	if ($("#jenis_kelamin").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Jenis Kelamin", "error");
		return false;	
	}
	if ($("#status_nikah").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Status Nikah", "error");
		return false;	
	}
	if ($("#idagama").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Agama", "error");
		return false;	
	}
	if ($("#idpekerjaan").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Pekerjaan", "error");
		return false;	
	}
	if ($("#alamat").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Alamat", "error");
		return false;	
	}
	if ($("#status_warga_id").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Status Warga", "error");
		return false;	
	}
	if ($("#idkota").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Kota", "error");
		return false;	
	}
	if ($("#idkota").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Kota", "error");
		return false;	
	}
	if ($("#idkecamatan").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Kecamatan", "error");
		return false;	
	}
	if ($("#iddesa").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Desa", "error");
		return false;	
	}
	if ($("#idrw").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi RW", "error");
		return false;	
	}
	if ($("#idrt").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi RT", "error");
		return false;	
	}
	return true;
	
}
function konfirmasi(){
	swal({
	  title: "Are you sure?",
	  text: "You will not be able to recover this imaginary file!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, delete it!",
	  cancelButtonText: "No, cancel plx!",
	  closeOnConfirm: false,
	  closeOnCancel: false
	},
	function(isConfirm) {
	  if (isConfirm) {
		swal("Deleted!", "Your imaginary file has been deleted.", "success");
		return true;
	  } else {
		return false;
		swal("Cancelled", "Your imaginary file is safe :)", "error");
	  }
	});
}
$("#alamat").keyup(function(){
	gabung();
});
$("#nik").blur(function(){
	if ($("#nik").val() != $("#nik_asal").val()){
		var str=$(this).val();
		str=str.trim();
		// nik= str.replace(/[^\w\s]/gi, '');
		var nik=str.replace(/\s/g, '');
		// alert(nik);
		$.ajax({
			url: site_url+'admin/mwarga/find_nik/'+nik,
			dataType: "json",
			method: 'POST',
			data: { nik: nik},
			success: function(data) {
				
				if (data != null) {
					$("#valid_nik").val('0');
					$("#div_nik").addClass('has-error');
					$('#val-nik-error').text('NIK Sudah Digunakan Oleh : '+ data);
					// alert(data);
				}else{
					$("#valid_nik").val('1');
					$("#div_nik").removeClass('has-error');	
					$('#val-nik-error').text('');
				}				
			}
		});
	
	}else{
		$("#valid_nik").val('1');
		$("#div_nik").removeClass('has-error');	
		$('#val-nik-error').text('');
	}
});

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
 
    return false;
    return true;
}
$("#status_warga_id").change(function(){
	if ($("#status_warga_id").val()=='1'){
		$("#alamat_ktp").val($("#alamat_lengkap").val());		
	}else{
		$("#alamat_ktp").val('');
	}
});
$("#status_kk").change(function(){
	if ($("#status_kk").val()=='1'){
		$('#idkk').val(0).trigger('change');	
	}else if($("#status_kk").val()=='2'){
		$('#jenis_kelamin').val('Perempuan').trigger('change');
		// $('#idkk').val('').trigger('change');
	}else{
		// if ($('#idkk').val(0)){
			
		// }else{
			// $('#idkk').val('').trigger('change');
		// }
	}
});
$("#idkota").change(function(){
	if ($(this).val()!=''){
		$('#idkecamatan').find('option').remove().end().append('<option value="">- Silahkan Pilih Kecamatan -</option>');
		$('#iddesa').find('option').remove().end().append('<option value="">- Silahkan Pilih Desa -</option>');
		$('#idrw').find('option').remove().end().append('<option value="">- RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="">- RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_kecamatan/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idkecamatan')
				.append('<option value="' + item.id + '">' + item.kecamatan + '</option>');
			});
			}
		});
	}
	gabung();
});
$("#idkecamatan").change(function(){
	if ($(this).val()!=''){
		$('#iddesa').find('option').remove().end().append('<option value="">- Silahkan Pilih Desa -</option>');
		$('#idrw').find('option').remove().end().append('<option value="">- RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="">- RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_desa/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#iddesa')
				.append('<option value="' + item.id + '">' + item.desa + '</option>');
			});
			}
		});
	}
	gabung();
});
$("#iddesa").change(function(){
	if ($(this).val()!=''){
		$('#idrw').find('option').remove().end().append('<option value="">- RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="">- RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_rw/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idrw')
				.append('<option value="' + item.id + '">' + item.rw + '</option>');
			});
			}
		});
	}
	gabung();
});
$("#idrw").change(function(){
	if ($(this).val()!=''){
		$('#idrt').find('option').remove().end().append('<option value="">- RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_rt/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idrt')
				.append('<option value="' + item.id + '">' + item.rt + '</option>');
			});
			}
		});
	}
	gabung();
});
$("#idrt").change(function(){
		// alert();
	gabung();
});
function gabung(){
	$alamat='';
	if ($("#alamat").val() != ''){
		$alamat=$alamat + $("#alamat").val();
	}
	if ($("#idrt").val() != ''){
		$alamat=$alamat + ' RT. ' + $("#idrt option:selected").text();
	}
	if ($("#idrw").val() != ''){
		$alamat=$alamat + ' RW. ' + $("#idrw option:selected").text();
	}
	if ($("#iddesa").val() != ''){
		$alamat=$alamat + ', Ds. ' + $("#iddesa option:selected").text();
	}
	if ($("#idkecamatan").val() != ''){
		$alamat=$alamat + ' Kec. ' + $("#idkecamatan option:selected").text();
	}
	if ($("#idkota").val() != ''){
		$alamat=$alamat + ', ' + $("#idkota option:selected").text();
	}
	
	if ($("#status_warga_id").val()=='1'){
		$("#alamat_ktp").val($alamat);		
	}else{
		$("#alamat_ktp").val('');
	}
	// alert($alamat);
	$("#alamat_lengkap").val($alamat);
}	
$("#idkota").select2({
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'admin/All/s2_kota/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.kota + ' - '+item.propinsi+'.',
						id: item.idkota
					}
				})
			};
		}
	}
});
