<?php

class Page_testimoni_model extends CI_Model {
	function __construct()
	{
		
   	}
	
	
	function add_record() 
	{
		// $this->urutan   		= $this->input->post('urutan'); 
		$this->link   	        = replace_embed($this->input->post('link')); 
		$this->title   	        = $this->input->post('title'); 
		$this->sinopsis   	    = $this->input->post('sinopsis'); 
		$this->str_key   	    = $this->get_key($this->input->post('kategori')); 
		
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->created_at   	= Format_YMD($this->input->post('created_at'));
		// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->publish   		= 0; 
		$this->created_by   	= $this->session->userdata('user_id');
		$this->author   		= $this->input->post('author');
		// if($this->upload_img_page_testimoni(false)){ 
			$this->db->insert('page_testimoni', $this);
			return true;
		// }else{
			// // print_r('sini');
			// return false;
		// }
	}
	function get_key($kategori){
		$q="SELECT SUBSTR(kategori FROM 1 FOR 1) as str_key FROM kategori WHERE id='$kategori'";
		$row=$this->db->query($q)->row('str_key');
		if ($row){
			$huruf=strtolower($row."-");				
		}else{
			$huruf="z-";			
		}
		$q="SELECT max(`str_key`) as akhir FROM page_testimoni WHERE str_key LIKE '".$huruf."%'";
		$row=$this->db->query($q)->row('akhir');
		if ($row){
			$akhir=$row;		
		}else{
			$akhir=$huruf.'000';				
		}		
		$urutan = (int) substr($akhir, 2, 3) + 1;
		$key = $huruf . sprintf("%03s", $urutan);
		return $key;
		// print_r($key);exit();
	}
	function upload_img_page_testimoni ($update = false){
		// print_r($_FILES['img_page_testimoni']);exit();
		if(isset($_FILES['img_page_testimoni'])){
			if($_FILES['img_page_testimoni']['name'] != ''){
				// $config['upload_path'] = './assets/images/page_testimoni/original/';
				$config['upload_path'] = './assets/upload/page_testimoni/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img_page_testimoni']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img_page_testimoni')){
					$image_upload = $this->upload->data();
					$this->img_page_testimoni  = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/page_testimoni/'.$this->img_page_testimoni );			
					$this->myimage->resize(1920,800);
					$this->myimage->save('./assets/upload/page_testimoni/'.$this->img_page_testimoni );
					$this->myimage->resize(798,370);
					$this->myimage->save('./assets/upload/page_testimoni/thumbs/'.$this->img_page_testimoni );
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				// print_r('sini');
				return true;
			}
		}else{
			
				print_r('Sahabat');exit();
			return false;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img_page_testimoni );exit();
		if(file_exists('./assets/upload/page_testimoni/'.$row->img_page_testimoni ) && $row->img_page_testimoni  !='') {
			unlink('./assets/upload/page_testimoni/'.$row->img_page_testimoni );
			unlink('./assets/upload/page_testimoni/thumbs/'.$row->img_page_testimoni );
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('page_testimoni');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$id                     = $this->input->post('id'); 
		$this->link   	        = replace_embed($this->input->post('link')); 
		$this->title   	        = $this->input->post('title'); 
		$this->sinopsis   	    = $this->input->post('sinopsis'); 
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->author   		= $this->input->post('author');
		$this->created_at   	= Format_YMD($this->input->post('created_at'));
		$this->updated_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->updated_by   	= $this->session->userdata('userid'); 
		// $this->author   		= $this->session->userdata('tipe_user'); 
		// print_r($this);exit();
		// if($this->upload_img_page_testimoni (true)){ 
			$this->db->where('id',$id);
			$this->db->update('page_testimoni',$this);
			return true;
		// }else{
			// return false;
		// }
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('page_testimoni',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('kategori.*');
		$this->db->from('kategori');
		// $this->db->where('kategori.id',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_author()
	{
		$this->db->select('mauthor.*');
		$this->db->from('mauthor');
		$this->db->where('publish',1);
		$this->db->order_by('author_ref','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_page_testimonis($id)
	{
		$this->db->select('page_testimoni.*');
		$this->db->from('page_testimoni');
		$this->db->where('page_testimoni.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function cek_page_testimoni($id)
	{
		$this->db->select('page_testimoni.*');
		$this->db->from('page_testimoni');
		$this->db->where('page_testimoni.username',$id);
		$this->db->where('page_testimoni.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
