<?php
class Home_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function count($admin = false)
	{
        if($admin == false) $this->db->where('publish',1);
		$this->db->from('news');
		$query = $this->db->count_all_results();
        return $query;
	}
	function news_list()
    {	
		$this->db->select('news.*,kategori.kategori as namakategori');
		$this->db->from('news');
		$this->db->join('kategori','kategori.id=news.kategori');
		$this->db->where('news.publish',1);
		$this->db->order_by('news.created_at','DESC');		
		$this->db->limit(10);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function get_index_page(){
		$q="SELECT *from pages M
			WHERE M.publish='1' AND M.kategori='0'
			ORDER BY M.id DESC
			LIMIT 1";
		return $this->db->query($q)->row_array();
	}
	function gallery_list()
    {	
		$this->db->select('gallery.*');
		$this->db->from('gallery');
		
		$this->db->where('gallery.publish',1);
		$this->db->order_by('gallery.tanggal_gallery','DESC');		
		// $this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	// function gallery_kategori()
    // {	
		// $this->db->select('gallery.*,kategori.kategori as namakategori');
		// $this->db->from('gallery');
		// $this->db->join('kategori','kategori.id=gallery.kategori');
		// $this->db->group_by('gallery.kategori');
		
		// $this->db->where('gallery.publish',1);
		// // $this->db->limit(5);
		// $query = $this->db->get();	
		// // print_r($this->db->last_query());exit();
		// return $query->result();
    // }
	function get_welcome(){
		// $lang=get_cookie('lang_is');
		//get_cookie('lang_is')=='in'
		// print_r($lang);exit();
		
		$this->db->select('news.*');
		$this->db->where('key','welcome');
		$query = $this->db->get('news');	
		// print_r($query->row());exit();
		return $query->result();	
		
	}
	function slider_list()
    {	
		$this->db->select('slider.*');
		$this->db->from('slider');
		$this->db->where('slider.publish',1);
		$this->db->order_by('slider.urutan','ASC');
		$query = $this->db->get();	
		return $query->result();
    }
	function services_list($id=0)
    {	
		$this->db->select('services.*');
		$this->db->from('services');
		$this->db->where('services.publish',1);
		if ($id){
		$this->db->where('services.id <> ',$id);
			
		}
		$this->db->order_by('services.id');
		$query = $this->db->get();	
		return $query->result();
    }
	function why_list()
    {	
		$this->db->select('why.*');
		$this->db->from('why');
		$this->db->limit(4);
		$this->db->order_by('rand()');
		$this->db->where('why.publish',1);
		$query = $this->db->get();	
		return $query->result();
    }
	function client_list()
    {	
		$this->db->select('client.*');
		$this->db->from('client');
		// $this->db->limit(10);
		$this->db->order_by('rand()');
		$this->db->where('client.publish',1);
		$query = $this->db->get();	
		return $query->result();
    }
	function project_list()
    {	
		$this->db->select('project.id,project.title_in,project.title_en,project.harga_in,
					project.harga_en,project.desc_in,project.desc_en,
					group_concat(concat("<li><p>",project_detail.desc_in,"</p></li>")order by project_detail.id asc separator "" ) as det_in,
					group_concat(concat("<li><p>",project_detail.desc_en,"</p></li>")order by project_detail.id asc separator "" ) as det_en',false);
		$this->db->from('project');
		$this->db->join('project_detail','project.id = project_detail.project_id');
		$this->db->where('project.promo',1);
		$this->db->order_by('project.id','ASC');
		$this->db->group_by('project.id');
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
   function project_all_list()
    {	
		$this->db->select('project.id,project.title_in,project.title_en,project.harga_in,
					project.harga_en,project.desc_in,project.desc_en,
					group_concat(concat("<li><p>",project_detail.desc_in,"</p></li>")order by project_detail.id asc separator "" ) as det_in,
					group_concat(concat("<li><p>",project_detail.desc_en,"</p></li>")order by project_detail.id asc separator "" ) as det_en',false);
		$this->db->from('project');
		$this->db->join('project_detail','project.id = project_detail.project_id');
		$this->db->order_by('project.id','ASC');
		$this->db->group_by('project.id');
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
    
	
	function team_list($limit=4)
    {	
		$this->db->select('team.*,mjabatan.jabatan_en,jabatan_in');
		$this->db->join('mjabatan','mjabatan.id=team.jabatan_id');
		$this->db->where('publish',1);
		// $this->db->where('kategori','promosi');
		$this->db->order_by('team.id');
		$this->db->limit($limit);
		$query = $this->db->get('team');	
		return $query->result();
    }
	
	
	
	
	function tampil_news($limit=3)
    {	
		$this->db->select('news.*');
		$this->db->where('kategori','news');
		$this->db->where('publish',1);
		$this->db->order_by('created_at','DESC');
		$this->db->limit($limit);
		$query = $this->db->get('news');	
		return $query->result();
    }
	
	function tampil_detail()
    {	
		$this->db->select('news.*');
		$this->db->where('id',$this->uri->segment(3));
		$this->db->where('publish',1);
		$this->db->order_by('id','DESC');
		$query = $this->db->get('news');	
		return $query->result();
    }
	
	
	
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('news');
        return $query->row();
    }
	
	 function autoupdates($id){
		$this->db->set('viewed', 'viewed + 1');
		$this->db->where('id',$id);
		$this->db->update('news');
		//echo '<pre>'; print_r($id); die();
	} 
}

?>