<?php

function backend_info()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),
        'assets_path'          => base_url().'assets/',
        'assets_front_path'          => base_url().'assets/front/',
        'css_path'             => base_url().'assets/css/',
        'fonts_path'           => base_url().'assets/fonts/',
        'img_path'             => base_url().'assets/img/',
        'js_path'              => base_url().'assets/js/',
        'plugins_path'         => base_url().'assets/js/plugins/',
        'upload_path'          => base_url().'assets/upload/',
        'avatar_path'          => base_url().'assets/upload/avatars/',
        'ttd_path'          => base_url().'assets/upload/ttd/',
        'slider_path'          => base_url().'assets/upload/slider/',
		'berita_path'          => base_url().'assets/upload/news/',
		'page_path'          => base_url().'assets/upload/pages/',
		'content_path'          => base_url().'assets/upload/content/',
		'tinymce_path' 		   => base_url().'assets/js/tiny_mce/',
        'cover_path'           => base_url().'assets/cover/',
        'bg_path'          		 => base_url().'assets/img/bg/',
        'guestfoto_path'       => base_url().'assets/guestphoto/',
        'ajax'                 => base_url().'assets/ajax/',
        'toastr_css'           => base_url().'assets/toastr/toastr.min.css',
        'toastr_js'            => base_url().'assets/toastr/toastr.min.js',
		'gallery_path'          => base_url().'assets/img/gallery/',
		'files_path'          => base_url().'assets/upload/files/',
       
    );
    return $data;
}
function basic_info()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),
        'assets_path'          => base_url().'assets/front/',
        'css_path'             => base_url().'assets/front/css/',
        'fonts_path'           => base_url().'assets/fonts/',
        'img_path'             => base_url().'assets/front/img/',
        'js_path'              => base_url().'assets/js/',
        'plugins_path'         => base_url().'assets/js/plugins/',
        'upload_path'          => base_url().'assets/upload/',
        'avatar_path'          => base_url().'assets/upload/avatars/',
		'page_path'          => base_url().'assets/upload/pages/',
        'slider_path'          => base_url().'assets/upload/slider/',
        'berita_path'          => base_url().'assets/upload/news/',
        // 'berita_path'          => 'https://tridayasinergi.com/assets/upload/news/',
        'gallery_path'          => base_url().'assets/img/gallery/',
        // 'gallery_path'          => 'https://tridayasinergi.com/assets/img/gallery/',
        'content_path'          => base_url().'assets/upload/content/',
		'tinymce_path' 		   => base_url().'assets/js/tiny_mce/',
        'cover_path'           => base_url().'assets/cover/',
        'bg_path'          		 => base_url().'assets/img/bg/',
        'guestfoto_path'       => base_url().'assets/guestphoto/',
        'ajax'                 => base_url().'assets/ajax/',
        'toastr_css'           => base_url().'assets/toastr/toastr.min.css',
        'toastr_js'            => base_url().'assets/toastr/toastr.min.js',
		'files_path'          => base_url().'assets/upload/files/',
       
    );
	// $CI =& get_instance();
	$q="select *from minfo";
    $row = $CI->db->query($q)->row();
	if ($row){
		$data['per_nama']=$row->nama_perusahaan;
		$data['per_email']=$row->email;
		$data['per_telepon']=$row->telepon;
		$data['per_fax']=$row->fax;
		$data['per_cs1']=$row->cs1;
		$data['per_cs2']=$row->cs2;
		$data['per_fb']=$row->fb;
		$data['per_ig']=$row->ig;
		$data['per_youtube']=$row->youtube;
		$data['per_cs1_judul']=$row->cs1_judul;
		$data['per_cs2_judul']=$row->cs2_judul;
		$data['per_fb_judul']=$row->fb_judul;
		$data['per_ig_judul']=$row->ig_judul;
		$data['per_youtube_judul']=$row->youtube_judul;
		$data['info1']=$row->info1;
		$data['info2']=$row->info2;
		$data['info3']=$row->info3;
		
	}
	$q="SELECT *from pop_up 
		WHERE publish='1' AND CURDATE() BETWEEN tgl_1 AND tgl_2
		ORDER BY publish DESC,prioritas DESC,tgl_1 DESC
		LIMIT 1";
	$row = $CI->db->query($q)->row();
	if ($row){
		$data['judul_info']=$row->judul;
		$data['content_info']=$row->content;
	}else{
		$data['judul_info']='';
		$data['content_info']='';
	}
	$q="SELECT *from flash 
		WHERE publish='1'
		ORDER BY urutan ASC
		";
	$data['flash_info'] = $CI->db->query($q)->result();
	// print_r($data['flash_info']);exit()
		// $data['judul_info']='SELAMAT SUKESE SELALU';
		// $data['content_info']='<img src="https://www.tridayasinergi.com/backend/images/news_Big_reaktivasi_sticker2.jpg" alt="" width="90%" height="90%"> <br><a href="about.html" class="btn-first btn-submit">Read More</a>';
       
    return $data;
}
