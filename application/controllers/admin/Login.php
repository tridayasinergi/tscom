<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

   public function __construct()
   {
      parent::__construct();
	  
      $this->load->model('login_model','login_model');
   }

	public function index()
   { 
	 	
     if ( $this->session->userdata('logged_in')) {
         redirect('admin/dashboard');
      }
    $data['user_err']="";
	if (isset($_POST['username'])){
      $username=$this->input->post('username');
        if ( $res = $this->login_model->verify_user($username, $this->input->post('password'))) {
        	$newdata = array(  	'userid'  => $res->id,
      							'username'  => $username,
							   	'tipe_user'    => $res->tipe_user,
      							'nama_user'      => $res->nama_user,
        						'img'    => $res->img,	
        						'user_tipe_id'    => $res->user_tipe_id,	
        						'idkota'      => $res->idkota,	
        						'idkecamatan'      => $res->idkecamatan,	
        						'iddesa'      => $res->iddesa,	
        						'idrw'      => $res->idrw,	
        						'idrt'      => $res->idrt,	
        						'operator_id'    => $res->operator_id,	
								'logged_in' => TRUE
			               );
			$this->session->set_userdata($newdata); 
            redirect('admin/dashboard');
                      
        }else{
	  		$data['user_err']="Username atau Password salah";
        }
	}
		$data = array_merge($data, backend_info());
		$this->load->view('admin/login_view',$data);
      
   }

   public function logout()
   {
    $this->session->sess_destroy();
	$this->load->dbutil();
	$this->dbutil->optimize_table('members_sessions');    
	redirect('admin/login');
   }
   
   
}

