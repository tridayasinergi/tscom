<?php

class Mfile_model extends CI_Model {
	function __construct()
	{
		
   	}
	
	
	function add_record() 
	{

		$this->keterangan   		= $this->input->post('keterangan'); 
		$this->kategori_id   	= $this->input->post('kategori_id'); 
		
		$this->publish   		= 1; 
		$this->upload_file(false);
		
		if($this->upload_img(false)){ 
			$this->db->insert('files', $this);
			return true;
		}
	}
	function upload_file($update = false){
		if(isset($_FILES['file_path'])){
			if($_FILES['file_path']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/files/';
				$config['allowed_types'] = 'bmp|jpg|jpeg|png|doc|docx|pdf|xls|xlsx|rtf|txt';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('file_path')){
					$image_upload = $this->upload->data();
					$this->file_path = $image_upload['file_name']; 
					$this->ukuran = formatSizeUnits($_FILES['file_path']['size']);
					// $data['size'] = formatSizeUnits($file['size']);
					// print_r($this);exit();
					
					// if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function upload_img($update = false){
		if(isset($_FILES['image_path'])){
			if($_FILES['image_path']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/files/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('image_path')){
					$image_upload = $this->upload->data();
					$this->image_path = $image_upload['file_name']; 
					// print_r($this);exit();
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/files/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/files/'.$row->img);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('muser');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->keterangan   		= $this->input->post('keterangan'); 
		$this->kategori_id   	= $this->input->post('kategori_id'); 
		$this->upload_file(false);
		
		if($this->upload_img(true)){ 
			$this->db->where('id',$id);
			$this->db->update('files',$this);
			return true;
		}
	}
	function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->update('mfile',array('publish' => 0));
		return true;
	}
	
	function get_kota()
	{
		$this->db->select('idkota,kota,propinsi');
		$this->db->from('mkota');
		$this->db->order_by('idkota','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function list_tipe()
	{
		$this->db->select('files_kategori.*');
		$this->db->from('files_kategori');
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_marketing()
	{
		$this->db->select('mmarketing.*');
		$this->db->from('mmarketing');
		$this->db->order_by('nama_marketing','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function get_mfile($id)
	{
		$this->db->select('files.*,files_kategori.nama_kategori');
		$this->db->from('files');
		$this->db->join('files_kategori','files_kategori.id=files.kategori_id');
		$this->db->where('files.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_identitas($id)
	{
		$this->db->select('mwarga.*');
		$this->db->from('mwarga');
		$this->db->where('mwarga.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function cek_mfile($id)
	{
		$this->db->select('mfile.*');
		$this->db->from('mfile');
		$this->db->where('mfile.username',$id);
		$this->db->where('mfile.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('files',array('publish' => $status));
		return true;
	}
}
