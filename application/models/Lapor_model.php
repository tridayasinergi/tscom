<?php
class Lapor_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	
	function perihal_list()
    {	
		$this->db->from('mperihal');
		
		$this->db->where('mperihal.publish',1);
		// $this->db->order_by('news.created_at','DESC');		
		// $this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function lapor_list()
    {	
		$this->db->select('qa.*,mrt.rt,mperihal.perihal');
		$this->db->from('qa');
		$this->db->join('mrt','mrt.id=qa.pengurus_id','LEFT');
		$this->db->join('mperihal','mperihal.id=qa.perihalid','LEFT');
		
		$this->db->where('qa.publish',1);
		$this->db->order_by('qa.id','DESC');		
		// $this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function simpan() 
	{
		$this->pertanyaan   	= $this->input->post('pertanyaan'); 
		$this->perihalid   	    = $this->input->post('perihalid'); 
		$this->pengurus_id   	= $this->input->post('pengurus_id'); 
		$this->email   	        = $this->input->post('email'); 
		$this->nama   	        = $this->input->post('nama'); 
		$this->db->insert('qa', $this);
		return true;
	}
	function RT_list()
    {	
		$this->db->from('mrt');
		
		$this->db->where('mrt.publish',1);
		$this->db->where('mrt.idrw',1);
		// $this->db->order_by('news.created_at','DESC');		
		// $this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function update_informasi($id,$jml)
    {	
		$jml_akses=$jml+1;
		$this->db->where('id',$id);
		$this->db->update('news',array('jml_akses' => $jml_akses));
    }
	function news_all()
    {	
		$this->db->from('news');
		
		$this->db->where('news.publish',1);
		$this->db->order_by('news.created_at','DESC');		
		// $this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function gallery_list()
    {	
		$this->db->from('gallery');
		
		$this->db->where('gallery.publish',1);
		$this->db->order_by('gallery.created_at','DESC');		
		$this->db->limit(5);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function content_list()
    {	
		$this->db->from('content');
		
		$this->db->where('content.publish',1);
		$this->db->where('content.kategori <>',7);
		$this->db->order_by('content.created_at','DESC');		
		$this->db->limit(10);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function get_news($id)
    {	
		$this->db->from('news');
		$this->db->where('news.id',$id);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->row();
    }
	function gallery_detail($id='0')
    {	
		$this->db->select('gallery_detail.*,gallery.nama_gallery');
		$this->db->from('gallery_detail');
		$this->db->join('gallery','gallery.id=gallery_detail.gallery_id','LEFT');
		if ($id !='0'){
		$this->db->where('gallery_detail.gallery_id',$id);
		}
		$this->db->order_by('gallery_detail.gallery_id','ASC');
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->result();
    }
	function get_content($id)
    {	
		$this->db->select('content.*,mkategori.kategori as nama_kategori');
		$this->db->from('content');
		$this->db->join('mkategori','mkategori.id=content.kategori','LEFT');
		$this->db->where('content.id',$id);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->row();
    }
	function get_informasi($id)
    {	
		$this->db->select('news.*,mkategori.kategori as nama_kategori');
		$this->db->from('news');
		$this->db->join('mkategori','mkategori.id=news.kategori','LEFT');
		$this->db->where('news.id',$id);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->row();
    }
	function get_gallery($id)
    {	
		$this->db->select('gallery.*,mkategori.kategori as nama_kategori');
		$this->db->from('gallery');
		$this->db->join('mkategori','mkategori.id=gallery.kategori','LEFT');
		$this->db->where('gallery.id',$id);
		$query = $this->db->get();	
		// print_r($this->db->last_query());exit();
		return $query->row();
    }
}

?>