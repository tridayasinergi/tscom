<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/info" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {judul}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/info/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_perusahaan">Perusahaan<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="nama_perusahaan" id="nama_perusahaan" placeholder="" value="{nama_perusahaan}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="email">Email<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="email" id="email" placeholder="" value="{email}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="telepon">Telepon<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="telepon" id="telepon" placeholder="" value="{telepon}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="fax">Fax<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="fax" id="fax" placeholder="" value="{fax}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="cs1">Whatsapp Title<span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="cs1_judul" id="cs1_judul" placeholder="" value="{cs1_judul}" />					
				</div>
				<label class="col-md-2 control-label" for="cs1">Whatsapp<span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input type="text" class="form-control input-sm" name="cs1" id="cs1" placeholder="" value="{cs1}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="cs1">Whatsapp 2  Title<span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="cs2_judul" id="cs2_judul" placeholder="" value="{cs2_judul}" />					
				</div>
				<label class="col-md-2 control-label" for="cs2">Whatsapp 2<span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input type="text" class="form-control input-sm" name="cs2" id="cs2" placeholder="" value="{cs2}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="fb">Facebook Title<span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="fb_judul" id="fb_judul" placeholder="" value="{fb_judul}" />					
				</div>
				<label class="col-md-2 control-label" for="fb">Facebook Link<span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input type="text" class="form-control input-sm" name="fb" id="fb" placeholder="" value="{fb}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="ig">Instagram Title<span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="ig_judul" id="ig_judul" placeholder="" value="{ig_judul}" />					
				</div>
				<label class="col-md-2 control-label" for="ig">Instagram Link<span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input type="text" class="form-control input-sm" name="ig" id="ig" placeholder="" value="{ig}" />					
				</div>					
			</div>		
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="youtube">Youtube Title<span class="text-danger">*</span></label>
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="youtube_judul" id="youtube_judul" placeholder="" value="{youtube_judul}" />					
				</div>
				<label class="col-md-2 control-label" for="youtube">Youtube Link<span class="text-danger">*</span></label>
				<div class="col-md-5">
					<input type="text" class="form-control input-sm" name="youtube" id="youtube" placeholder="" value="{youtube}" />					
				</div>					
			</div>				
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_perusahaan">Header 1<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control" id="info1" name="info1" rows="16" placeholder="Informasi di menu Home..."><?=$info1?></textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_perusahaan">Header 2<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control" id="info2" name="info2" rows="16" placeholder="Informasi di menu Home..."><?=$info2?></textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_perusahaan">Header 3<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control" id="info3" name="info3" rows="16" placeholder="Informasi di menu Home..."><?=$info3?></textarea>
				</div>					
			</div>
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/info" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript" src="{tinymce_path}tiny_mce.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
  
  $(document).ready(function(){
		// alert('sini');
		$('.js-select2').select2();
		tinyMCE.init({
			// General options
			// mode : "textareas",
			selector: '#info1,#info2,#info3',
			theme : "advanced",
			plugins : "imagemanager,filemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,insertfile,insertimage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			
		});
    })
  
	
</script>