<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/page" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {judul}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/page/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
						
			<?php 
				if($img_pages == '' ){ 
					$img_pages='default.jpg';							
				}
				// $a=file_exists($page_path.$img_pages);
				// echo $a;
				if(file_exists('./assets/upload/pages/'.$img_pages)) {
					
				}else{
					$img_pages='default.jpg';							
				}
			?>
			<div class="form-group">
			<label class="col-md-2 control-label" for=""> </label>
				<div class="col-md-4">
					<img src="{page_path}<?php echo $img_pages; ?>" id="output_img" class="img-polaroid"  style="width:400px;height:200px;"/>
				</div>
			</div>
			<div class="form-group">
			<label class="col-md-2 control-label" for="img">Image </label>
				<div class="col-md-4">
					<input type="file" class="fileinput"  accept="image/*" onchange="loadFile_foto(event)" name="img_pages" value="{img_pages}">
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="kategori">Kategori <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select class="js-select2 form-control" id="kategori" name="kategori" size="1">
						<option value="" <?=($kategori==''?'selected':'')?>>- Pilih Kategori -</option>
						<option value="0" <?=($kategori=='0'?'selected':'')?>>Page Index</option>
						<?foreach($list_tipe as $row){?>
						<option value="<?=$row->id?>" <?=($kategori==$row->id?'selected':'')?>><?=$row->kategori?></option>
						<?}?>
					</select>
				</div>							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kategori">Author <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select id="author" name="author" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
						<option value="#">Silahkan Pilih Author</option>
						<?php foreach ($list_author as $row) { ?>
								<option value="<?php echo $row->id; ?>" <?=($author==$row->id?'selected':'')?>><?php echo $row->author_ref; ?></option>
						<?php } ?>
					</select>
				</div>							
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="title">Judul<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control" id="title" name="title" rows="2" placeholder="Judul..."><?=$title?></textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tgl_akad">Tgl Publish <span class="text-danger">(Opsi)</span></label>				
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="created_at" id="created_at" placeholder="" value="{created_at}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
					
				</div>
			</div>
			<div class="form-group" hidden>
				<label class="col-md-2 control-label" for="title">Sinopsis<span class="text-danger">*</span></label>
					<div class="col-md-10"><textarea class="form-control" id="sinopsis" name="sinopsis" rows="6" placeholder="Sinopsis..."><?=$sinopsis?></textarea>
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="title">Berita<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<?php 
						$opt = array('id' 		=> 'content',
									 'name' 	=> 'content',
									 'value' 	=> $content,
									 'style' 	=> 'width:100%;height:500px;',
									 'class' 	=> 'ke-content');
						
						echo form_textarea($opt) 
					?>
					
				</div>	
				
			</div>
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/page" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript" src="{tinymce_path}tiny_mce.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
  jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
  $(document).ready(function(){
		// alert('sini');
		$('.js-select2').select2();
		tinyMCE.init({
			// General options
			// mode : "textareas",
			selector: '#content',
			theme : "advanced",
			plugins : "imagemanager,filemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,insertfile,insertimage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			template_replace_values : {
				username : "admin",
				staffid : "123456"
			}
		});
		
    })
  
	
</script>