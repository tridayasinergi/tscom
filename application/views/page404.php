<!DOCTYPE html>
<? $assets_path= base_url().'assets/front/' ?>
<html lang="en">


<!-- Mirrored from slidesigma.com/themes/html/gardenia/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Jan 2021 06:08:12 GMT -->
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="#">
    <meta name="description" content="#">
    <title>{per_nama}</title>
    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$assets_path?>images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$assets_path?>images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$assets_path?>images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="<?=$assets_path?>images/favicon.ico">
    <link rel="shortcut icon" href="<?=$assets_path?>favicon.png">
    <!-- Bootstrap -->
    <link href="<?=$assets_path?>css/bootstrap.min.css" rel="stylesheet">
    <!-- Fontawesome -->
    <link href="<?=$assets_path?>css/font-awesome.css" rel="stylesheet">
    <!-- Icon Font -->
    <link href="<?=$assets_path?>css/font/flaticon.css" rel="stylesheet">
    <!-- Swiper Slider -->
    <link href="<?=$assets_path?>css/swiper.min.css" rel="stylesheet"> 
    <!-- Slick Slider -->
    <link href="<?=$assets_path?>css/slick.css" rel="stylesheet">
    <!-- Range Slider -->
    <link href="<?=$assets_path?>css/jquery-ui.css" rel="stylesheet">
    <!-- magnific popup -->
    <link href="<?=$assets_path?>css/magnific-popup.css" rel="stylesheet">
    <!-- Nice Select -->
    <link href="<?=$assets_path?>css/nice-select.css" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="<?=$assets_path?>css/style.css" rel="stylesheet">
    <!-- Custom Responsive -->
    <link href="<?=$assets_path?>css/responsive.css" rel="stylesheet">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;display=swap" rel="stylesheet">
    <!-- place -->
</head>
<body id="page-404">
    <!-- Start 404 -->
    <section>
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="404-img">
                <img src="<?=$assets_path?>images/404-img.png" alt="image">
              </div>
              <div class="caption">
                <h5 class="text-custom-white full-width">Opps! Halaman Tidak ditemukan.</h5>
                <a href="<?=base_url()?>" class="btn-first btn-border mt-4 text-custom-white">Back to home</a>
              </div>
            </div>
          </div>
        </div>
    </section>
    <!-- End 404 -->
    <!-- Place all Scripts Here -->
    <!-- jQuery -->
    <script src="<?=$assets_path?>js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="<?=$assets_path?>js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?=$assets_path?>js/bootstrap.min.js"></script>
    <!-- Range Slider -->
    <script src="<?=$assets_path?>js/jquery-ui.js"></script>
    <!-- Swiper Slider -->
    <script src="<?=$assets_path?>js/swiper.min.js"></script>
    <!-- Slick Slider -->
    <script src="<?=$assets_path?>js/slick.min.js"></script>
    <!-- Nice Select -->
    <script src="<?=$assets_path?>js/jquery.nice-select.js"></script>
    <!-- Jarallex -->
    <script src="<?=$assets_path?>js/jarallax.min.js"></script>
    <script src="<?=$assets_path?>js/jarallax-video.min.js"></script>
    <!-- magnific popup -->
    <script src="<?=$assets_path?>js/jquery.magnific-popup.min.js"></script>
    <!-- Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnd9JwZvXty-1gHZihMoFhJtCXmHfeRQg"></script>
    <!-- info box -->
    <script src="<?=$assets_path?>js/infobox.min.js"></script>
    <!-- Markerclusterer js -->
    <script src="<?=$assets_path?>js/markerclusterer.js"></script>
    <!-- Maps js -->
    <script src="<?=$assets_path?>js/custom-maps.js"></script>
    <!-- Custom Js -->
    <script src="<?=$assets_path?>js/custom.js"></script>
    <!-- /Place all Scripts Here -->
</body>

<!-- Mirrored from slidesigma.com/themes/html/gardenia/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Jan 2021 06:08:15 GMT -->
</html>