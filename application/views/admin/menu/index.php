<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>				
				<a href="{base_url}admin/menu" class="btn" data-toggle="tooltip" title="" data-original-title="Refresh"> <i class="fa fa-refresh"></i></a>
			</li>
           <li>
				<a href="{base_url}admin/menu/add_new" class="btn" data-toggle="tooltip" title="" data-original-title="Add Menu"> <i class="fa fa-plus"></i></a>
				
			</li>
           
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="index_list">
				<thead>
					<tr>                                    
						<th width="5%">#</th>
						<th width="5%">Level</th>
						<th width="15%">Menu</th>
						<th width="20%">URL </th>
						<th width="10%">Status</th>
						<th width="15%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>



<script src="{js_path}pages/base_index_datatable.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		get_index();
    })
	function get_index() {
		// alert('sini');
		
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 50,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}admin/menu/get_index/',
			type: "POST",
			dataType: 'json',
			data: {
				
			}
		},
		columnDefs: [
					// {"targets": [12,13], "visible": false },
					 {  className: "text-right", targets:[0] },
					 {  className: "text-center", targets:[1,4] },
					 { "width": "5%", "targets": [0] },
					 // { "width": "8%", "targets": [5,6,7,8] },
					 { "width": "15%", "targets": [5] }
					 // { "width": "15%", "targets": [2,13] }

					]
		});
	}

</script>