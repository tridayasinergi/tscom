<div class="content">
	<div class="row">
		<div class="col-sm-6 col-lg-3">
			<div class="block">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td class="bg-danger" style="width: 50%;">
								<div class="push-30 push-30-t">
									<i class="fa fa-newspaper-o fa-3x text-white-op"></i>
								</div>
							</td>
							<td style="width: 50%;">
								<div class="h1 font-w700"><span class="h2 text-muted"></span><a href="{base_url}admin/berita">+ <?=$jml_news?> </a></div>
								<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}admin/berita"> BERITA PUBLISH</a></div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3">
			<div class="block">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td style="width: 50%;">
								<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}admin/page">+ <?=$jml_pages?></a></div>
								<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}admin/page"> PAGES PUBLISH</a></div>
							</td>
							<td class="bg-success" style="width: 50%;">
								<div class="push-30 push-30-t">
									<i class="fa fa-object-ungroup fa-3x text-white-op"></i>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6 col-lg-3">
			<div class="block">
				<table class="block-table text-center">
					<tbody>
						<tr>
							<td class="bg-primary" style="width: 50%;">
								<div class="push-30 push-30-t">
									<i class="fa fa-file-picture-o fa-3x text-white-op"></i>
								</div>
							</td>
							<td style="width: 50%;">
								<div class="h1 font-w700"><span class="h2 text-muted"></span> <a href="{base_url}admin/gallery">+ <?=$jml_gal?></a></div>
								<div class="h5 text-muted text-uppercase push-5-t"><a href="{base_url}admin/gallery">GALLERY </a></div>
							</td>
							
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>