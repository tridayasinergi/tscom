<?php

class Berita_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('news.*,kategori.kategori');
		$this->datatables->from('news');
		$this->datatables->join('kategori','kategori.id=news.kategori');
		
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$idtag   	        = $this->input->post('idtag'); 
		
		$this->title   	        = $this->input->post('title'); 
		$this->sinopsis   	    = $this->input->post('sinopsis'); 
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	    = $this->input->post('kategori'); 
		// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->created_at   	= Format_YMD($this->input->post('created_at'));
		$this->publish   		= 0; 
		$this->created_by   	= $this->session->userdata('user_id'); 
		$this->author   		= $this->input->post('author'); 
		if($this->upload_img_news (false)){ 
			$this->db->insert('news', $this);
			$idberita=$this->db->insert_id();
			foreach($idtag as $r=>$value){
				$data_detail=array(
					'idberita' => $idberita,
					'idtag' => $value,
				);
				$this->db->insert('news_tag',$data_detail);
			}
			return true;
		}else{
			// print_r('sini');
			return false;
		}
	}
	function upload_img_news ($update = false){
		// print_r(isset($_FILES['img_news']));exit();
		if(isset($_FILES['img_news'])){
			if($_FILES['img_news']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/news/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img_news']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img_news')){
					$image_upload = $this->upload->data();
					$this->img_news  = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/news/'.$this->img_news );			
					$this->myimage->resize(1920,800);
					$this->myimage->save('./assets/upload/news/'.$this->img_news );
					$this->myimage->resize(798,370);
					$this->myimage->save('./assets/upload/news/thumbs/'.$this->img_news );
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				// print_r('sini');
				return true;
			}
		}else{
			
				print_r('Sahabat');exit();
			return false;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img_news );exit();
		if(file_exists('./assets/upload/news/'.$row->img_news ) && $row->img_news  !='') {
			unlink('./assets/upload/news/'.$row->img_news );
			unlink('./assets/upload/news/thumbs/'.$row->img_news );
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('news');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$idtag                 = $this->input->post('idtag'); 
		$id                 = $this->input->post('id'); 
		$this->title   	    = $this->input->post('title'); 
		$this->sinopsis   	= $this->input->post('sinopsis'); 
		$this->content   	= replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	= $this->input->post('kategori'); 
		$this->created_at   = Format_YMD($this->input->post('created_at'));
		$this->author   	= $this->input->post('author');
		$this->updated_at   = date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->updated_by   = $this->session->userdata('userid'); 
		// $this->author   	= $this->session->userdata('tipe_user'); 
		// print_r($this);exit();
		if($this->upload_img_news (true)){ 
			$this->db->where('id',$id);
			$this->db->update('news',$this);
			
			$this->db->where('idberita',$id);
			$this->db->delete('news_tag');
			$idberita=$id;
			foreach($idtag as $r=>$value){
				$data_detail=array(
					'idberita' => $idberita,
					'idtag' => $value,
				);
				$this->db->insert('news_tag',$data_detail);
			}
			
			return true;
		}else{
			return false;
		}
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('news',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('kategori.*');
		$this->db->from('kategori');
		// $this->db->where('kategori.id',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_author()
	{
		$this->db->select('mauthor.*');
		$this->db->from('mauthor');
		$this->db->where('publish',1);
		$this->db->order_by('author_ref','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function list_tag($id='0')
	{
		$q="SELECT M.id,M.tag,CASE WHEN H.id IS NOT NULL THEN 'selected' ELSE '' END selected FROM mtags M
			LEFT JOIN news_tag H ON H.idberita='$id' AND H.idtag=M.id
			WHERE M.publish='1' ORDER BY M.tag ASC";
		$query=$this->db->query($q);
		return $query->result();
	}
	
	function get_news($id)
	{
		$this->db->select('news.*');
		$this->db->from('news');
		$this->db->where('news.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function cek_news($id)
	{
		$this->db->select('news.*');
		$this->db->from('news');
		$this->db->where('news.username',$id);
		$this->db->where('news.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
