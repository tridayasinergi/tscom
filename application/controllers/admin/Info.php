<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('info_model','info_model'); 
   }

	function index()
	{	
		$data=array();
		$data=$this->info_model->detail();
		// print_r($data);exit();
			$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("INFO",'admin\info'));
		
		$data['edit'] = '1';
		$data['error'] = '';
		$data['judul'] = 'INFO - Edit';
		$data['template'] = 'admin/info/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		if ($edit){
		// print_r($this->input->post());exit();
			// print_r($this->input->post('noid'));exit();
			
			if ($this->info_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/info");				
			}
		}
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'info/manage';
		$data['list_tipe'] = $this->info_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->info_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'admin\info',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("info",'admin\info',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'info - Edit');
			if($row = $this->info_model->get_news($id))
			{
				$data['id'] = $id;
				$data['sinopsis'] = $row->sinopsis;
				$data['content'] = $row->content;
				$data['img_news'] = $row->img_news ;
				$data['kategori'] = $row->kategori;
				$data['author'] = $row->author;
				$data['title'] = $row->title;
				$data['created_at'] = date_format(date_create($row->created_at),'d-m-Y');
				// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
				$data['edit'] = 1;
				// print_r($data['img_news']);exit();
			}
		
		// print_r($data);exit();
		$data['list_tipe'] = $this->info_model->list_tipe();
		$data['list_author'] 			= $this->info_model->list_author();
		if ($id){
			$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("NEWS",'admin\info'));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'admin\info',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['judul'] = 'NEWS - Edit';
		$data['template'] = 'admin/info/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->info_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/info");
		}
	}
}
?>