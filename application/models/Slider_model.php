<?php

class Slider_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('slider.*');
		$this->datatables->from('slider');
		
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		$this->urutan   	= $this->input->post('urutan'); 
		$this->title_in   	= $this->input->post('title_in'); 
		$this->kata1   	    = $this->input->post('kata1'); 
		$this->kata2   	    = $this->input->post('kata2'); 
		$this->kata3   	    = $this->input->post('kata3'); 
		$this->url   	    = $this->input->post('url'); 
		$this->publish   	= 0; 
		
		if($this->upload_img(false)){ 
			$this->db->insert('slider', $this);
			return true;
		}
	}
	function upload_img($update = false){
		if(isset($_FILES['img'])){
			if($_FILES['img']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/slider/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img')){
					$image_upload = $this->upload->data();
					$this->img = $image_upload['file_name']; 
					// print_r($this);exit();
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/avatars/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/avatars/'.$row->img);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('slider');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->urutan   	= $this->input->post('urutan'); 
		$this->title_in   	= $this->input->post('title_in'); 
		$this->kata1   	    = $this->input->post('kata1'); 
		$this->kata2    	= $this->input->post('kata2'); 
		$this->kata3   	    = $this->input->post('kata3'); 
		$this->url   	    = $this->input->post('url'); 
		
		if($this->upload_img(true)){ 
			$this->db->where('id',$id);
			$this->db->update('slider',$this);
			return true;
		}
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('slider',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('slider_tipe.*');
		$this->db->from('slider_tipe');
		$this->db->where('slider_tipe.id >=',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	
	function get_slider($id)
	{
		$this->db->select('slider.*');
		$this->db->from('slider');
		$this->db->where('slider.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function cek_slider($id)
	{
		$this->db->select('slider.*');
		$this->db->from('slider');
		$this->db->where('slider.username',$id);
		$this->db->where('slider.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
