<?php

class Mtags_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT T1.*,T2.menu as menu_parent,CONCAT(T2.parent_id,'.',COALESCE(T1.urutan,0)) as level from mtags T1
LEFT JOIN mtags T2 ON T2.parent_id=T1.id
			) as tbl";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        // $this->column_search   = array();
        // $this->column_order    = array();

        // $list = $this->datatable->get_datatables(true);
		
        // $this->datatables->select("mtags.*,parent.menu as menu_parent,CONCAT(mtags.parent_id,'.',COALESCE(mtags.urutan,0)) as level");
		// $this->datatables->from('mtags');
		// $this->datatables->join('mtags as parent','parent.id=mtags.parent_id','LEFT');
		
		$this->datatables->add_column('action', '', 'id');
		// print_r($this->datatables->last_query());exit();
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		$this->tag   	= $this->input->post('tag'); 
        $this->db->insert('mtags', $this);
        return true;
	}
	function upload_img($update = false){
		if(isset($_FILES['img'])){
			if($_FILES['img']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/mtags/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img')){
					$image_upload = $this->upload->data();
					$this->img = $image_upload['file_name']; 
					// print_r($this);exit();
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/avatars/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/avatars/'.$row->img);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('mtags');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->tag   	= $this->input->post('tag'); 
		
		$this->db->where('id',$id);
		$this->db->update('mtags',$this);
		return true;
	}
	function update_status($id,$status)
	{
		// print_r($id);exit();
		$this->db->where('id',$id);
		$this->db->update('mtags',array('publish' => $status));
		return true;
	}
	
	function list_parent()
	{
		// $this->db->select('mtags.*');
		// $this->db->from('mtags');
		// $this->db->where('mtags.parent_id',0);
		// $this->db->order_by('id','ASC');
		// $query = $this->db->get();
		// // print_r($this->db->last_query());exit();
		// return $query->result();
		$q="SELECT TBL.id,TBL.path,
				CASE WHEN TBL.level=1 THEN TBL.nama_level1
				 WHEN TBL.level=2 THEN TBL.nama_level2
				 WHEN TBL.level=3 THEN TBL.nama_level3
				END as menu,TBL.level,TBL.publish
				
			FROM (
			SELECT T1.id,T1.menu,1 as level,T1.url,T1.publish,T1.urutan,T1.menu as  nama_level1,null nama_level2 ,null nama_level3 
			,T1.urutan as path
			from mtags T1
			WHERE T1.parent_id=0 AND T1.publish='1'

			UNION ALL 

			SELECT T2.id,T2.menu,2 as level,T2.url,T2.publish,T2.urutan,T1.menu as  nama_level1,T2.menu nama_level2 ,null nama_level3
			,concat(T1.urutan,'-',T2.urutan) AS path
			from mtags T1
			INNER JOIN mtags T2 ON T1.id=T2.parent_id AND T2.publish='1'
			WHERE T1.parent_id =0


			) TBL ORDER BY TBL.path";
		return $this->db->query($q)->result();
	}
	
	function get_menu($id)
	{
		$this->db->select('mtags.*');
		$this->db->from('mtags');
		$this->db->where('mtags.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
}
