<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
   {
	   // print_r('SINI');exit();
   	  parent::__construct();
      if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
	  $this->load->model('dashboard_model','dashboard_model');
   }
   
	
	function index()
    {
		// print_r($this->session->userdata());exit();
        $data = array();
        $data['title']      = 'Dashboard';
        $data['jml_news']      = $this->dashboard_model->get_jml_berita();
        $data['jml_pages']      = $this->dashboard_model->get_jml_page();
        $data['jml_gal']      = $this->dashboard_model->get_jml_gallery();
        $data['template']    = 'admin/dashboard';
		
        $data['breadcrum']  = array(
                                array("Dashboard",'#')
                              );
							  
		
        $data = array_merge($data, backend_info());
        $this->parser->parse('admin/module_template', $data);
    }
	
}
