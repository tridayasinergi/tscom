<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('slider_model','slider_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Slider';
		$data['template']    = 'admin/slider/index';
		$data['ajax_url'] = 'admin/slider/getDataList'; 
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("Slider",'#'),
								array("List",'slider')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	
	
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
		$title_in     		= $this->input->post('title_in');
		$publish     		= $this->input->post('publish');
		
		$where='';
		if ($title_in !=''){
			$where .=" AND title_in LIKE '%".$title_in."%'";
		}
		
		if ($publish !='#'){
			$where .=" AND publish = '".$publish."'";
		}
		
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *from slider WHERE id is not null ".$where."
			
			) as T ORDER BY urutan ASC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/slider/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->title_in;
			$img='<img src="'.base_url().'assets/upload/slider/'.$r->img.'" id="output_img" class="img-polaroid"  style="width:200px;height:80px;"/>';
            $row[] = $img;
            // $row[] = $r->img;

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			$aksi .= '<a href="'.base_url().'assets/upload/slider/'.$r->img.'" target="_blank" type="button"  title="Photo" class="btn btn-xs btn-info"><i class="fa fa-file-picture-o"></i></a>';		
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Unpublish" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
				// $aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Slider - Addnew');
		$data['id'] = "";
		$data['urutan'] = "";
		$data['title_in'] = "";
		$data['img'] = "";
		$data['content_in'] = "";
		$data['created_at'] = "";
		$data['publish'] = "0";
		$data['updated_at'] = "";
		$data['error'] = "";
		$data['edit'] = "0";
		
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("Add New Slider",'#'),
								array("List",'admin\slider')
								);
		
		$data['title'] = 'Slider - Input Baru';
		$data['template'] = 'admin/slider/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->slider_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/slider/editor/$id/");				
			}
		}else{		
			if ($this->slider_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/slider/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'slider/manage';
		$data['list_tipe'] = $this->slider_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->slider_model->error_message;
		if($id==''){
			$data['title'] = 'Add Sliders';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Slider",'slider',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Sliders';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Slider",'slider',"point_right"),array("Edit Slider",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Slider - Edit');
			if($row = $this->slider_model->get_slider($id))
			{
				$data['id'] = $id;
				$data['urutan'] = $row->urutan;
				$data['kata1'] = $row->kata1;
				$data['kata2'] = $row->kata2;
				$data['kata3'] = $row->kata3;
				$data['url'] = $row->url;
				$data['img'] = $row->img;
				$data['title_in'] = $row->title_in;
				$data['edit'] = 1;
				
			}
		
		// print_r($data);exit();
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("Slider",'#'),
								array("Edit",'admin\slider')
								);
		$data['error'] = '';
		$data['title'] = 'Slider - Edit';
		$data['template'] = 'admin/slider/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->slider_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/slider");
		}
	}
}
?>