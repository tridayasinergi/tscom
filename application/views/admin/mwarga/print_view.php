<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Surat Pengantar Wargakami </title>
    <style>
    body {
      -webkit-print-color-adjust: exact;
    }
    @media screen  {
		 html, body {
			height: 95%; 
			margin: 10 10 10 10;
			 }
   
      table {
		 
		font-family: "Arial", Verdana, sans-serif;
        font-size: 11px !important;
        border-collapse: collapse !important;
        width: 100% !important;
      }
	  p{
		font-family: "Arial", Verdana, sans-serif;
        font-size: 11px !important;
	  }
      h5{
		font-family: "Courier New", Verdana, sans-serif;
        font-size: 11px !important; 
		font-weight:normal
	  }
      td {
        padding: 3px;
		 
      }
	  .header {
		 font-family: "Arial"!important; 
		 font-weight: bold;
		 margin: 0px;
		 padding: 0px;
      }
      .alasan {
		 font-family: "Arial"!important; 
		 font-weight: bold;
		 margin: 0px;
		 padding: 0px;
      }
      .header td{
         font-family: "Arial"!important;
		 margin: 0px;
		 padding: 0px;
      }
      

      /* border-normal */
      .border-full {
        border: 1px solid #000 !important;
		
      }
	  .text-header{
		font-size: 15px !important;
		font-weight: bold !important;
		
      }
      .text-keperluan{
		font-size: 15px !important;
		font-weight: bold !important;
		
      }
      .border-bottom {
        border-bottom:1px solid #000 !important;
      }
	  .border-bottom5 {
        border-bottom:3px solid #000 !important;
      }
	  .border-bottom-top {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
      }
	  .border-bottom-top-left {
        border-bottom:1px solid #000 !important;
        border-top:1px solid #000 !important;
		border-left:1px solid #000 !important;
      }
	  .border-left {
        border-left:1px solid #000 !important;
      }

      /* border-thick */
      .border-thick-top{
        border-top:2px solid #000 !important;
      }
      .border-thick-bottom{
        border-bottom:1px dotted #000 !important;
      }

      .border-dotted{
        border-width: 1px;
        border-bottom-style: dotted;
      }

      /* text-position */
      .text-center{
        text-align: center !important;
      }
      .text-left{
        text-align: left !important;
      }
      .text-right{
        text-align: right !important;
      }

      /* text-style */
      .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
		
      }
	  fieldset {
			border:1px solid #000;
			border-radius:8px;
			box-shadow:0 0 10px #000;
		}
		legend {
			background:#fff;
		}
    }
	@media print {
		html, body {
			height: 99%;    
		}
		 .text-italic{
        font-style: italic;
      }
      .text-bold{
        font-weight: bold;
      }
	}
		</style>
		<script type="text/javascript">
			
    </script>
  </head>
  <body>

  
	<div id="form" style="width:100%;">
	   <fieldset style="border:1px solid #000 !important;border-radius:0px;">
		  <legend style="font-weight:bold;"></legend>
    <table class="header">
      <tr>
        <td width="10%" class="border-bottom5" rowspan="3" align="center"><img src="<?php echo $slip_path;?>" alt="some text" width="80" height="70"></td>
		<td width="90%" align="center" class="text-header" >RUKUN TETANGGA (RT) <?=$rt?> RUKUN WARGA (RW) <?=$rw?></td>
      </tr>
	  <tr>
		<td  align="center" class="text-header"><strong>DESA CITAPEN</strong></td>
      </tr>
	  <tr>
		<td class="border-bottom5 text-header" align="center">KECAMATAN CIHAMPELAS KABUPATEN BANDUNG BARAT</td>
      </tr>
    </table><br style="line-height:5px">
	<table>
      <tr>
        <td width="10%" align="center"></td>
		<td width="90%" align="center" class="text-header" ><u>SURAT PENGANTAR</u></td>
      </tr>
	  <tr>
        <td width="10%" align="center"></td>
		<td width="90%" align="center"  style="font-size:12px!important;"><strong>Nomor : <?=$ket_surat?></strong></td>
      </tr>	  
    </table>
	<br style="line-height:0px">
	  <p>Ketua Rukun Tetangga <?=$rt?> / <?=$rw?> Desa Citapen Kecamatan Cihampelas Kabupaten Bandung Barat, dengan ini menerangkan bahwa :</p>
	  <table>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Nomor NIK</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom" width="68%"><?=$nik?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Nomor KK</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom" width="68%"><?=$nokk?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Nama Lengkap</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$nama?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Tempat, Tgl Lahir</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$tempat_lahir?>, <?=human_date($tgl_lahir)?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Jenis Kelamain</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$jenis_kelamin?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Agama</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$agama?></td>
		 </tr>	
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Kewarganegaraan</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$warga_negara?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Pekerjaan</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$pekerjaan?></td>
		 </tr>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="20%">Alamat Lengkap</td>
			<td class="" width="2%">:</td>
			<td class="border-thick-bottom"><?=$alamat_lengkap?></td>
		 </tr>
	  </table>
	   
	<table>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="90%">Orang tersebut diatas adalah benar warga / masyarakat kami, dan bermaksud membuat</td>
			
		 </tr>
		
	</table>
	<table class="alasan">
		 <tr>
			<td class="" width="10%"></td>
			<td class="border-dotted" width="90%" style="font-size:11px!important;font-style: italic!important;"><b><i><?=$keperluan?><i><b></td>
			
		 </tr>		
	</table>
	<table>
		 <tr>
			<td class="" width="10%"></td>
			<td class="" width="90%">Demikian surat pengantar ini dibuat dengan sebenarnya dan dijadikan bahan pertimbangan lebih lanjut.</td>
			
		 </tr>
		
	</table>
	
	<table>
		<tr>
			<td class="text-right">
				Citapen, <?=tgl_indo($input_date)?>
				
			</td>
		</tr>
	</table>
	<table>
		<tr>
			<td class="text-center" width="50%">Ketua RW, <?=$rw?></td>
			<td class="text-center" width="50%">Ketua RT, <?=$rt?></td>
		</tr>
		<tr>
			<td class="text-center" width="50%"><img src="<?php echo $ttd_path.$ttd_rw;?>" alt="some text" width="80" height="50"></td>
			<td class="text-center" width="50%"><img src="<?php echo $ttd_path.$ttd_rt;?>" alt="some text" width="80" height="50"></td>
		</tr>
		<tr>
			<td class="text-center" width="50%">(<?=$nama_rw?>)</td>
			<td class="text-center" width="50%">(<?=$nama_rt?>)</td>
		</tr>
		
	</table>
	</fieldset>
	</div>
	
  </body>
</html>
