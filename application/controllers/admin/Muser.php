<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muser extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('muser_model','muser_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All User';
		$data['template']    = 'admin/muser/index';
		$data['ajax_url'] = 'admin/muser/getDataList'; 
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("User",'#'),
								array("List",'muser')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
	
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT M.id,M.nama_user,M.username,T.tipe_user,M.publish FROM muser M
					LEFT JOIN muser_tipe T ON T.id=M.user_tipe_id
			) as T ORDER BY id DESC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/muser/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->nama_user;
            $row[] = $r->tipe_user;
            $row[] = $r->username;

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'User - Addnew');
		$data['nik'] = "";
		$data['nama'] = "";
		$data['idwarga'] = "";
		$data['nama_user'] = "";
		$data['passtext'] = "";
		$data['username'] = "";
		$data['user_tipe_id'] = "";
		$data['email'] = "";
		$data['img'] = "";
		$data['publish'] = "1";
		$data['error'] = "";
		$data['edit'] = "0";
		$data['id'] = "0";
		$data['list_tipe'] = $this->muser_model->list_tipe();
		
		$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Input Baru",'#',"active"));
		
		$data['title'] = 'User - Input Baru';
		$data['template'] = 'admin/muser/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->muser_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/muser/editor/$id/");				
			}
		}else{		
			if ($this->muser_model->cek_muser($this->input->post('username'))){
				if ($this->muser_model->add_record()){
					$this->session->set_flashdata('confirm', true);
					$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
					redirect("admin/muser/add_new/");
				}
			}else{
				$this->session->unset_userdata('searchkey');		
				$this->session->set_userdata('page', 'User - Addnew');
				$data['idwarga'] = $this->input->post('idwarga');
				$data['nama_user'] = $this->input->post('nama_user');
				$data['passtext'] = $this->input->post('passtext');
				$data['username'] = $this->input->post('username');
				$data['user_tipe_id'] = $this->input->post('user_tipe_id');
				$data['email'] = $this->input->post('email');
				$data['img'] = $this->input->post('img');
				$data['publish'] = "1";
				$data['error'] = "User Name Sudah Digunakan Oleh User Lain";
				$data['edit'] = "0";
				$data['id'] = "0";
				$data['list_tipe'] = $this->muser_model->list_tipe();
				$row=$this->muser_model->get_identitas($this->input->post('idwarga'));
				if ($row){
					$data['nik'] = $row->nik;
					$data['nama'] = $row->nama;
				}else{
					$data['nik'] ='';
					$data['nama'] = '';
				}
				$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Input Baru",'#',"active"));
				
				$data['title'] = 'User - Input Baru';
				$data['template'] = 'muser/manage';
				$data = array_merge($data, backend_info());
				$this->parser->parse('admin/module_template', $data);
			}
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'muser/manage';
		$data['list_tipe'] = $this->muser_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->muser_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'User - Edit');
			if($row = $this->muser_model->get_muser($id))
			{
				$data['id'] = $id;
				$data['nama_user'] = $row->nama_user;
				$data['img'] = $row->img;
				$data['user_tipe_id'] = $row->user_tipe_id;
				$data['idwarga'] = $row->idwarga;
				$data['email'] = $row->email;
				$data['username'] = $row->username;				
				$data['password'] = $row->passtext;				
				$data['password2'] = $row->passtext;				
				$data['passtext'] = $row->passtext;				
				$data['list_tipe'] = $this->muser_model->list_tipe();
				$data['edit'] = '1';
				
			}
		
		// print_r($data);exit();
		
		if ($id){
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['title'] = 'User - Edit';
		$data['template'] = 'admin/muser/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function profile($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'User - Edit');
			if($row = $this->muser_model->get_muser($id))
			{
				$data['id'] = $id;
				$data['nama_user'] = $row->nama_user;
				$data['img'] = $row->img;
				$data['user_tipe_id'] = $row->user_tipe_id;
				$data['idwarga'] = $row->idwarga;
				$data['email'] = $row->email;
				$data['username'] = $row->username;				
				$data['password'] = $row->passtext;				
				$data['password2'] = $row->passtext;				
				$data['passtext'] = $row->passtext;				
				$data['list_tipe'] = $this->muser_model->list_tipe();
				$data['edit'] = '1';
				
			}
		
		// print_r($data);exit();
		
		if ($id){
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'muser',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['title'] = 'User - Edit';
		$data['template'] = 'admin/muser/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function delete($id){
		if ($this->muser_model->delete($id)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah Dihapus.');
			redirect("admin/muser");
		}
	}
	function update_status($id,$status){
		
		if ($this->muser_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/muser");
		}
	}
}
?>