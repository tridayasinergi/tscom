<div class="topbar bg-custom-black">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="left-side ">
					<ul class="custom">
						<li>
							<a href="https://www.tridayasinergi.co/" target="_blank" class="text-custom-white" ><i class="fas fa-user text-custom-blue"></i> Login Member</a>
						</li>
						<? if ($per_cs1){ ?>
						<li>
							<a href="https://wa.me/<?=$per_cs1?>?text=Hi, PT. Tridaya Sinergi indonesia" target="_blank" class="text-custom-white"><i class="fab fa-whatsapp text-custom-blue"></i> <?=$per_cs1_judul?></a>
						</li>
						<?}?>
						
					</ul>
				</div>
			</div>
			<div class="col-md-4">
				<div class="right-side">
					<ul class="custom">
						<?if ($per_fb){?>
						<li>
						<a href="{per_fb}" class="text-custom-white" title="{per_fb_judul}"><i class="fab fa-facebook-f"></i></a>
						</li>	
						<?}?>	
						<? if ($per_ig){ ?>
						<li>
							<a href="{per_ig}" class="text-custom-white" title="{per_ig_judul}"><i class="fab fa-instagram"></i></a>
						</li>
						<?}?>
						<? if ($per_cs2){ ?>
						<li>
							<a href="https://wa.me/<?=$per_cs2?>?text=Hi, PT. Tridaya Sinergi indonesia" target="_blank" title="{per_cs2_judul}" class="text-custom-white"><i class="fab fa-whatsapp"></i></a>
						</li>
						<?}?>
						<? if ($per_youtube){ ?>
						<li>
							<a href="{per_youtube}" target="_blank" class="text-custom-white" title="{per_youtube_judul}"><i class="fab fa-youtube"></i></a>
						</li>
						<?}?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<header class="menu-style">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="header">
					<div class="logo">
						<a href="{base_url}">
							<img src="<?=$assets_path?>logo.png" class="img-fluid image-fit" alt="Logo">
						</a>
					</div>
					<div class="right-side">
						<div class="navigation">
							<div class="logo">
								<a href="{base_url}">
									<img src="<?=$assets_path?>logo.png" class="img-fluid image-fit" alt="Logo">
								</a>
							</div>
							<nav>
								<ul class="custom main-menu">
									<? foreach (get_menu_header(0) as $row){?>
										<li class="menu-item <?=$row->jml_downline !=0 ?'menu-item-has-children':'' ?>">
											<a href="{base_url}<?=$row->url?>" class="text-theme fs-14"><?=$row->menu?></a>
											<?if ($row->jml_downline > 0){?>
												<ul class="custom sub-menu">
												<? foreach (get_menu_header($row->id) as $lv1){?>
													<li class="menu-item <?=$lv1->jml_downline !=0 ?'menu-item-has-children':'' ?>">
														<a href="<?=(strpos($lv1->url,"ttp")>0 ? '':$base_url)?><?=$lv1->url?>" class="text-theme"><?=$lv1->menu?></a>
														<?if ($lv1->jml_downline > 0){?>
															<ul class="custom sub-menu">
															<? foreach (get_menu_header($lv1->id) as $lv2){?>
																<li class="menu-item">
																	<a href="{base_url}<?=$lv2->url?>" class="text-theme"><?=$lv2->menu?></a>
																</li>
															<?}?>
															</ul>
														<?}?>
													</li>
													
												<?}?>
												</ul>
											<?}?>
										</li>
									<?}?>
									
								</ul>
							</nav>
							<div class="social-media">
								<ul class="custom">
									<?if ($per_fb){?>
										<li>
										<a href="{per_fb}" class="text-custom-white" title="{per_fb_judul}" ><i class="fab fa-facebook-f"></i></a>
										</li>	
									<?}?>	
									<? if ($per_ig){ ?>
										<li>
											<a href="{per_ig}" class="text-custom-white" title="{per_ig_judul}" ><i class="fab fa-instagram"></i></a>
										</li>
									<?}?>
									<? if ($per_cs1){ ?>
										<li>
											<a href="https://wa.me/<?=$per_cs1?>?text=Hi, PT. Tridaya Sinergi indonesia" target="_blank" title="{per_cs1_judul}"  class="text-custom-white"><i class="fab fa-whatsapp"></i></a>
										</li>
									<?}?>
									<? if ($per_youtube){ ?>
										<li>
											<a href="{per_youtube}" target="_blank" class="text-custom-white" title="{per_youtube_judul}" ><i class="fab fa-youtube"></i></a>
										</li>
									<?}?>
								</ul>
							</div>
						</div>
						<div class="hamburger-menu">
							<div class="menu-btn">
								<span></span>
								<span></span>
								<span></span>
							</div>
						</div>
						<div class="cta-btn">
							<div class="cta-text">
								<a href="https://aktivasi.tridayasinergi.co/" class="addlisting-btn btn-first btn-border">Pendaftaran<span>+</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>