<?php

class Client extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//permission_basic_admin($this->session);
		$this->load->model('about_us_model','about_us_model');
		$this->load->model('home_model','home_model');
		$this->load->model('services_model','services_model');
		
	}
	function index(){
		$row = $this->about_us_model->get_about('about_us');
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['template'] = 'client';
			$data['menu'] = 'menu.we';
            $data['services_list']=$this->home_model->services_list();
			$data['client_list']=$this->home_model->client_list();
			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */