<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function pdf_create($html, $filename='', $stream=TRUE, $paper = "A4", $attach = false,$orientation = "portrait") 
{
    require_once("dompdf/dompdf_config.inc.php");
	// print_r('TES');exit();
    $dompdf = new DOMPDF();
    $dompdf->set_paper($paper, $orientation);
    $dompdf->load_html($html);
    $dompdf->render();
	
    if ($stream) {
        $dompdf->stream($filename.".pdf", array("Attachment" => $attach));
    } else {
        return $dompdf->output();
    }
}
function pdf_save($html, $filename='', $path='', $paper = "A4", $attach = false,$orientation = "portrait") 
{
    require_once("dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
    $dompdf->set_paper($paper, $orientation);
    $dompdf->load_html($html);
    $dompdf->render();
	$output = $dompdf->output();
	file_put_contents($path.$filename, $output);
	// print_r('daddd');exit();
	// return true;
    
}

function pdf_save_email($html, $filename='', $path='',$no_antrian='', $paper = "A4", $attach = false,$orientation = "portrait") 
{
    require_once("dompdf/dompdf_config.inc.php");

    $dompdf = new DOMPDF();
    $dompdf->set_paper($paper, $orientation);
    $dompdf->load_html($html);
    $dompdf->render();
    // $dompdf->get_canvas()->get_cpdf()->setEncryption($no_antrian, $no_antrian);
	$output = $dompdf->output();
	file_put_contents($path.$filename, $output);
    
}
?>