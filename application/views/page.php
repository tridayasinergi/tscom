<div class="subheader section-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="breadcrumb-wrapper">
					<div class="page-title">
						<h1 class="text-theme fw-500">{title}</h1>
					</div>
					<ul class="custom breadcrumb">
						<li>
							<a href="{base_url}">Home - Page</a>
						</li>
						<li class="active">
							{title}
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<section class="section-padding blog-details">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<!-- article -->
				<article class="post">
					<div class="post-wrapper">
						
						<div class="blog-meta">
							
							<?=$content?>
						</div>
					</div>
				</article>
				<!-- article -->
				<hr>
				
			</div>
		</div>
	</div>
</section>