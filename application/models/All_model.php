<?php
defined('BASEPATH') or exit('No direct script access allowed');

class All_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
	function get_propinsi(){
		$this->db->select('mkota.propinsi');
		$this->db->from('mkota');
		$this->db->order_by('propinsi','ASC');
		$this->db->group_by('propinsi');
		$query=$this->db->get();
		return $query->result();
	}
	function get_kota_id($id)
	{
		$this->db->select('mkota.*');
		$this->db->from('mkota');
		$this->db->where('idkota',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_desa_id($id)
	{
		$this->db->select('mdesa.*,mkecamatan.kecamatan,mkota.kota,mkota.propinsi');
		$this->db->from('mdesa');
		$this->db->join('mkecamatan','mkecamatan.id=mdesa.idkecamatan','LEFT');
		$this->db->join('mkota','mkota.idkota=mdesa.idkota','LEFT');
		$this->db->where('mdesa.id',$id);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->row();
	}
	function get_rw_id($id)
	{
		$this->db->select('mrw.rw,mdesa.desa,mrw.iddesa,mrw.id,mdesa.idkecamatan,mkecamatan.kecamatan,mkota.kota,mkota.propinsi');
		$this->db->from('mrw');
		$this->db->join('mdesa','mdesa.id=mrw.iddesa','LEFT');
		$this->db->join('mkecamatan','mkecamatan.id=mdesa.idkecamatan','LEFT');
		$this->db->join('mkota','mkota.idkota=mdesa.idkota','LEFT');
		$this->db->where('mrw.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_kecamatan($idkota='',$idkecamatan=''){
		$this->db->select('mkecamatan.kecamatan,mkecamatan.id');
		$this->db->from('mkecamatan');
		if ($idkota != ''){
			$this->db->where('mkecamatan.idkota',$idkota);
		}
		if ($idkecamatan != ''){
			$this->db->where('mkecamatan.id',$idkecamatan);
		}
		$this->db->order_by('kecamatan','ASC');
		$this->db->group_by('id');
		$query=$this->db->get();
		return $query->result();
	}
	function get_desa($idkecamatan='',$iddesa='')
	{
		$this->db->select('mdesa.*,mkecamatan.kecamatan,mkota.kota,mkota.propinsi');
		$this->db->from('mdesa');
		$this->db->join('mkecamatan','mkecamatan.id=mdesa.idkecamatan');
		$this->db->join('mkota','mkota.idkota=mdesa.idkota');
		if ($idkecamatan != ''){
			$this->db->where('mdesa.idkecamatan',$idkecamatan);
		}
		if ($iddesa != ''){
			$this->db->where('mdesa.id',$iddesa);
		}
		$this->db->order_by('mdesa.desa');
		$query = $this->db->get();
		return $query->result();
	}
	function get_rw($iddesa='',$idrw='')
	{
		$this->db->select('mrw.rw,mdesa.desa,mrw.iddesa,mrw.id,mdesa.idkecamatan,mkecamatan.kecamatan,mkota.kota,mkota.propinsi');
		$this->db->from('mrw');
		$this->db->join('mdesa','mdesa.id=mrw.iddesa');
		$this->db->join('mkecamatan','mkecamatan.id=mdesa.idkecamatan');
			$this->db->join('mkota','mkota.idkota=mdesa.idkota');
			$this->db->where('mrw.publish',1);
		if ($iddesa != ''){
			$this->db->where('mrw.iddesa',$iddesa);
		}
		if ($idrw != ''){
			$this->db->where('mrw.id',$idrw);
		}
		$this->db->order_by('mrw.rw');
		$query = $this->db->get();
		return $query->result();
	}
	function get_rt($idrw='',$idrt='')
	{
		$this->db->select('mrt.*');
		$this->db->from('mrt');
		if ($idrw != ''){
			$this->db->where('mrt.idrw',$idrw);
		}
		if ($idrt != ''){
			$this->db->where('mrt.id',$idrt);
		}
		$this->db->order_by('mrt.rt');
		$query = $this->db->get();
		return $query->result();
	}
	function get_status_warga()
	{
		$this->db->select('warga_status.*');
		$this->db->from('warga_status');
		
		$this->db->order_by('warga_status.id');
		$query = $this->db->get();
		return $query->result();
	}
	function get_status_ekonomi()
	{
		$this->db->select('mstatus_ekonomi.*');
		$this->db->from('mstatus_ekonomi');
		
		$this->db->order_by('mstatus_ekonomi.urutan');
		$query = $this->db->get();
		return $query->result();
	}
	function get_kota($propinsi){
		$this->db->select('mkota.*');
		$this->db->where('mkota.propinsi',$propinsi);
		$this->db->from('mkota');
		$this->db->order_by('propinsi','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function find_kecamatan($idkota){
		$this->db->select('mkecamatan.*');
		$this->db->where('mkecamatan.idkota',$idkota);
		$this->db->from('mkecamatan');
		$this->db->order_by('kecamatan','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function find_desa($idkecamatan){
		$this->db->select('mdesa.*');
		$this->db->where('publish','1');
		$this->db->where('mdesa.idkecamatan',$idkecamatan);
		$this->db->from('mdesa');
		$this->db->order_by('desa','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function find_rw($iddesa){
		$this->db->select('mrw.*');
		$this->db->where('mrw.iddesa',$iddesa);
		$this->db->from('mrw');
		$this->db->order_by('rw','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function find_rt($idrw){
		$this->db->select('mrt.*');
		$this->db->where('mrt.idrw',$idrw);
		$this->db->where('mrt.publish',1);
		$this->db->from('mrt');
		$this->db->order_by('rt','ASC');		
		$query=$this->db->get();
		//print_r($query->result());exit();
		return $query->result();
	}
	function s2_client($seacrchText){
		$this->db->select("mclient.*");
		$this->db->from('mclient');
		$this->db->like('mclient.nama_client',$seacrchText);
		$this->db->or_like('mclient.nama_event',$seacrchText);
		$query = $this->db->get();
		return $query->result_array();
	}
	function s2_kota($searchText){
		$this->db->select("mkota.*");
		$this->db->from('mkota');
		if ($this->session->userdata('user_tipe_id')>=4){
			$this->db->where('mkota.idkota',$this->session->userdata('idkota'));
		}
		$this->db->where("(mkota.kota like '%$searchText%' OR mkota.propinsi like '%$searchText%')");
		// $this->db->or_like('mkota.propinsi',$searchText);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result_array();
	}
	function s2_warga($searchText){
		$this->db->select("mwarga.*");
		$this->db->from('mwarga');
		$this->db->like('mwarga.nik',$searchText);
		$this->db->or_like('mwarga.nama',$searchText);
		$query = $this->db->get();
		return $query->result_array();
	}
	function find_detail($id){
		// $this->db->select("id,nama,idagama,idpekerjaan,tempat_lahir,DATE_FORMAT(tgl_lahir,'%d-%m-%Y') as tgl_lahir,
		// jenis_kelamin,alamat_lengkap");
		// $this->db->from('mwarga');
		// $this->db->where('mwarga.id',$id);
		// $query = $this->db->get();
		// print_r($this->db->last_query());exit();
		$q="SELECT
				mwarga.id,mwarga.nik,mwarga.nama,mwarga.idagama,mwarga.idpekerjaan,mwarga.nokk,
				mwarga.tempat_lahir,DATE_FORMAT(mwarga.tgl_lahir, '%d-%m-%Y' ) AS tgl_lahir,
				mwarga.jenis_kelamin,mwarga.alamat_lengkap,mrw.rw,RW.nama as nama_rw,mrt.rt,RT.nama as nama_rt,
				mdesa.desa,mkecamatan.kecamatan,mkota.kota,mkota.propinsi,mwarga.idrt,mwarga.idrw,mrw.img as ttd_rw,mrt.img as ttd_rt  
			FROM mwarga 
				LEFT JOIN mrw ON mwarga.idrw=mrw.id
				LEFT JOIN mwarga RW ON RW.id=mrw.idwarga
				LEFT JOIN mrt ON mwarga.idrt=mrt.id
				LEFT JOIN mwarga RT ON RT.id=mrt.idwarga
				LEFT JOIN mdesa ON mwarga.iddesa=mdesa.id
				LEFT JOIN mkecamatan ON mwarga.idkecamatan=mkecamatan.id
				LEFT JOIN mkota ON mwarga.idkota=mkota.idkota		
			WHERE mwarga.id = '$id'";
			$query=$this->db->query($q);
		return $query->row();
	}
	function find_warga($searchText){
		$this->db->select("mwarga.*");
		$this->db->from('mwarga');
		if ($this->session->userdata('user_tipe_id')>=7){
			$this->db->where('mwarga.idrw',$this->session->userdata('idrw'));
		}
		if ($this->session->userdata('user_tipe_id')>=8){
			$this->db->where('mwarga.idrt',$this->session->userdata('idrt'));
		}
		$this->db->where('mwarga.publish',1);
		$this->db->where("(mwarga.nik LIKE '%$searchText%' OR mwarga.nama LIKE '%$searchText%')" );
		// $this->db->or_like('mwarga.nama',$searchText);
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result_array();
	}
	
	function get_event_bulan(){
		$this->db->select("order.id,order.nama_event,order.tgl_event,mwaktu.warna");
		$this->db->from('order');
		$this->db->join('mwaktu','mwaktu.id=order.waktu_id');
		$this->db->where('deleted',0);
		$query = $this->db->get();
		$result=$query->result();
		foreach($result as $row){
			$data[]=array(
				'id' =>$row->id,
				'title' =>$row->nama_event,
				'start' =>$row->tgl_event,
				'end' =>$row->tgl_event,
				'color' =>$row->warna,
			);
		}
		return $data;
		// print_r($data);exit();
		// return $query->result();
	}
	function get_event_waktu(){
		$this->db->select("mwaktu.*");
		$this->db->from('mwaktu');
		$query = $this->db->get();
		return $query->result();
		
		// return $query->result();
	}
}
