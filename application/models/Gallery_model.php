<?php

class Gallery_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('gallery.*,mkategori.kategori');
		$this->datatables->from('gallery');
		$this->datatables->join('mkategori','mkategori.id=gallery.kategori');
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		// $this->urutan   		    = $this->input->post('urutan'); 
		$this->nama_gallery   	    = $this->input->post('nama_gallery'); 
		$this->keterangan   	    = $this->input->post('keterangan',false); 
		$this->tanggal_gallery   	= Format_YMD($this->input->post('tanggal_gallery'));
		$this->tanggal_create   	= date('Y-m-d');
		$this->publish   		    = 0; 
		$this->deleted   		    = 0; 
		$this->db->insert('gallery', $this);
			return true;
	}
	function add() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$this->gallery_id   	= $this->input->post('id'); 
		if($this->upload_img(false)){ 
			$this->db->insert('gallery_detail', $this);
			return true;
		}
	}
	function upload_img($update = false){
		if(isset($_FILES['img'])){
			if($_FILES['img']['name'] != ''){
				// $config['upload_path'] = './assets/images/gallery/original/';
				$config['upload_path'] = './assets/upload/gallery/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img')){
					$image_upload = $this->upload->data();
					$this->img = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/gallery/'.$this->img);			
					// $this->myimage->resize(1280,960);
					$this->myimage->save('./assets/upload/gallery/'.$this->img);
					$this->myimage->scale(50);
					$this->myimage->save('./assets/upload/gallery/thumbs/'.$this->img);
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/gallery/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/gallery/'.$row->img);
			unlink('./assets/upload/gallery/thumbs/'.$row->img);
		}
		
	}
	function hapus_image($id){
		$row = $this->detail_gellery($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/img/gallery/'.$row->image_path) && $row->image_path !='') {
			unlink('./assets/img/gallery/'.$row->image_path);
			unlink('./assets/img/gallery/thumbs/'.$row->image_path);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('gallery');
        return $query->row();
    }
	function detail_gellery($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('gallery_detail');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->nama_gallery   	    = $this->input->post('nama_gallery'); 
		$this->keterangan   	    = $this->input->post('keterangan',false); 
		$this->tanggal_gallery   	= Format_YMD($this->input->post('tanggal_gallery'));
		// print_r($this);exit();
        $this->db->where('id',$id);
        $this->db->update('gallery',$this);
        return true;
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('gallery',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('kategori.*');
		$this->db->from('kategori');
		// $this->db->where('mkategori.id',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_detail($id)
	{
		$this->db->select('gallery_detail.*');
		$this->db->from('gallery_detail');
		$this->db->where('gallery_detail.gallery_id',$id);
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	
	function get_gallery($id)
	{
		$this->db->select('gallery.*');
		$this->db->from('gallery');
		$this->db->where('gallery.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function hapus_photo($id)
	{
		$this->hapus_image($id);
		
		$this->db->where('id', $id);
		$this->db->delete('gallery_detail');
		return true;
	}
	
	function cek_gallery($id)
	{
		$this->db->select('gallery.*');
		$this->db->from('gallery');
		$this->db->where('gallery.username',$id);
		$this->db->where('gallery.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
