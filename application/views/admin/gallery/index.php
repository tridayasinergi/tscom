<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block">
	<div class="block-header">
		<ul class="block-options">
			<li>				
				<a href="{base_url}admin/gallery" class="btn" data-toggle="tooltip" title="" data-original-title="Refresh"> <i class="fa fa-refresh"></i></a>
			</li>
           <li>
				<a href="{base_url}admin/gallery/add_new" class="btn" data-toggle="tooltip" title="" data-original-title="Add Gallery"> <i class="fa fa-plus"></i></a>
				
			</li>
           
		</ul>
		<h3 class="block-title">{title}</h3>
		<hr style="margin-top:25px">
        <div class="row">
            <?php echo form_open('tgudang_pengembalian/filter', 'class="form-horizontal" id="form-work"'); ?>
            <div class="col-md-6">
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Gallery</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="nama_gallery" placeholder="Judul" name="nama_gallery" value="">
                    </div>
                </div>
				<div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-4 control-label" for="tanggal">Keterangan Gallery</label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="keterangan" placeholder="Keterangan Gallery" name="keterangan" value="">
                    </div>
                </div>
				
                
                
            </div>
			<div class="col-md-6">
				<div class="form-group" style="margin-bottom: 15px;">
                    <label class="col-md-3 control-label" for="status">Status </label>
                    <div class="col-md-8">
                        <select id="publish" name="publish" class="js-select2 form-control" style="width: 100%;" data-placeholder="Choose one..">
                            <option value="#" selected>Semua Status</option>
                            <option value="1" >PUBLISHED</option>
                            <option value="0" >UNPUBLISH</option>
                        </select>
                    </div>
                </div>
				
                
                <div class="form-group" style="margin-bottom: 5px;">
                    <label class="col-md-3 control-label" for=""></label>
                    <div class="col-md-8">
                        <button class="btn btn-success text-uppercase" type="button" id="btn_cari" name="btn_cari" style="font-size:13px;width:100%;float:right;"><i class="fa fa-filter"></i> Filter</button>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="index_list">
				<thead>
					<tr>                                    
						<th width="5%">#</th>
						<th width="25%">Tanggal</th>
						<th width="20%">Gallery</th>
						<th width="15%">Keterangan</th>
						<th width="15%">Jumlah Foto</th>
						<th width="10%">Status</th>
						<th width="15%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<script src="{js_path}pages/base_index_datatable.js"></script>
<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		// alert('sini');
		// js-select2
		$('.js-select2').select2();
		get_index();
    })
	$(document).on("click","#btn_cari",function(){	
		get_index();
	});
	function get_index() {
		// alert('sini');
		var content=$("#keterangan").val();
		var title=$("#nama_gallery").val();
		var publish=$("#publish").val();
		$('#index_list').DataTable().destroy();
		var table = $('#index_list').DataTable({
		"pageLength": 10,
		"ordering": false,
		"processing": true,
		"serverSide": true,
		"autoWidth": false,
		"fixedHeader": true,
		"searching": false,
		"order": [],
		"ajax": {
			url: '{site_url}admin/gallery/get_index/',
			type: "POST",
			dataType: 'json',
			data: {
				content:content,title:title,publish:publish,
			}
		},
		columnDefs: [
					 {  className: "text-right", targets:[0] },
					 {  className: "text-left", targets:[1,4] },
					 { "width": "5%", "targets": [0] },
					 { "width": "10%", "targets": [1] },
					 { "width": "15%", "targets": [4] }

					]
		});
	}

</script>

