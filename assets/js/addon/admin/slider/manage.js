jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
$(document).ready(function(){
	$('.number').number(true, 0);
	$('#user_tipe_id').select2();
	$(document).on("click","#btn_simpan",function(){
		// alert('KESNI');
		if(!validasi_save_final()) return false;		
		
		

		$("#form1").submit();
	});
});
$("#idwarga").select2({
	
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'admin/All/s2_warga/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nik + ' - '+item.nama+'.',
						id: item.id
					}
				})
			};
		}
	}
});
$("#password").keyup(function(){
	$("#passtext").val($("#password").val());
});
function validasi_save_final(){
	
	if ($("#idwarga").val()==''){
		sweetAlert("Maaf...", "Silahkan Isi Identitas", "error");
		return false;	
	}	
	if ($("#username").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Username", "error");
		return false;	
	}
	if ($("#password").val() != $("#password2").val() ){
		sweetAlert("Maaf...", "Password Tidak Sama", "error");
		return false;	
	}
	
	return true;
	
}

$("#user_tipe_id").change(function(){
	
}); 

