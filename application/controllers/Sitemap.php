<?php

class Sitemap extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('page_model');
		$this->load->model('services_model');
		$this->load->model('client_model');
	}
	
	function diyenes()
	{
		$data = array();
		$data['news_list'] = $this->page_model->news_list();
		$data['services_list'] = $this->services_model->list_services_sitemap();
		$data['client_list'] = $this->client_model->list_client_sitemap();
		$this->output->set_content_type('text/xml');
		$this->parser->parse('sitemap',array_merge($data,basic_info()));
	}
    
    function news()
	{
		$data = array();
		$data['news_list'] = $this->page_model->news_list();
		$this->output->set_content_type('text/xml');
		$this->parser->parse('sitemap_news',array_merge($data,basic_info()));
	}	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */