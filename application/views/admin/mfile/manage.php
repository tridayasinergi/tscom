<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/mfile" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/mfile/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			
			<?php 
				if($image_path == '' ){ 
					$image_path='default.png';							
				}
				// $a=file_exists($slider_path.$img);
				// echo $a;
				if(file_exists('./assets/upload/files/'.$image_path)) {
					
				}else{
					$image_path='default.png';							
				}
			?>
			<div class="form-group">
			<label class="col-md-2 control-label" for=""> </label>
				<div class="col-md-4">
					<img src="{files_path}<?php echo $image_path; ?>" id="output_img" class="img-polaroid"  style="width:100px;height:100px;"/>
				</div>
			</div>
			<div class="form-group">
			<label class="col-md-2 control-label" for="img">Photo </label>
				<div class="col-md-4">
					<input type="file" class="fileinput"  accept="image/*" onchange="loadFile_foto(event)" name="image_path" value="{image_path}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="keterangan">Keterangan <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="keterangan" id="keterangan" placeholder="" value="{keterangan}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kategori_id">Kategori <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select class="form-control js-select2" id="kategori_id" name="kategori_id" size="1">
						<option value="" <?=($kategori_id==''?'selected':'')?>>- Kategori -</option>
						<?foreach($list_tipe as $row){?>
						<option value="<?=$row->id?>" <?=($kategori_id==$row->id?'selected':'')?>><?=$row->nama_kategori?></option>
						<?}?>
					</select>
				</div>							
			</div>
			<div class="form-group">
			<label class="col-md-2 control-label" for="img">File </label>
				<div class="col-md-4">
					<input type="file" class="fileinput" name="file_path" value="{file_path}">
				</div>
				<div class="col-md-6">
					<?if($file_path){?>
					<a href="{files_path}{file_path}" target="_blank">{file_path}</a>
					<?}?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/mfile"  class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
 
  $(document).ready(function(){
		$('.js-select2').select2();
		$(document).on("click","#btn_simpan",function(){
			// alert('KESNI');
			if(!validasi_save_final()) return false;		
			
			

			$("#form1").submit();
		});
    })
  
	$("#password").keyup(function(){
		$("#passtext").val($("#password").val());
	});
function validasi_save_final(){
	
	if ($("#idwarga").val()==''){
		sweetAlert("Maaf...", "Silahkan Isi Identitas", "error");
		return false;	
	}	
	if ($("#username").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Username", "error");
		return false;	
	}
	if ($("#password").val() != $("#password2").val() ){
		sweetAlert("Maaf...", "Password Tidak Sama", "error");
		return false;	
	}
	
	return true;
	
}
</script>