jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
$(document).ready(function(){
	$('.number').number(true, 2);	
	$("#idkk").select2();
	
	$("#idpekerjaan").select2();
	$("#status_nikah").select2();
	$("#idagama").select2();
	$("#jenis_kelamin").select2();
	$("#status_warga_id").select2();
	$("#idpendidikan").select2();
	$("#warga_negara").select2();
	create_no_surat();
	$(document).on("click","#btn_simpan",function(e){
		if(!validasi_save_final()) return false;		
	});
	
	$('#form1').on('submit', function(e) {		
		var form = this;
	});
});
$("#idwarga").select2({	
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'admin/All/find_warga/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nik + ' - '+item.nama+'.',
						id: item.id
					}
				})
			};
		}
	}
});
function create_no_surat(){	
	$.ajax({
		url: site_url+'admin/Mwarga/get_kode_surat/'+$("#idrt").val()+'/'+$("#kode_surat").val(),
		dataType: "json",
		success: function(data) {
			// alert(data);
			var no_surat="";
			var no_urut=data;
			no_surat=$("#kode_surat").val()+'. '+no_urut+' /RT'+$("#rt").val()+'/RW'+$("#rw").val()+'/'+$("#bulan").val()+'/'+$("#tahun").val();
			$("#ket_surat").val(no_surat);
		}
	});
	
	
}
$("#idwarga").change(function(){
	if ($(this).val()!=''){
		$.ajax({
			url: site_url+'admin/All/find_detail/'+$(this).val(),
			dataType: "json",
			success: function(data) {
				// alert(data.idrt);
				
				$('#idagama').val(data.idagama).trigger('change');
				$('#idpekerjaan').val(data.idpekerjaan).trigger('change');
				$('#jenis_kelamin').val(data.jenis_kelamin).trigger('change');
				$('#tempat_lahir').val(data.tempat_lahir);
				$('#tgl_lahir').val(data.tgl_lahir);
				$('#alamat_lengkap').val(data.alamat_lengkap);
				$('#rt').val(data.rt);
				$('#rw').val(data.rw);
				$('#nama_rw').val(data.nama_rw);
				$('#nama_rt').val(data.nama_rt);
				$('#nik').val(data.nik);
				$('#nama').val(data.nama);
				$('#idrt').val(data.idrt);
				$('#idrw').val(data.idrw);
				$('#nokk').val(data.nokk);
				$('#ttd_rt').val(data.ttd_rt);
				$('#ttd_rw').val(data.ttd_rw);
				create_no_surat();
				// $("#idagama").
			}
		});
	}

});
$("#kode_surat").change(function(){
	
	create_no_surat();
			

});

function allowSubmit(){
				alert('SINI BROOOO');
			
	swal({
			title: "Lanjutkan Input",
			text: "'Ya' dengan KK Baru, 'Tidak' Untuk Document Baru",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: '#96d25f',
			confirmButtonText: 'Ya',
			cancelButtonText: "Tidak",
			closeOnConfirm: true,
			closeOnCancel: true
		},
		function(isConfirm) {
			if (isConfirm) {
			  $(e.currentTarget).trigger(e.type, {'send': true});
			}
		}); 
}
function validasi_save_final(){
	// alert($("#status_kk").val());
	// alert($("#idkota").val());
	if ($("#idwarga").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Data Warga", "error");
		return false;	
	}
	
	if ($("#nik").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Data Warga", "error");
		return false;	
	}
	if ($("#nama").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Nama", "error");
		return false;	
	}
	if ($("#jenis_kelamin").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Jenis Kelamin", "error");
		return false;	
	}
	if ($("#tempat_lahir").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi tempat_lahir", "error");
		return false;	
	}
	if ($("#tgl_lahir").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Tanggal", "error");
		return false;	
	}
	
	if ($("#idagama").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Agama", "error");
		return false;	
	}
	if ($("#idpekerjaan").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Pekerjaan", "error");
		return false;	
	}
	if ($("#alamat_lengkap").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Alamat", "error");
		return false;	
	}
	if ($("#idrt").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi RT", "error");
		return false;	
	}
	if ($("#idrw").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi RW", "error");
		return false;	
	}
	if ($("#keperluan").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Isi Keperluan", "error");
		return false;	
	}
	
	return true;
	
}
function konfirmasi(){
	swal({
	  title: "Are you sure?",
	  text: "You will not be able to recover this imaginary file!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, delete it!",
	  cancelButtonText: "No, cancel plx!",
	  closeOnConfirm: false,
	  closeOnCancel: false
	},
	function(isConfirm) {
	  if (isConfirm) {
		swal("Deleted!", "Your imaginary file has been deleted.", "success");
		return true;
	  } else {
		return false;
		swal("Cancelled", "Your imaginary file is safe :)", "error");
	  }
	});
}


