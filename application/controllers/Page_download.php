<?php

class Page_download extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('page_download_model');
	}
	function index()
	{
		// print_r($str_key);exit();
		
		$data = array(
			'title' =>'Page Download',
		);	
		$data['list_kategori']=$this->page_download_model->get_list_kategori();
		if ($data){
			$data['template'] = 'page_download';			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		}else{
			$this->load->view('page404');
			// redirect('404');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */