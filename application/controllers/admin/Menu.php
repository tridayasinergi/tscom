<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('menu_model','menu_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Menu';
		$data['template']    = 'admin/menu/index';
		$data['ajax_url'] = 'admin/menu/getDataList'; 
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\menu'));
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	function get_index(){
	
		$iddistributor     		= $this->input->post('iddistributor');
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT TBL.id,TBL.path,
					CASE WHEN TBL.level=1 THEN TBL.nama_level1
					 WHEN TBL.level=2 THEN TBL.nama_level2
					 WHEN TBL.level=3 THEN TBL.nama_level3
					END as menu,TBL.level,TBL.publish,TBL.url
					
				FROM (
				SELECT T1.id,T1.menu,1 as level,T1.url,T1.publish,T1.urutan,T1.menu as  nama_level1,null nama_level2 ,null nama_level3 
				,T1.urutan as path
				from menu_front T1
				WHERE T1.parent_id=0 AND T1.publish='1'

				UNION ALL 

				SELECT T2.id,T2.menu,2 as level,T2.url,T2.publish,T2.urutan,T1.menu as  nama_level1,T2.menu nama_level2 ,null nama_level3
				,concat(T1.urutan,'-',T2.urutan) AS path
				from menu_front T1
				INNER JOIN menu_front T2 ON T1.id=T2.parent_id AND T2.publish='1'
				WHERE T1.parent_id =0

				UNION ALL 

				SELECT T3.id,T1.menu,3 as level,T1.url,T3.publish,T1.urutan,T1.menu as  nama_level1,T2.menu nama_level2,T3.menu  nama_level3
				,concat(T1.urutan,'-',T2.urutan,'-',T3.urutan) AS path
				from menu_front T1
				INNER JOIN menu_front T2 ON T1.id=T2.parent_id AND T2.publish='1'
				INNER JOIN menu_front T3 ON T2.id=T3.parent_id AND T3.publish='1'
				WHERE T3.parent_id !=0 



				) TBL ORDER BY TBL.path
			) as T ORDER BY path";
		// print_r($from);exit();
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array();
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/menu/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->level;
			$row[] = TreeView($r->level, $r->menu);
			
            $row[] = $r->url;
			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">ACTIVE</span>';
			}else{
				$row[] = '<span class="label label-danger">DELETED</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Aktifkan</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function getDataList()
    {
        $query = $this->menu_model->data_list(0);
        $this->output->set_output($query);
    }
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Menu - Addnew');
		$data['id'] = "";
		$data['urutan'] = "";
		$data['parent_id'] = "0";
		$data['menu'] = "";
		$data['url'] = "";
		$data['publish'] = "0";
		$data['error'] = "";
		$data['edit'] = "0";
		$data['list_parent'] = $this->menu_model->list_parent();
		
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\menu'));
		
		$data['title'] = 'Menu - Input Baru';
		$data['template'] = 'admin/menu/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->menu_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/menu/editor/$id/");				
			}
		}else{		
			if ($this->menu_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/menu/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'menu/manage';
		$data['list_tipe'] = $this->menu_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->menu_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'menu',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Menu",'menu',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Menu - Edit');
			if($row = $this->menu_model->get_menu($id))
			{
				$data['id'] = $id;
				$data['parent_id'] = $row->parent_id;
				$data['urutan'] = $row->urutan;
				$data['url'] = $row->url;
				$data['menu'] = $row->menu;
				$data['edit'] = 1;
				
			}
			$data['list_parent'] = $this->menu_model->list_parent();
		
		// print_r($data);exit();
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("MENU",'admin\menu'));
		$data['error'] = '';
		$data['title'] = 'Menu - Edit';
		$data['template'] = 'admin/menu/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->menu_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/menu");
		}
	}
}
?>