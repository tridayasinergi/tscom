<section class="section-padding bg-light-white our_articles">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-8">
				<? if ($list_berita){?>
					<div class="row">
					<? foreach($list_berita as $row){?>
						<!-- article -->
						<article class="post col-md-12 mb-xl-20">
							<div class="post-wrapper">
								<div class="blog-img animate-img">
									<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>">
										<img loading="lazy" src="{berita_path}<?=$row->img_news?>" class="img-fluid full-width" alt="blog">
									</a>
									<div class="property-type">
										<a href="#" class="property-tag-1 bg-theme text-custom-white"><?=$row->kategori?></a>
									</div>
									<div class="post-date">
										<a href="#"><?=tsi_date($row->created_at)?></a>
									</div>
								</div>
								<div class="blog-meta bg-custom-white padding-20">
									<h2 class="post-title"><a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="text-theme"><?=$row->title?></a></h2>
									<p class="text-light-white no-margin"><?=substr(strip_tags($row->sinopsis),0,160)?>...</p>
								</div>
								<div class="blog-footer-meta bg-custom-white padding-20">
									<div class="post-author">
										<div class="author-img">
											<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>">
												<img src="{assets_path}tsi.png" class="rounded-circle" alt="#">
											</a>
										</div>
										<span class="text-theme fs-14">By <a href="#" class="text-theme fw-500"><?=$row->author_ref?></a></span>
									</div>
									<div class="post-link">
										<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="link-btn text-theme fw-600 fs-14">Read More</a>
									</div>
								</div>
							</div>
						</article>
						<?}?>
						
					</div>
					<div class="row">
						<div class="col-12">
							<nav class="section-padding-top">
									{pagination}
							</nav>
						</div>
					</div>
				<?}else{?>
					<div class="row">
						<div class="col-12">
						<div class="caption text-center">
							<div class="icon-box">
                                <i class="flaticon-sprout"></i>
                            </div>
							<h5 class="text-custom-blue full-width">Opps! Konten tidak ditemukan. <a href="{base_url}page/berita/all" class="text-custom-blue"> Coba lagi</a></h5>
							<a href="{base_url}page/berita/all" type="button" class="btn-first btn-submit"><i class="fa fa-refresh"></i> Kembali</a>
						  </div>
						</div>
					</div>
				<?}?>
			</div>
			<? $this->load->view('inc_right') ?>
		</div>
	</div>
</section>