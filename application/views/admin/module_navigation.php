<?php 
	$menu = $this->uri->segment(2); 
	$menu2 = $this->uri->segment(2);
	$menu3 = $this->uri->segment(3);
	$user_permission_id=$this->session->userdata('user_tipe_id');
 ?>
<li>
	<a class="active" href="{base_url}admin/dashboard"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
</li>

<li  <?=($menu == 'page_video'||$menu == 'mfile'||$menu == 'page_testimoni'||$menu == 'slider'||$menu == 'berita'||$menu == 'page'||$menu == 'flash' ||$menu == 'pop_up' ||$menu == 'gallery'||$menu == 'menu'||$menu == 'mtags') ? "class=\"open\"" : ""; ?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-external-link"></i><span class="sidebar-mini-hide">Front End</span></a>
	<ul>				
		<li><a <?=menuIsActive2('menu')?> href="{base_url}admin/menu"><i class="fa fa-list-ol"></i>Menu </a></li>		
		<li><a <?=menuIsActive2('mtags')?> href="{base_url}admin/mtags"><i class="fa fa-tags"></i>Tags</a></li>		
		<li><a <?=menuIsActive2('page')?> href="{base_url}admin/page"><i class="fa fa-pagelines"></i>Page</a></li>		
		<li><a <?=menuIsActive2('pop_up')?> href="{base_url}admin/pop_up"><i class="si si-ban"></i>Pop Up</a></li>		
		<li><a <?=menuIsActive2('flash')?> href="{base_url}admin/flash"><i class="fa fa-newspaper-o"></i>News Flash</a></li>		
		<li><a <?=menuIsActive2('berita')?> href="{base_url}admin/berita"><i class="fa fa-newspaper-o"></i>Berita</a></li>		
		<li><a <?=menuIsActive2('gallery')?> href="{base_url}admin/gallery"><i class="fa fa-file-image-o"></i>Gallery</a></li>		
		<li><a <?=menuIsActive2('slider')?> href="{base_url}admin/slider"><i class="fa fa-file-video-o"></i>Slider</a></li>		
		<li><a <?=menuIsActive2('page_video')?> href="{base_url}admin/page_video"><i class="fa fa-youtube"></i>Sinergi TV</a></li>		
		<li><a <?=menuIsActive2('page_testimoni')?> href="{base_url}admin/page_testimoni"><i class="fa fa-youtube-play"></i>Testimoni</a></li>		
		<li><a <?=menuIsActive2('mfile')?> href="{base_url}admin/mfile"><i class="fa fa-file-text-o"></i>Upload Files</a></li>		
	</ul>
</li>

<li  <?=($menu == 'kategori'||$menu == 'author') ? "class=\"open\"" : ""; ?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-fire"></i><span class="sidebar-mini-hide">Referensi</span></a>
	<ul>				
		<li><a <?=menuIsActive2('kategori')?> href="{base_url}admin/kategori"><i class="fa fa-certificate"></i>Kategori </a></li>		
		<li><a <?=menuIsActive2('author')?> href="{base_url}admin/author"><i class="fa fa-chain"></i>Author </a></li>		
	</ul>
</li>
<li  <?=($menu == 'muser'||$menu == 'info') ? "class=\"open\"" : ""; ?>>
	<a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">Setting</span></a>
	<ul>				
		<li><a <?=menuIsActive2('info')?> href="{base_url}admin/info"><i class="fa fa-certificate"></i> Perusahaan </a></li>		
		<li><a <?=menuIsActive2('muser')?> href="{base_url}admin/muser"><i class="fa fa-chain"></i>User </a></li>		
	</ul>
</li>


