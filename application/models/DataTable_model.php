<?php defined('BASEPATH') or exit('No direct script access allowed');

class DataTable_model extends CI_Model
{
    /**
     * Server Side Method Binding.
     * Modified @gunalirezqimauludi
     */


    public function __construct()
    {
        parent::__construct();

    }

    private function _get_datatables_query($protect=false)
    {
        $_ci =& get_instance();
		if ($protect){
		$this->db->_protect_identifiers=false;
		}
        if (count($this->select) != 0) {
            $this->db->select($this->select);
        }

        if ($this->from != '') {
			// print_r($this->from);exit();
            $this->db->from($this->from,false);
        }


        if (isset($_ci->join)) {
            if (count($_ci->join) != 0) {
                foreach ($_ci->join as $item) {
                    $_ci->db->join($item[0], $item[1], $item[2]);
                }
            }
        }

        if (isset($_ci->where)) {
            if (count($_ci->where) != 0) {
                $this->db->where($_ci->where);
            }
        }

        if (isset($_ci->like)) {
            $this->db->like($_ci->like);
        }

        if (isset($_ci->where_not_in)) {
            if (count($_ci->where_not_in) > 0) {
                foreach($_ci->where_not_in as $key => $value){
                    $this->db->where_not_in($key ,$value);
                }
            }
        }

        if (isset($_ci->where_in)) {
            if (count($_ci->where_in) > 0) {
                foreach($_ci->where_in as $key => $value){
                    $this->db->where_in($key ,$value);
                }
            }
        }

        if (isset($_ci->or_where)) {
            if (count($_ci->or_where) != 0) {
                $this->db->group_start();
                foreach($_ci->or_where as $key => $value){
                    $key1 = key($value);
                    $this->db->or_where($key1,$value[$key1]);
                }
                $this->db->group_end();
            }
        }

        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value'], 'both');
                } else {
                    $this->db->or_like($item, $_POST['search']['value'], 'both');
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $order = $this->order;
            if($order) {
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

        if (count($this->group) != 0) {
            $this->db->group_by($this->group);
        }
		if ($protect){
		$this->db->_protect_identifiers=true;
		}
    }

    public function get_datatables($protect=false)
    {

        $this->_get_datatables_query($protect);
        if(isset($_POST['length'])) {

            if ($_POST['length'] != -1) {
                $this->db->limit($_POST['length'], $_POST['start']);
            }
        }
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit();
        // print_r($this->input->post()); exit();
        return $query->result();
    }


    public function count_filtered($protect=false)
    {
        $this->_get_datatables_query($protect);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($protect=false)
    {
        // $this->_get_datatables_query();
        // return $this->db->count_all_results();

        $this->_get_datatables_query($protect);
        $query = $this->db->get();
        return $query->num_rows();
    }
}

/* End of file DataTable_model.php */
/* Location: ./application/models/DataTable_model.php */
