<?php

class Content_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('content.*,mkategori.kategori');
		$this->datatables->from('content');
		$this->datatables->join('mkategori','mkategori.id=content.kategori');
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$this->title_in   	    = $this->input->post('title_in'); 
		$this->content_in   	= $this->input->post('content_in',false); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->publish   		= 0; 
		$this->created_by   	= $this->session->userdata('user_id'); 
		$this->author   		= $this->session->userdata('tipe_user'); 
		if($this->upload_img(false)){ 
			$this->db->insert('content', $this);
			return true;
		}
	}
	function upload_img($update = false){
		if(isset($_FILES['img'])){
			if($_FILES['img']['name'] != ''){
				// $config['upload_path'] = './assets/images/content/original/';
				$config['upload_path'] = './assets/upload/content/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img')){
					$image_upload = $this->upload->data();
					$this->img = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/content/'.$this->img);			
					$this->myimage->resize(1920,800);
					$this->myimage->save('./assets/upload/content/'.$this->img);
					$this->myimage->resize(798,370);
					$this->myimage->save('./assets/upload/content/thumbs/'.$this->img);
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/content/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/content/'.$row->img);
			unlink('./assets/upload/content/thumbs/'.$row->img);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('content');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->title_in   	    = $this->input->post('title_in'); 
		$this->content_in   	= $this->input->post('content_in',false); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->created_by   	= $this->session->userdata('userid'); 
		$this->author   		= $this->session->userdata('tipe_user'); 
		// print_r($this);exit();
		if($this->upload_img(true)){ 
			$this->db->where('id',$id);
			$this->db->update('content',$this);
			return true;
		}
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('content',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('mkategori.*');
		$this->db->from('mkategori');
		// $this->db->where('mkategori.id',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	
	function get_content($id)
	{
		$this->db->select('content.*');
		$this->db->from('content');
		$this->db->where('content.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function cek_content($id)
	{
		$this->db->select('content.*');
		$this->db->from('content');
		$this->db->where('content.username',$id);
		$this->db->where('content.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
