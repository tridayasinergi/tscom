<?php

class Author_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select("author_ref.*");
		$this->datatables->from('mauthor');
		
		$this->datatables->add_column('action', '', 'id');
		// print_r($this->datatables->last_query());exit();
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

			$this->author_ref   	= $this->input->post('mauthor'); 
			$this->db->insert('mauthor', $this);
			return true;
	}
	
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->author_ref   	= $this->input->post('mauthor'); 
		
		$this->db->where('id',$id);
		$this->db->update('mauthor',$this);
		return true;
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('mauthor',array('publish' => $status));
		return true;
	}
	
	function get_author_ref($id)
	{
		$this->db->select('author_ref.*');
		$this->db->from('mauthor');
		$this->db->where('author_ref.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
}
