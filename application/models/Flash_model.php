<?php

class Flash_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('flash.*,kategori.kategori');
		$this->datatables->from('flash');
		$this->datatables->join('kategori','kategori.id=flash.kategori');
		
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		// $this->urutan   	= $this->input->post('urutan'); 
		$this->content   	= $this->input->post('content',false); 
		$this->url   	    = $this->input->post('url'); 
		$this->urutan   	= $this->input->post('urutan'); 
		
		$this->db->insert('flash', $this);
		return true;
	
	}
	function upload_img_flash ($update = false){
		// print_r(isset($_FILES['img_flash']));exit();
		if(isset($_FILES['img_flash'])){
			if($_FILES['img_flash']['name'] != ''){
				// $config['upload_path'] = './assets/images/flash/original/';
				$config['upload_path'] = './assets/upload/flash/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img_flash']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img_flash')){
					$image_upload = $this->upload->data();
					$this->img_flash  = $image_upload['file_name']; 
					// print_r($this);exit();
					$this->myimage->load('./assets/upload/flash/'.$this->img_flash );			
					$this->myimage->resize(1920,800);
					$this->myimage->save('./assets/upload/flash/'.$this->img_flash );
					$this->myimage->resize(798,370);
					$this->myimage->save('./assets/upload/flash/thumbs/'.$this->img_flash );
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				// print_r('sini');
				return true;
			}
		}else{
			
				print_r('Sahabat');exit();
			return false;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img_flash );exit();
		if(file_exists('./assets/upload/flash/'.$row->img_flash ) && $row->img_flash  !='') {
			unlink('./assets/upload/flash/'.$row->img_flash );
			unlink('./assets/upload/flash/thumbs/'.$row->img_flash );
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('flash');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$id                 = $this->input->post('id'); 
		$this->content   	= $this->input->post('content',false); 
		$this->url   	    = $this->input->post('url'); 
		$this->urutan   	= $this->input->post('urutan'); 
		$this->db->where('id',$id);
		$this->db->update('flash',$this);
		return true;
	
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('flash',array('publish' => $status));
		return true;
	}
	
	
	function get_flash($id)
	{
		$this->db->select('flash.*');
		$this->db->from('flash');
		$this->db->where('flash.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	
	
}
