<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/muser" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/muser/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			
			<?php 
				if($img == '' ){ 
					$img='default.jpg';							
				}
				// $a=file_exists($slider_path.$img);
				// echo $a;
				if(file_exists('./assets/upload/avatars/'.$img)) {
					
				}else{
					$img='default.jpg';							
				}
			?>
			<div class="form-group">
			<label class="col-md-2 control-label" for=""> </label>
				<div class="col-md-4">
					<img src="{avatar_path}<?php echo $img; ?>" id="output_img" class="img-polaroid"  style="width:100px;height:100px;"/>
				</div>
			</div>
			<div class="form-group">
			<label class="col-md-2 control-label" for="img">Photo </label>
				<div class="col-md-4">
					<input type="file" class="fileinput"  accept="image/*" onchange="loadFile_foto(event)" name="img" value="{img}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_user">Nama User <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="nama_user" id="nama_user" placeholder="" value="{nama_user}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="user_tipe_id">Tipe <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select class="form-control js-select2" id="user_tipe_id" name="user_tipe_id" size="1">
						<option value="" <?=($user_tipe_id==''?'selected':'')?>>- Silahkan Pilih Jenis User -</option>
						<?foreach($list_tipe as $row){?>
						<option value="<?=$row->id?>" <?=($user_tipe_id==$row->id?'selected':'')?>><?=$row->tipe_user?></option>
						<?}?>
					</select>
				</div>							
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="username">User Name<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="username" id="username" placeholder="" value="{username}" />					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="password">Password<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="password" class="form-control input-sm" name="password" id="password" placeholder="" value="{password}"/>					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="password2">Confirm Password<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="password" class="form-control input-sm" name="password2" id="password2" placeholder="" value="{password}"/>					
				</div>	
				
			</div>
			
			<input type="hidden"  name="passtext" id="passtext" value="{passtext}" class="form-control input-sm" placeholder=""  />
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/muser" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

<script src="{plugins_path}select2/select2.full.min.js"></script>
<script type="text/javascript">
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
 
  $(document).ready(function(){
		$('.js-select2').select2();
		$(document).on("click","#btn_simpan",function(){
			// alert('KESNI');
			if(!validasi_save_final()) return false;		
			
			

			$("#form1").submit();
		});
    })
  
	$("#password").keyup(function(){
		$("#passtext").val($("#password").val());
	});
function validasi_save_final(){
	
	if ($("#idwarga").val()==''){
		sweetAlert("Maaf...", "Silahkan Isi Identitas", "error");
		return false;	
	}	
	if ($("#username").val()=='' ){
		sweetAlert("Maaf...", "Silahkan Username", "error");
		return false;	
	}
	if ($("#password").val() != $("#password2").val() ){
		sweetAlert("Maaf...", "Password Tidak Sama", "error");
		return false;	
	}
	
	return true;
	
}
</script>