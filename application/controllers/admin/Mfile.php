<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfile extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 // if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       // redirect('admin/login');
      // }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('mfile_model','mfile_model'); 
   }

	function index()
	{	
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Files';
		$data['template']    = 'admin/mfile/manage_index';
		$data['ajax_url'] = 'admin/mfile/getDataList'; 
		// $data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("Files",'#'),
								array("List",'mfile')
								);
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		// print_r($data);exit();
		
	}
	
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
	
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT M.id,M.keterangan,M.file_path,M.image_path,T.nama_kategori,M.publish,T.publish as publish_kategori FROM files M
					LEFT JOIN files_kategori T ON T.id=M.kategori_id
					ORDER BY M.id DESC
			) as T ";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('keterangan','nama_kategori');
        $this->column_order    = array();

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = null;
        if (isset($_POST['start'])) {
            $no = $_POST['start'];
        }

		$url        = site_url('admin/mfile/');
		$url_file        = base_url().'assets/upload/files/';
        foreach ($list as $r) {
            $no++;
            $row = array();
			if ($r->image_path){
			$image='<img class="img-avatar img-avatar48" src="'.base_url().'assets/upload/files/'.$r->image_path.'" alt="">';
				
			}else{
				
			$image='<img class="img-avatar img-avatar48" src="'.base_url().'assets/upload/files/default.png" alt="">';
			}
            $row[] = $no;
            $row[] = $image;
            $row[] = ($r->publish_kategori?text_primary($r->nama_kategori):text_danger($r->nama_kategori));
            $row[] = $r->keterangan;
			if ($r->file_path){
            $row[] ='<a href="'.$url_file.$r->file_path.'" type="button" target="_blank"  title="Lihat File" class="btn btn-xs btn-default"><i class="fa fa-eye"></i> Lihat File</a>';
				
			}else{
				$row[] =text_danger('TIDAK ADA FILE YANG DIUPLOAD');
			}	
            $row[] = 'assets/upload/files/'.$r->file_path;

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
         $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Files - Addnew');
		$data['keterangan'] = "";
		$data['kategori_id'] = "";
		$data['image_path'] = "";
		$data['publish'] = "1";
		$data['error'] = "";
		$data['edit'] = "0";
		$data['id'] = "0";
		$data['list_tipe'] = $this->mfile_model->list_tipe();
		// print_r($data['list_tipe']);exit;
		$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Input Baru",'#',"active"));
		
		$data['title'] = 'Files - Input Baru';
		$data['template'] = 'admin/mfile/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->mfile_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/mfile/editor/$id/");				
			}
		}else{		
			if ($this->mfile_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/mfile/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'mfile/manage';
		$data['list_tipe'] = $this->mfile_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->mfile_model->error_message;
		if($id==''){
			$data['title'] = 'Add Filess';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Filess';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Edit Files",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Files - Edit');
			if($row = $this->mfile_model->get_mfile($id))
			{
				$data['id'] = $id;
				$data['keterangan'] = $row->keterangan;
				$data['image_path'] = $row->image_path;
				$data['kategori_id'] = $row->kategori_id;
				$data['file_path'] = $row->file_path;
				$data['list_tipe'] = $this->mfile_model->list_tipe();
				$data['edit'] = '1';
				
			}
		
		// print_r($data);exit();
		
		if ($id){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['title'] = 'Files - Edit';
		$data['template'] = 'admin/mfile/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function profile($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Files - Edit');
			if($row = $this->mfile_model->get_mfile($id))
			{
				$data['id'] = $id;
				$data['keterangan'] = $row->keterangan;
				$data['img'] = $row->img;
				$data['user_tipe_id'] = $row->user_tipe_id;
				$data['idwarga'] = $row->idwarga;
				$data['email'] = $row->email;
				$data['username'] = $row->username;				
				$data['password'] = $row->passtext;				
				$data['password2'] = $row->passtext;				
				$data['passtext'] = $row->passtext;				
				$data['list_tipe'] = $this->mfile_model->list_tipe();
				$data['edit'] = '1';
				
			}
		
		// print_r($data);exit();
		
		if ($id){
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Edit",'#',"active"));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("Files",'mfile',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['title'] = 'Files - Edit';
		$data['template'] = 'admin/mfile/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function delete($id){
		if ($this->mfile_model->delete($id)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah Dihapus.');
			redirect("admin/mfile");
		}
	}
	function update_status($id,$status){
		
		if ($this->mfile_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/mfile");
		}
	}
}
?>