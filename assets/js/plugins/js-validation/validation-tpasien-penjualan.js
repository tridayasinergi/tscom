/*
 *  Document   : validation-tpasien-penjualan.js
 *  Author     : Deni Purnama
 *  Description: Custom JS code used in Form Validation Page
 */

var BaseFormValidation = function() {

    var ValidationDeni = function(){
        jQuery('.js-validation-deni').validate({
            ignore: [],
            errorClass: 'help-block animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-validnya > div').append(error);
            },
            highlight: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-validnya').removeClass('has-error').addClass('has-error');
                elem.closest('.help-block').remove();
            },
            success: function(e) {
                var elem = jQuery(e);

                elem.closest('.form-validnya').removeClass('has-error');
                elem.closest('.help-block').remove();
            },
            rules: {
                'manage-input-tanggal': {
                    required: true
                },
                'manage-input-kode-pasien': {
                    required: false
                },
                'manage-input-nama-pasien': {
                    required: true
                },
                'manage-textarea-alamat-pasien': {
                    required: true,
                    minlength: 5
                },
                'manage-input-status-pasien': {
                    required: true
                },
                'manage-select-resep': {
                    required: true
                }
            },
            // Pesan error validasi
            messages: {
                'manage-input-tanggal': 'Tanggal wajib diisi.',
                'manage-input-kode-pasien': 'Kode pasien wajib ada.',
                'manage-input-nama-pasien': 'Nama pasien wajib diisi.',
                'manage-select-resep': 'Anda harus pilih salah satu.',
                'manage-textarea-alamat-pasien': 'Minimal 5 karakter.',
                'manage-input-status-pasien': 'Anda harus pilih salah satu.'
            }
        });
    };


    return {
        init: function () {
            // Panggil fungsi untuk Forms Validation
            ValidationDeni();

            // Validation jika Select2 change
            // jQuery('.js-select2').on('change', function(){
            //     jQuery(this).valid();
            // });
        }
    };
}();

// Meng-Analisa jika page diload
jQuery(function(){ BaseFormValidation.init(); });
