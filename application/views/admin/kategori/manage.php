<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/kategori" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/kategori/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="kategori">Kategori<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="kategori" id="kategori" placeholder="" value="{kategori}" />
					
				</div>	
				
			</div>
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/kategori" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

