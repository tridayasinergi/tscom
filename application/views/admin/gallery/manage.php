<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/gallery" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/gallery/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
						
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_gallery">Judul<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="nama_gallery" id="nama_gallery" placeholder="" value="{nama_gallery}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tgl_akad">Tanggal Kegiatan <span class="text-danger"></span></label>				
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="tanggal_gallery" id="tanggal_gallery" placeholder="" value="{tanggal_gallery}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_gallery">Keterangan<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<?php 
						$opt = array('id' 		=> 'keterangan',
									 'name' 	=> 'keterangan',
									 'value' 	=> $keterangan,
									 'style' 	=> 'width:100%;height:500px;',
									 'class' 	=> 'ke-content');
						
						echo form_textarea($opt) 
					?>
					
				</div>	
				
			</div>
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/gallery" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript" src="{tinymce_path}tiny_mce.js"></script>
<script type="text/javascript">
jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
  $(document).ready(function(){
		// alert('sini');
		$('.js-select2').select2();
		tinyMCE.init({
			// General options
			// mode : "textareas",
			selector: '#keterangan',
			theme : "advanced",
			plugins : "imagemanager,filemanager,safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,insertfile,insertimage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			template_replace_values : {
				username : "admin",
				staffid : "123456"
			}
		});
		// tinymce.init({
			// selector: '#content',
			// themes: 'modern',
			// plugins : "imagemanager,filemanager",
			// height: 500
		  // });
    })
  
	
</script>