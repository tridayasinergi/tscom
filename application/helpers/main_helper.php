<?php

function menuIsActive($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(1) == $menu) ? "class=\"active\"" : "";
}
function menuIsActive2($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(2) == $menu) ? "class=\"active\"" : "";
}
function menuIsActive3($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(3) == $menu) ? "class=\"active\"" : "";
}
function replace_embed($content){
	$isi=str_replace('watch?v=','embed/',$content);
	return $isi;
}
function replace_watch($content){
	$isi=str_replace('embed/','watch?v=',$content);
	return $isi;
}
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
function text_primary($text)
{
    return '<span class="label label-primary">'.$text.'</span>';
}
function text_danger($text)
{
    return '<span class="label label-danger">'.$text.'</span>';
}
function text_warning($text)
{
    return '<span class="label label-warning">'.$text.'</span>';
}
function text_success($text)
{
    return '<span class="label label-success">'.$text.'</span>';
}
function text_info($text)
{
    return '<span class="label label-info">'.$text.'</span>';
}
function text_default($text)
{
    return '<span class="label label-default">'.$text.'</span>';
}
function ErrorSuccess($session)
{
    if ($session->flashdata('error')) {
        return ErrorMessage($session->flashdata('message_flash'));
    } elseif ($session->flashdata('confirm')) {
        return SuccesMessage($session->flashdata('message_flash'));
    } else {
        return '';
    }
}
function HumanDateShort($date)
{
    $date = date('d-m-Y', strtotime($date));

    return $date;
}
function Format_YMD($date)
{
    $date = date('Y-m-d', strtotime($date));

    return $date;
}
function strip_tags_content($string) {

    $string = preg_replace ('/<[^>]*>/', ' ', $string); 
    // ----- remove control characters ----- 
    $string = str_replace("\r", '', $string);
    $string = str_replace("\n", ' ', $string);
    $string = str_replace("\t", ' ', $string);
    // ----- remove multiple spaces ----- 
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
    return $string; 

 }
 function replace_url($kata){
	return strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'),
  array('', '-', ''), $kata));
 }
function get_menu_header($parent){
	$CI =& get_instance();
	$query = $CI->db->query("SELECT menu_front.*,COUNT(D.id) as jml_downline from menu_front 
			LEFT JOIN  menu_front D ON D.parent_id=menu_front.id AND D.publish='1'
			where menu_front.parent_id='$parent' AND menu_front.publish='1' 
			GROUP BY menu_front.id
			Order by menu_front.urutan");
	$row = $query->result();
	return $row;
}
function ErrorMessage($message)
{
    return '<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h3 class="font-w300 push-15">Error!</h3>
        <p>'.$message.'</p>
      </div>';
}

function SuccesMessage($message)
{
    return '<div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  
      <h3 class="font-w300 push-15">Success!</h3>
      <p>'.$message.'</p>
    </div>';
}
function BackendBreadcrum($data)
{
    $result = array();
    foreach ($data as $row) {
        if ($row[1] == '#') {
            $url = '<li><a class="text-muted" href="#">'.$row[0].'</a></li>';
        } elseif ($row[1] == '') {
            $url = '<li><a class="link-effect" href="#">'.$row[0].'</a></li>';
        } else {
            $url = '<li><a class="link-effect" href="'.site_url($row[1]).'">'.$row[0].'</a></li>';
        }
        array_push($result, $url);
    }

    return implode('', $result);
}
function hapus_koma($angka,$delimiter = ","){
	return str_replace($delimiter,"",$angka);
}
function get_tgl_akhir($thn,$bln){
	$bulan[1]='31';
	$bulan[2]='28';
	$bulan[3]='31';
	$bulan[4]='30';
	$bulan[5]='31';
	$bulan[6]='30';
	$bulan[7]='31';
	$bulan[8]='31';
	$bulan[9]='30';
	$bulan[10]='31';
	$bulan[11]='30';
	$bulan[12]='31';

	if ($thn%4==0){
	$bulan[2]=29;
	}
	return $bulan[$bln];
}
function front_breadcrum($data)
{
	$result = array();
	foreach($data as $row){	
		if($row[1] == '#'){ 
			$url = anchor(current_url().'#',$row[0]);
		}elseif($row[1] == ''){
			$url = $row[0];
		}else{
			$url = anchor(site_url($row[1]),$row[0]);
		}			
		
		$url = '<li>'.$url.'</li>';	
		array_push($result,$url);
	}
	return implode("", $result);
}

/* END Breadcrum Function */

/* Breadcrum Function */

function admin_breadcrum($data)
{
	$result = array();
	foreach($data as $row){	
		if($row[1] == '#'){ 
			$url = '<li><a href="#">'.$row[0].'</a></li><span class="divider">&gt;</span>';
		}elseif($row[1] == ''){
			$url = '<li class="active">'.$row[0].'</li>';
		}else{
			$url = '<li><a href="'.site_url($row[1]).'">'.$row[0].'</a></li><span class="divider">&gt;</span>';
		}			
		array_push($result,$url);
	}
	return implode("", $result);
}

/* END Breadcrum Function */

/* CMS Function  */

function display_permission($perm)
{
	$permission = array('0' => 'Private','1' => 'Public');
	return  $permission[$perm];
}

function display_publish($perm)
{
	$permission = array('0' => 'Unpublish','1' => 'Published');
	return  $permission[$perm];
}

/* END CMS Function  */

/* DATE Function */

function tsi_date($date) 
{
	$date = date_format(date_create($date),'d M Y');
	return $date;
}

function tsi_long_date($date) 
{
	$date = date_format(date_create($date),'d M Y H:t:s');
	return $date;
}


function human_date($date) 
{
	$date = date("j F Y",strtotime($date));
	return $date;
}

function human_date_audit($date) 
{
	if($date == "0000-00-00" || $date == null){
		$date = "-";
	}else{
		$date = date("d/m/Y",strtotime($date));
	}
	return $date;
}

function human_date_leader($date) 
{
	$date = date("d F Y",strtotime($date));
	return $date;
}

function event_date($date) 
{
	$date = date("d M",strtotime($date));
	return $date;
}

function bulan_date($date) 
{
	$date = date("F Y",strtotime($date));
	return $date;
}

function tahun_date($date) 
{
	$date = date("Y",strtotime($date));
	return $date;
}

function home_date() 
{
	$date = date("l, j F Y");
	return $date;
}

function human_date_short($date) 
{
	$date = date("d-m-Y",strtotime($date));
	return $date;
}

function human_date_mid($date) 
{
	$date = date("j M Y",strtotime($date));
	return $date;
}

function human_date_time($date) 
{
	$date = date("d-m-Y H:i:s ",strtotime($date));
	return $date;
}

function sitemap_date($date) 
{
	$date = date("Y-m-d",strtotime($date));
	$time = date("h:i:s+07:00",strtotime($date));
	return $date."T".$time;
}

function last_login_date($date) 
{
	if($date != '' && $date != '0000-00-00 00:00:00'){
		$date = date("j F Y h:i:s",strtotime($date));
	}else{
		$date = 'First Login';
	}
	return $date;
}

function human_date_full($date) 
{
	$date = date("l, j F Y",strtotime($date));
	return $date;
}

function human_date_admin($date) 
{
	$date = date("d-m-Y h:i",strtotime($date));
	return $date;
}

/* END DATE Function */


/* Permission Function */

function permission_basic_admin($session)
{
	if(!$session->userdata('logged_in')){
		$session->set_flashdata('error',true);
		$session->set_flashdata('message_flash','Access Denied');
		redirect('zuser/login');
	}
}

function permission_admin_logged_in($session)
{
	if($session->userdata('logged_in')){
		$session->set_flashdata('error',true);
		$session->set_flashdata('message_flash','Access Denied');
		redirect('dashboard');
	}
}

function permission_super_admin($session)
{
	if($session->userdata('role') !='SA'){
		$session->set_flashdata('error',true);
		$session->set_flashdata('message_flash',"You don't have privilege to access this page");
		redirect('dashboard');
	}
}

function permission_admin($session)
{
	if($session->userdata('role') !='SA' && $session->userdata('role') !='A'){
		$session->set_flashdata('error',true);
		$session->set_flashdata('message_flash',"You don't have privilege to access this page");
		redirect('admin');
	}
}

function type_user($level)
{	
	if($level !=''){
		$userlevel = array(	
					"SA" => "Super Admin",
					"A" => "Administrator",
					"I" => "Auditor",
					"E" => "Editor",
					"S" => "SER",
					"M" => "Manager Region",
					"B" => "Branch Manager",
					"P" => "Petugas Pusat Pertamina",
					);
		return $userlevel[$level];
	}else{
		return '';
	}
}

/* END Permission Function */

/* Paging Function */

function paging_front($row,$url,$urisegment=3,$perpage=10){
	$config = array();
	
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $urisegment;
	$config['total_rows'] = $row;
	$config['per_page'] = $perpage;
	$config['attributes'] = array('class' => 'page-link');
	// $config[‘num_links’] = 5;
	$config['full_tag_open'] = '<ul class="custom pagination justify-content-center">';
	$config['full_tag_close'] = '</ul>';
	
	$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
	$config['first_tag_open'] = '<li class="page-item">';
	$config['first_tag_close'] = '</a></li>';
	
	$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
	$config['last_tag_open'] = '<li class="page-item">';
	$config['last_tag_close'] = '</li>';
	
	$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
	$config['prev_tag_open'] = '<li class="page-item">';
	$config['prev_tag_close'] = '</li>';

	$config['next_link'] = '<i class="fa fa-angle-right"></i>';
	$config['next_tag_open'] = '<li class="page-item">';
	$config['next_tag_close'] = '</li>';

	// $config['num_links'] = 3;
	$config['num_tag_open'] = '<li class="page-item">';
	$config['num_tag_close'] = '</li>';
	
	$config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
	$config['cur_tag_close'] = '</a></li>';
	
	return $config;
}
function paging_admin_asal($row,$url,$uri_segment = 4,$per_page = 5){
	$config = array();
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $uri_segment;
	$config['total_rows'] = $row;
	$config['per_page'] = $per_page; 	
	$config['num_links'] = 5;
	$config['full_tag_open'] = '<div class="dataTables_paginate paging_full_numbers" >';
	$config['full_tag_close'] = '</div>';
	$config['first_link'] = 'First';
	$config['last_link'] = 'Last';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Previous';
	$config['cur_tag_open'] = '<a class="paginate_active">';
	$config['cur_tag_close'] = '</a>';
	$config['anchor_class'] = 'class="paginate_button"';
	return $config;
}
function paging_admin($row,$url,$uri_segment = 4,$per_page = 5){
	// $config = array();
	// $config['base_url'] = site_url($url);
	// $config['uri_segment'] = $uri_segment;
	// $config['total_rows'] = $row;
	// $config['per_page'] = $per_page; 	
	// $config['num_links'] = 5;
	// $config['full_tag_open'] = '<ul class="paginationpagination">';
	// $config['full_tag_close'] = '</ul>';
	// $config['first_link'] = '<li class="paginate_button first">';
	// $config['last_link'] = '<li class="paginate_button last"> Last';
	// $config['next_link'] = '<li class="paginate_button next">';
	// $config['prev_link'] = '<li class="paginate_button prev">';
	// $config['cur_tag_open'] = '<li class="paginate_button active"></a>';
	// $config['cur_tag_close'] = '</a></li>';
	// $config['anchor_class'] = '<li class="paginate_button ">';
	// // print_r($config);exit();
	// return $config;
	// $config = array();
	// $config['base_url'] = site_url($url);
	// $config['uri_segment'] = $uri_segment;
	// $config['total_rows'] = $row;
	// $config['per_page'] = $per_page; 	
	// $config['num_links'] = 5;
	// $config['full_tag_open'] = '<div class="dataTables_paginate paging_full_numbers" >';
	// $config['full_tag_close'] = '</div>';
	// $config['first_link'] = 'First';
	// $config['last_link'] = 'Last';
	// $config['next_link'] = 'Next';
	// $config['prev_link'] = 'Previous';
	// $config['cur_tag_open'] = '<a class="paginate_active">';
	// $config['cur_tag_close'] = '</a>';
	// $config['anchor_class'] = 'class="paginate_button"';
	// return $config;
	$config = array();
	$config['base_url'] = site_url($url);
	$config['uri_segment'] = $uri_segment;
	$config['total_rows'] = $row;
	$config['per_page'] = $per_page;
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	
	$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
	$config['first_tag_open'] = '<li class="paginate_button first">';
	$config['first_tag_close'] = '</li>';
	
	$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
	$config['last_tag_open'] = '<li class="paginate_button last">';
	$config['last_tag_close'] = '</li>';
	
	$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
	$config['prev_tag_open'] = '<li class="paginate_button prev">';
	$config['prev_tag_close'] = '</li>';

	$config['next_link'] = '<i class="fa fa-angle-right"></i>';
	$config['next_tag_open'] = '<li class="paginate_button next">';
	$config['next_tag_close'] = '</li>';


	// $config['next_link'] = '<a class="paginate_button next">';
	// $config['prev_link'] = '<li class="paginate_button prev"></li>';
	$config['num_tag_open'] = '<li class="paginate_button ">';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
	$config['cur_tag_close'] = '</a></li>';
	return $config;
}
/* Paging Function */

/* WYSIWYG Function */
function replace_image_url_asset($content){
	$isi=str_replace('"images/','"'.base_url().'images/',str_replace('../','',$content));
	return $isi;
}
function replace_p($content){
	$isi=str_replace('<p>','',$content);
	$isi=str_replace('</p>','',$isi);
	return $isi;
}
function replace_image_url($content){
	$isi=str_replace('../../','',$content);
	$isi = str_replace(' src="./images', ' src="'.base_url().'images', $isi);
	return $isi;
}

function replace_image_url2($content){
	$isi=str_replace('../','',$content);
	$isi = str_replace(' src="images', ' src="'.base_url().'images', $isi);
	return $isi;
}

/* END WYSIWYG Function */


/*Admin Error Message */
function error_success($session){
	if($session->flashdata('error')){
		return error_message($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message($session->flashdata('message_flash'));
	}else{
		return '';
	}
}
function error_success_tsi($session){
	if($session->flashdata('error')){
		return error_message_tsi($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message_tsi($session->flashdata('message_flash'));
	}else{
		return '';
	}
}
function error_message_tsi($message){
   return '<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong> Warning ! </strong>'.$message.'</div>';
}

function succes_message_tsi($message){
	 return '<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong> Informasi ! </strong>'.$message.'</div>';
}
function error_message($message){
   
	return '<div class="nNote nFailure">
				<p>'.$message.'</p>
			</div>';
}

function succes_message($message){
	return '<div class="nNote nSuccess">
				<p>'.$message.'</p>
			</div>';
}

function error_success_front($session){
	if($session->flashdata('error')){
		return error_message_front($session->flashdata('message_flash'));
	}elseif($session->flashdata('confirm')){
		return succes_message_front($session->flashdata('message_flash'));
	}else{
		return '';
	}
}

function error_message_front($message){

	return '<div class="alert alert-error">
				<h4><strong>Error notification:</strong></h4>'.$message.'
			</div>';
}

function succes_message_front($message){
	return '<div class="alert alert-success">
				<h4><strong>Success notification:</strong></h4>'.$message.'
			</div>';
}


/* helper checklist */

function convert_score_text($str)
{
	$hasil = $str;
	if($str == "A/B/C/D/E/F"){
		$hasil = "A-F";
	}elseif($str == "A/B/C/D/E/F/X"){
		$hasil = "A-F/X";
	}
	
	return $hasil;

}

function format_score($angkat){
	return number_format($angkat,2);
}


function format_angka($angkat){
	return number_format($angkat,0,",",".");
}


function convert_month($num){
    switch($num){
        case "01":
            return "Januari";
            break;
        case "02":
            return "Februari";
            break;
        case "03":
            return "Maret";
            break;
        case "04":
            return "April";
            break;
        case "05":
            return "Mei";
            break;
        case "06":
            return "Juni";
            break;
        case "07":
            return "Juli";
            break;
        case "08":
            return "Agustus";
            break;
        case "09":
            return "September";
            break;
        case "10":
            return "Oktober";
            break;
        case "11":
            return "November";
            break;
        case "12":
            return "Desember";
            break;
    }
}

function site_themes() {
	$sql = mysql_query("select themes from z_site_setting ")or die (mysql_error());
	$rest = mysql_fetch_array($sql);
	return $rest['themes'];
}

function themes() {
	global $system_folder,$application_folder;
	$themes = site_url().''.APPPATH.'views/themes/'.site_themes().'/';
	return $themes;
}


	
function ec_pb($noterima, $tglterima, $query){
	$mailcontent='<p>Yth Admin<br />Terdapat <strong>Penerimaan Barang</strong> dengan data:<br /></p><table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px"><tr><td><strong>Data Penerimaan</strong></td><td>&nbsp;</td></tr>';
	$mailcontent.='<tr><td width="187">No Penerimaan</td><td width="400">: &nbsp;&nbsp; '.$noterima.'</td></tr><tr><td>Tanggal Input</td><td>: &nbsp;&nbsp; '.$tglterima.'</td></tr><tr><td>&nbsp;</td></tr></table><table border="1" cellspacing="2" cellpadding="2" style="margin-left:40px; text-align:center;"><tr><td width="50">No</td><td width="250">Nama Barang</td><td width="200">Jumlah</td><td width="200"><strong>Stok Update</strong></td></tr>';
	$no=1;
	foreach ($query as $row){
	$mailcontent.='<tr><td>'.$no.'.</td><td>'.$row->namabarang.'</td><td>'.$row->jumlah.'</td><td><strong>'.$row->stok.'</strong></td></tr>';
	$no++;
	}
	$mailcontent.='</table><p>  Salam,<br />Banyu PS</p>';		
	return $mailcontent; 
}

function opt_month($cur) {
	$hasil='';
	for ($i=1;$i<13;$i++){
		$temp=substr("00", 0,2-strlen($i)).$i;
		if ($cur==$temp){
		$hasil.="<option value=\"$temp\" selected=\"SELECTED\" class='ayrsingle'>".get_month_name($temp)."</option>";
		}else{
		$hasil.="<option value=\"$temp\" class='ayrsingle'>".get_month_name($temp)."</option>";
		}
	}
	return $hasil;
}
function get_month_name($cur,$initial=0) {
	switch ($cur)
	{
		case '01':
			$list="Januari";
			break;
		case '02':
			$list="Februari";
			break;
		case '03':
			$list="Maret";
			break;
		case '04':
			$list="April";
			break;
		case '05':
			$list="Mei";
			break;
		case '06':
			$list="Juni";
			break;
		case '07':
			$list="Juli";
			break;
		case '08':
			$list="Agustus";
			break;
		case '09':
			$list="September";
			break;
		case '10':
			$list="Oktober";
			break;
		case '11':
			$list="November";
			break;
		case '12':
			$list="Desember";
			break;
	}
	if ($initial){if ($cur=='08'){$list='Ags';}else{$list=substr($list,0,3);}}
	return $list;
}

function getauth($allow,$level) {
	// $CI =& get_instance();
	// $query = $CI->db->query("SELECT nilai FROM zsetting WHERE setting = '".$allow."'");
	// if ($row = $query->row()){
		// $listcb=getpermissions($row->nilai);
		// if(array_search($level, $listcb)){
			// return 1;
		// }else{
			// return 0;
		// }		
	// }else{
		// return 1;
	// }
}


function getstatuspesan($sta,$jml,$progress,$realisasi) {
	if ($realisasi == 0){
        $oto="Pending";
	}elseif ($sta==0 && $progress == 0){
		$oto="Opened";
	}elseif ($sta<$jml){
		$oto="Progress";
	}elseif ($sta==$jml){
		$oto="Closed";
	}else{
		$oto="ERROR";
	}
	return $oto;
}

function getstatuspesandetail($sta,$jml,$progress) {
	if ($jml == 0){
        $oto="Rejected";
	}elseif ($sta==0 && $progress == 0){
		$oto="Opened";
	}elseif ($progress<$jml){
		$oto="Progress";
	}elseif ($sta == 1){
		$oto="Closed";
	}else{
		$oto="ERROR";
	}
	return $oto;
}
function ec_useronline($namastokies,$username,$pwd){
$mailcontent='<!doctype html><html lang="en"><head><title>Permohonan Stokies Online</title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /></head><body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr>';
$mailcontent.='<td height="75"><a style="font-family: Helvetica, arial, sans-serif; font-size: 18px; font-weight: bold; text-decoration: none; color: #ffffff" href="#">Tridaya Sinergi : Reset Password</a>';
$mailcontent.='</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="3"><img width="650" height="3" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/top_zpsbe5f4c64.jpg"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr>	<td bgcolor="#FFFFFF" align="center" height="100" style="font-family: Helvetica, arial, sans-serif; font-size: 22px; color: #191919; font-weight: bold; line-height: 29px;">';
$mailcontent.='<img style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/logotridaya_zps99557cb9.png">Tridaya Sinergi<br><span style="font-weight: normal; font-size: 18px; color: #6c6c6c;">Permohonan Stokies Online.</span></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;"><tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">';
$mailcontent.='<img width="648" height="1" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/divider_zps4dbed001.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" width="30">&nbsp;</td>	<td bgcolor="#FFFFFF" align="left" style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #4f4f4f; line-height: 15px;">';
$mailcontent.='<span style="font-weight: bold;">Yth. '.$namastokies.',<br><br></span>Sesuai permintaan anda untuk <b>"Latihan Stokies Online"</b>, Untuk itu sekarang anda telah bisa latihan online.<br>Rincian login anda sekarang adalah sebagai berikut:<br><br><table border="0" cellspacing="2" cellpadding="2" style="margin-left:40px">';
$mailcontent.='<tr><td width="110">Username</td><td width="400">:&nbsp; '.$username.'</td></tr><tr><td width="110">Password</td><td width="400">:&nbsp; '.$pwd.'</td></tr></table>';
$mailcontent.='<br>Untuk latihan silahkan akses alamat URL : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://latstokies.tridayasinergi.net" target="new">latstokies.tridayasinergi.net</a>';
$mailcontent.='<br>Kami sarankan untuk merubah password ini secepatnya. Untuk merubahnya silahkan Login dan paling kiri atas klik &quot;usernameanda&quot; -> &quot;Ganti Password&quot;.<br><br>Salam Sejahtera,<br><br><a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">Tridaya Sinergi.</a><br>';
$mailcontent.='</td><td bgcolor="#FFFFFF" width="30">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center" style="border-left: 1px solid #c5c5c5; border-right: 1px solid #c5c5c5;">';
$mailcontent.='<tbody><tr><td bgcolor="#FFFFFF" height="20" valign="top">&nbsp;</td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td height="22">';
$mailcontent.='<img width="650" height="22" style="display: block;" src="http://i1054.photobucket.com/albums/s490/sakatirta/mail/bottom_zps0fb600b5.jpg"></td></tr></tbody></table><table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#008800" height="10"></td></tr></tbody></table>';
$mailcontent.='<table width="650" cellspacing="0" cellpadding="0" border="0" align="center"><tbody><tr><td width="650" bgcolor="#66cc66" align="center"><span style="font-family: Helvetica, arial, sans-serif; font-size: 11px; color: #ffffff; font-style: normal; line-height: 16px;">';
$mailcontent.='Tridaya Sinergi Official Website : <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.com">tridayasinergi.com</a>. &#38; <a style="text-decoration: none; color: #315e8b; font-weight: bold;" href="http://tridayasinergi.net">tridayasinergi.net</a></span></td></tr></tbody></table></body></html>';
return $mailcontent; 
}
function get_nama($noid){
	$CI =& get_instance();
	$CI->db->select('namamembers');
	$CI->db->from('mmembers');
	$CI->db->where('noid',$noid);
	$query = $CI->db->get();
	if($row = $query->row()){return $row->namamembers;}
}
/////Begin New

function load_icon($nama_file){
	
	return '<img src="'.site_url('assets/img/ico/'.$nama_file.'.png').'">';
}
function get_nama_periode($bulan,$tahun){
	$bulan=convert_month($bulan);
	return $bulan .' '.$tahun;
}
// End New

//TSADMIN NEW 2014

function RemoveComma($number, $delimiter = ',')
{
    return str_replace($delimiter, '', $number);
}
function gabung_alamat($alamat='',$rt='',$rw='',$desa='',$kec='',$kota='',$provinsi=''){
	$alamat="";
	if ($rt <>''){
		$alamat=$alamat & " RT. " & $rt;
	}
	if ($rw <>''){
		$alamat=$alamat & " RW. " & $rw;
	}
	if ($desa <>''){
		$alamat=$alamat & " Desa. " & $desa;
	}
	if ($kec <>''){
		$alamat=$alamat & " Kec. " & $kec;
	}
	if ($kota <>''){
		$alamat=$alamat & ", " & $kota;
	}
	if ($provinsi <>''){
		$alamat=$alamat & " ,Provinsi " & $provinsi;
	}
}
function increment_single_string($last) {
		 $string = $last;
		 $string++;		
		 return $string;
	}
	function increment_banyak_string($last,$bil) {
		 $string = $last;
		 for($i=0;$i<$bil;$i++){
			 $string++;		
		 }
		 
		 return $string;
	}
	function kurangin_abjad($last){
		$string = $last;
		 $string=$string+1;
		
		 return $string;
	}
	function replace_header($kata){
	$kata=substr($kata,-2);
	if ($kata=="01"){
		$kata="JANUARI";
	}elseif($kata=="02"){
		$kata="FEBRUARI";	
	}elseif($kata=="03"){
		$kata="MARET";
	}elseif($kata=="04"){
		$kata="APRIL";
	}elseif($kata=="05"){
		$kata="MEI";
	}elseif($kata=="06"){
		$kata="JUNI";
	}elseif($kata=="07"){
		$kata="JULI";
	}elseif($kata=="08"){
		$kata="AGUSTUS";
	}elseif($kata=="09"){
		$kata="SEPTEMBER";
	}elseif($kata=="10"){
		$kata="OKTOBER";
	}elseif($kata=="11"){
		$kata="NOVEMBER";
	}elseif($kata=="12"){
		$kata="DESEMBER";
	}else{
		$kata="";	
	}
	return $kata;
}
function replace_spasi($kata,$pengganti="_"){
	return str_replace(" ",$pengganti,$kata);
}
function balik_spasi($kata,$pengganti=" "){
	return str_replace("_",$pengganti,$kata);
}
function balik_spasi_header($kata,$pengganti=" "){
	$kata= str_replace("_",$pengganti,$kata);
	$result = preg_replace("/[^a-zA-Z ]/", "", $kata);
	return $result;
}
function getRomawi($bln){
	switch ($bln){
			case 1: 
				return "I";
				break;
			case 2:
				return "II";
				break;
			case 3:
				return "III";
				break;
			case 4:
				return "IV";
				break;
			case 5:
				return "V";
				break;
			case 6:
				return "VI";
				break;
			case 7:
				return "VII";
				break;
			case 8:
				return "VIII";
				break;
			case 9:
				return "IX";
				break;
			case 10:
				return "X";
				break;
			case 11:
				return "XI";
				break;
			case 12:
				return "XII";
				break;
	  }
}
function onprogress($status)
{
    if ($status != '') {
        $data = array(
            '0' => '<span class="label label-danger text-uppercase" data-toggle="tooltip" title="Belum Di Acc">On Progress</span>',
            '1' => '<span class="label label-success text-uppercase" data-toggle="tooltip" title="Sudah Terverifikasi">Sudah Selesai</span>',
            '2' => '<span class="label label-warning text-uppercase" data-toggle="tooltip" title="Unverifikasi">Unverifikasi</span>'
        );

        return $data[$status];
    } else {
        return '';
    }
}
function tgl_indo($tanggal){
	$tanggal=human_date_short($tanggal);
	// print_r($tanggal);exit();
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[0] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[2];
}
function TreeView($level, $name)
{
    $indent = '';
    for ($i = 1; $i < $level; $i++) {
        if ($i > 1) {
            $indent .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        } else {
            $indent .= '';
        }
    }

    if ($i > 1) {
        $indent .= '└─';
    } else {
        $indent .= '';
    }

    return $indent . ' ' . $name;
}
function rename_url($url){
	$url=strip_tags($url);
	$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
   $url = trim($url, "-");
   $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
   $url = strtolower($url);
   $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
	// print_r($url);exit();
   return $url;
	// return $hasil;
}
function rename_image($url){
	$url=strip_tags($url);
	$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
   $url = trim($url, "-");
   $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
   $url = strtolower($url);
   $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
	// print_r($url);exit();
   return $url;
	// return $hasil;
}
function RemoveSpecialChar($str) { 
      
	  // $str="c1 !`\dalam Pernajalan <br>.jpg";
	  // $str=strip_tags($str);
		// Using str_replace() function  
		// to replace the word  
		$res = str_replace( array( '\'', '"', 
		',' , ';', '<', '>', ' ','!','`','%','^','(',')','*','&',' ','\'' ), '-', $str); 
		  
		// Returning the result
		// $res=rename_url($str);
		// print_r($res);exit();
		return $res; 
    } 
?>