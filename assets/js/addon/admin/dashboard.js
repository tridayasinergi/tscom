var table;
$(document).ready(function(){
	$("#idkota").select2();
	$("#idkecamatan").select2();
	$("#iddesa").select2();
	$("#idrw").select2();
	$("#idrt").select2();
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 20,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [[ 1, 'asc' ], [ 2, 'asc' ]],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false, className : 'not_search'},
        {"data": "nama_wilayah", searchable : true, orderable: true},
        {"data": "warga", searchable : false, orderable: true},
        {"data": "kk", searchable : false, orderable: true},
        {"data": "l", searchable : false, orderable: true},
        {"data": "p", searchable : false, orderable: true},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {                 
    }        
})
function cek_status($status)
{	
	if ($status=='1'){
		return '<i class="si si-check fa-2x"></i>';	
	}else{
		return '<i class="si si-close fa-2x"></i>';	
	}
	// return $status;
		
}

$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
$("#idkota").change(function(){
	if ($(this).val()!=''){
		$('#idkecamatan').find('option').remove().end().append('<option value="99999" selected>- ALL Kecamatan -</option>');
		$('#iddesa').find('option').remove().end().append('<option value="99999" selected>- ALL Desa -</option>');
		$('#idrw').find('option').remove().end().append('<option value="99999" selected>- ALL RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="99999" selected>- ALL RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_kecamatan/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idkecamatan')
				.append('<option value="' + item.id + '">' + item.kecamatan + '</option>');
			});
			}
		});
	}
	// gabung();
});
$("#idkecamatan").change(function(){
	if ($(this).val()!=''){
		$('#iddesa').find('option').remove().end().append('<option value="99999" selected>- ALL Desa -</option>');
		$('#idrw').find('option').remove().end().append('<option value="99999" selected>- ALL RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="99999" selected>- ALL RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_desa/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#iddesa')
				.append('<option value="' + item.id + '">' + item.desa + '</option>');
			});
			}
		});
	}
	// gabung();
});
$("#iddesa").change(function(){
	if ($(this).val()!=''){
		$('#idrw').find('option').remove().end().append('<option value="99999" selected>- ALL RW -</option>');
		$('#idrt').find('option').remove().end().append('<option value="99999" selected>- ALL RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_rw/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idrw')
				.append('<option value="' + item.id + '">' + item.rw + '</option>');
			});
			}
		});
	}
	// gabung();
});
$("#idrw").change(function(){
	if ($(this).val()!=''){
		$('#idrt').find('option').remove().end().append('<option value="99999" selected>- ALL RT -</option>');
		$.ajax({
			url: site_url+'admin/All/find_rt/'+$(this).val(),
			dataType: "json",
			success: function(data) {

			$.each(data.detail, function (i,item) {
			$('#idrt')
				.append('<option value="' + item.id + '">' + item.rt + '</option>');
			});
			}
		});
	}
	// gabung();
});
function validate_final(){
	$('#idkota').removeAttr('disabled');
	$('#idkecamatan').removeAttr('disabled');
	$('#idkecamatan').removeAttr('disabled');
	$('#iddesa').removeAttr('disabled');
	$('#idrw').removeAttr('disabled');
	$('#idrt').removeAttr('disabled');
	return true;
}