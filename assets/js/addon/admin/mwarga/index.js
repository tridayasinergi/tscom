var table;
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 1, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false, className : 'not_search'},
        {"data": "nokk", searchable : true, orderable: true},
        {"data": "nik", searchable : true, orderable: true},
        {"data": "nama", searchable : true, orderable: true},
        {"data": "alamat_lengkap", searchable : true, orderable: true},
        {"data": "domisili", searchable : true, orderable: true},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
        action += '<li><a href="'+ site_url +'admin/mwarga/editor/' + aData['id'] + '">Edit</a></li>';
        action += '<li><a href="'+ site_url +'admin/mwarga/catat_pindah/' + aData['id'] + '">Catat Pindah</a></li>';
        action += '<li><a href="'+ site_url +'admin/mwarga/catat_meninggal/' + aData['id'] + '">Catat Meninggal</a></li>';		
        action += '<li><a href="'+ site_url +'admin/mwarga/delete/' + aData['id'] + '">Delete</a></li>';
        
		
		action += '</ul></div>'							
								
        action += '</div>';
		
        $("td:eq(6)", nRow).html(action);
		
		
        return nRow;            
    }        
})
function cek_status($status)
{	
	if ($status=='1'){
		return '<i class="si si-check fa-2x"></i>';	
	}else{
		return '<i class="si si-close fa-2x"></i>';	
	}
	// return $status;
		
}

$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
