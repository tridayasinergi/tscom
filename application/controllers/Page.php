<?php

class Page extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('page_model','page_model');
		$this->load->model('home_model','home_model');
	}
	function index($str_key='',$judul='')
	{
		// print_r($str_key);exit();
		
		$data = array();	
		$data=$this->page_model->get_content_page($str_key);
		if ($data){
			$data['template'] = 'page';			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		}else{
			$this->load->view('page404');
			// redirect('404');
		}
	}
	function berita($kategori='all')
	{
		// print_r();exit();
		// if (){
			
		// }
		$cari_text=$this->input->post('cari_text');
		$kategori_nama=$kategori;
		if ($kategori!='all'){			
			$kategori_nama=$this->page_model->get_kategori($kategori);
		}
		if ($kategori_nama!=''){
			$page=$this->uri->segment(4);
			// print_r();exit();
			if ($cari_text){
				$jml_tampil=100;
			}else{
				$jml_tampil=5;
			}
			// $total=5;
			$data = array();	
			$total =$this->page_model->count_berita($kategori);
			$data['list_berita'] =$this->page_model->list_berita($kategori,$page,$jml_tampil);
			$data['list_all_tag'] =$this->page_model->list_all_tag();
			$data['top_kategori'] =$this->page_model->top_kategori();
			$data['top_populer'] =$this->page_model->top_populer();
			$data['recent_post'] =$this->page_model->recent_post();
			
			$this->pagination->initialize(paging_front($total,'page/berita/'.$kategori,4,$jml_tampil));
			$data['pagination'] =  $this->pagination->create_links();
				
			// print_r($data['list_berita']);exit();	
			$data['kategori_nama'] = $kategori_nama;			
			$data['kategori'] = $kategori;			
			$data['cari_text'] = $cari_text;			
			$data['template'] = 'news';			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		}else{
			$this->load->view('page404');
			// redirect('404');
		}
	}
	function berita_detail($str_key='',$judul='')
	{
		// $str_key=strlo
		// print_r(substr($str_key,0,1).'-'.substr($judul,0,1));exit();
		if (substr($str_key,0,1)== substr($judul,0,1)){
			$str_key=substr($str_key,2,6);
			$data = array();	
			$q="update news set jml_klik=jml_klik +1 WHERE id='$str_key'";
			$this->db->query($q);
			$data=$this->page_model->get_content_news($str_key);
			if ($data){
			// print_r($data);exit();
				$data['template'] = 'news_detail';	
				$data['list_tag_berita'] =$this->page_model->list_tag_berita($str_key);
				$data['list_all_tag'] =$this->page_model->list_all_tag();
				$data['top_kategori'] =$this->page_model->top_kategori();
				$data['top_populer'] =$this->page_model->top_populer();
				$data['recent_post'] =$this->page_model->recent_post();
				
				// print_r($data['list_berita']);exit();	
				$this->pagination->initialize(paging_front($total,'page/berita/'.$kategori,4,$jml_tampil));
				$data['pagination'] =  $this->pagination->create_links();
					
				$data['kategori_nama'] = $kategori_nama;			
				$data['kategori'] = $kategori;			
				$data['cari_text'] = $cari_text;	
				
				$data = array_merge($data,basic_info());
				$this->parser->parse('index',$data);
			}else{
				$this->load->view('page404');
				// redirect('404');
			}
		}else{
			// print_r('tidak sama');exit();
			$this->load->view('page404');
		}
	}
	function gallery(){
		$data = array();	
		$jml_tampil=9;
		$page=$this->uri->segment(3);
		$total =$this->page_model->count_gallery();
		$data['list_gallery']=$this->page_model->list_gallery2($page,$jml_tampil);
		$this->pagination->initialize(paging_front($total,'page/gallery/',3,$jml_tampil));
		$data['pagination'] =  $this->pagination->create_links();
		$data['template'] = 'gallery';			
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function sinergitv(){
		$data = array();	
		$jml_tampil=20;
		$page=$this->uri->segment(3);
		$total =$this->page_model->count_video();
		$data['list_gallery']=$this->page_model->list_video($page,$jml_tampil);
		$this->pagination->initialize(paging_front($total,'page/sinergitv/',3,$jml_tampil));
		$data['pagination'] =  $this->pagination->create_links();
		$data['template'] = 'video';			
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function testimoni(){
		$data = array();	
		$jml_tampil=20;
		$page=$this->uri->segment(3);
		$total =$this->page_model->count_testimoni();
		$data['list_gallery']=$this->page_model->list_testimoni($page,$jml_tampil);
		$this->pagination->initialize(paging_front($total,'page/testimoni/',3,$jml_tampil));
		$data['pagination'] =  $this->pagination->create_links();
		$data['template'] = 'testimoni';			
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function gallery_detail($str_key='',$judul=''){
		$data = array();	
		$data=$this->page_model->get_gallery($str_key);
		$data['list_gallery']=$this->page_model->list_gallery_detail($str_key);
		$data['list_other']=$this->page_model->list_other($str_key);
		
		$data['template'] = 'gallery_detail';			
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */