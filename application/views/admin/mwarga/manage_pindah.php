<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/mwarga/add_new" class="btn" data-toggle="tooltip" title="" data-original-title=""> <i class="fa fa-plus"></i></a>
			</li>
			<li>
				<a href="{base_url}admin/mwarga" class="btn"><i class="fa fa-reply"></i></a>
			</li>			
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('admin/mwarga/simpan_pindah',array('id'=>'form1','method'=>'post','class'=>'js-validation-bootstrap  form-horizontal push-10-t')); ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nokk">No.KK <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" readonly class="form-control input-sm" name="nokk" id="nokk" placeholder="" value="{nokk}" />					
				</div>
				<div class="col-md-4">
					<label class="css-input css-checkbox css-checkbox-primary">
						<input type="checkbox" <?=($status_kk=='1')?'checked':'';?> name="status_kk"><span></span> Kepala Keluarga
					</label>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tgl_pindah">Tanggal Pindah <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="tgl_pindah" id="tgl_pindah" placeholder="" value="{tgl_pindah}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="jenis_kelamin">Anggota Keluarga <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<div class="block-content">
						<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
						<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
						
							<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
								<thead>
									<tr>                                    
										<th width="5%">#</th>
										<th width="10%">NIK</th>
										<th width="20%">Nama</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$i=0;
									foreach($list_keluarga as $row){ ?>
										<tr> 
											<td><input type="checkbox" checked name="status_kk[<?=$i?>]"><input type="hidden" name="no_id[<?=$i?>]" value="<?=$row->id?>"></td>
											<td><?=$row->nik?></td>
											<td><?=$row->nama?></td>
										</tr>
									<?
									$i=$i+1;
									}?>
								</tbody>
							</table>
						<?= ($this->agent->is_mobile())? '</div>' : '' ?>
					</div>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alasan_pindah">Alasan Pindah <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" id="alasan_pindah" name="alasan_pindah" rows="3" placeholder="Alasan Pindah">{alasan_pindah}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat_tujuan">Alamat Tujuan <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" id="alamat_tujuan" name="alamat_tujuan" rows="3" placeholder="Alamat Tujuan">{alamat_tujuan}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="st_lanjut" id="st_lanjut" placeholder="" value="{st_lanjut}" />					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" id="btn_simpan" type="submit">Simpan</button>
					<a href="{base_url}admin/mwarga" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

