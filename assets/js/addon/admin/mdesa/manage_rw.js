var table;
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
$(document).ready(function(){
	// alert('masuk sini');

// $('#datatable_index tfoot th').each( function () {
        
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });
	
table = $('#datatable_index').DataTable({
    "pageLength": 10,  
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [ 2, 'ASC' ],
    "ajax": { "url": site_url + ajax_url, "type": "POST" },
    "columns": [
        {"data": "nomor", searchable : false, orderable: false, className : 'not_search'},
        {"data": "desa", searchable : true, orderable: true},
        {"data": "rw", searchable : true, orderable: true},
        {"data": "nama", searchable : true, orderable: true},
        {"data": "action", searchable : false, orderable: false, className : 'not_search'},
       
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

        var action = '<div class="btn-group"><button data-toggle="dropdown" class="btn btn-primary">Action <span class="caret"></span></button><ul class="dropdown-menu">';
		if (user_tipe_id <= 6){
        action += '<li><a href="'+ site_url +'admin/mdesa/editor_rw/' + aData['id'] + '">Edit</a></li>';
		}
        action += '<li><a href="'+ site_url +'admin/mdesa/add_rt/' + aData['id'] + '">Add RT</a></li>';		
		if (user_tipe_id <= 6){
		action += '<li><a href="'+ site_url +'admin/mdesa/delete_rw/' + aData['id'] + '/'+ aData['iddesa'] + '">Delete</a></li>';	
		}
		action += '</ul></div>'							
								
        action += '</div>';
		if (user_tipe_id < 7){
			$("td:eq(4)", nRow).html(action);
		}else{
			//idrw_session
			if (idrw_session == aData['id']){
				$("td:eq(4)", nRow).html(action);
			}
		}
		
        return nRow;            
    }        
})
function cek_status($status)
{	
	if ($status=='1'){
		return '<i class="si si-check fa-2x"></i>';	
	}else{
		return '<i class="si si-close fa-2x"></i>';	
	}
	// return $status;
		
}
$("#idwarga").select2({	
	minimumInputLength: 2,
	noResults: 'Tidak Ditemukan.',          
	// allowClear: true
	// tags: [],
	ajax: {
		url: site_url+'admin/All/find_warga/',
		dataType: 'json',
		type: "POST",
		quietMillis: 50,
		
	 data: function (params) {
		  var query = {
			search: params.term,                
		  }
		  return query;
		}, 
		processResults: function (data) {
			return {
				results: $.map(data, function (item) {
					return {
						text: item.nik + ' - '+item.nama+'.',
						id: item.id
					}
				})
			};
		}
	}
});
$('#datatable_index_filter input').unbind();
$('#datatable_index_filter input').bind('keyup', function(e) {
   if(e.keyCode == 13) {
    table.search(this.value).draw();   
   }
}); 

$("#refresh_list").click(function(){
    table.state.clear();
    window.location.reload();
});        


	
})
