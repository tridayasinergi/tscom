<?php

class Muser_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('muser.id,muser.nama_user,muser.username,
			muser.user_tipe_id,muser_tipe.tipe_user,mrw.rw,mrt.rt,mwarga.nama');
        // $this->datatables->select('muser.*');
		$this->datatables->from('muser');
		$this->datatables->join('muser_tipe','muser.user_tipe_id=muser_tipe.id','LEFT');		
		$this->datatables->join('mwarga','muser.idwarga=mwarga.id','LEFT');		
		$this->datatables->join('mrw','mwarga.idrw=mrw.id','LEFT');		
		$this->datatables->join('mrt','mwarga.idrt=mrt.id','LEFT');		
		$this->datatables->group_by('muser.id');
		// $this->datatables->where('muser.publish',1);
		// $this->datatables->or_where('muser.id',$this->session->userdata('userid'));
		if ($this->session->userdata('user_tipe_id')==7){
			$this->datatables->where('mwarga.idrw',$this->session->userdata('idrw'));
		}
		
		$this->datatables->where("(muser.publish = 1 OR muser.id=".$this->session->userdata('userid')." AND muser.user_tipe_id > ".$this->session->userdata('user_tipe_id').")");
		// $this->datatables->or_where('muser.id',$this->session->userdata('userid'));
		// $this->datatables->where('muser.user_tipe_id >',$this->session->userdata('user_tipe_id'));
		
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		$this->nama_user   		= $this->input->post('nama_user'); 
		$this->user_tipe_id   	= $this->input->post('user_tipe_id'); 
		$this->passtext   		= $this->input->post('passtext'); 		
		$this->username   		= $this->input->post('username'); 
		$this->password   		= $this->input->post('password'); 
		$this->email   		    = $this->input->post('email'); 
		$this->publish   		= 1; 
		
		($this->input->post('password') !='')? $this->password = md5($this->input->post('password')) : $this->password = md5('123456');
		($this->input->post('password') !='')? $this->passtext = $this->input->post('passtext') : $this->passtext = '123456';
		
		if($this->upload_img(false)){ 
			$this->db->insert('muser', $this);
			return true;
		}
	}
	function upload_img($update = false){
		if(isset($_FILES['img'])){
			if($_FILES['img']['name'] != ''){
				// $config['upload_path'] = './assets/images/news/original/';
				$config['upload_path'] = './assets/upload/avatars/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img')){
					$image_upload = $this->upload->data();
					$this->img = $image_upload['file_name']; 
					// print_r($this);exit();
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img);exit();
		if(file_exists('./assets/upload/avatars/'.$row->img) && $row->img !='') {
			unlink('./assets/upload/avatars/'.$row->img);
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('muser');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->nama_user   		= $this->input->post('nama_user'); 
		$this->user_tipe_id   	= $this->input->post('user_tipe_id'); 
		$this->passtext   		= $this->input->post('passtext'); 		
		$this->username   		= $this->input->post('username'); 
		$this->password   		= $this->input->post('password'); 
		$this->email   		= $this->input->post('email'); 
		$this->publish   		= 1; 
		
		($this->input->post('password') !='')? $this->password = md5($this->input->post('password')) : $this->password = md5('123456');
		($this->input->post('password') !='')? $this->passtext = $this->input->post('passtext') : $this->passtext = '123456';
		
		if($this->upload_img(true)){ 
			$this->db->where('id',$id);
			$this->db->update('muser',$this);
			return true;
		}
	}
	function delete($id)
	{
		$this->db->where('id',$id);
		$this->db->update('muser',array('publish' => 0));
		return true;
	}
	
	function get_kota()
	{
		$this->db->select('idkota,kota,propinsi');
		$this->db->from('mkota');
		$this->db->order_by('idkota','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function list_tipe()
	{
		$this->db->select('muser_tipe.*');
		$this->db->from('muser_tipe');
		$this->db->where('muser_tipe.id >=',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_marketing()
	{
		$this->db->select('mmarketing.*');
		$this->db->from('mmarketing');
		$this->db->order_by('nama_marketing','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	function get_muser($id)
	{
		$this->db->select('muser.*,muser_tipe.tipe_user');
		$this->db->from('muser');
		$this->db->join('muser_tipe','muser_tipe.id=muser.user_tipe_id');
		$this->db->where('muser.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function get_identitas($id)
	{
		$this->db->select('mwarga.*');
		$this->db->from('mwarga');
		$this->db->where('mwarga.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	function cek_muser($id)
	{
		$this->db->select('muser.*');
		$this->db->from('muser');
		$this->db->where('muser.username',$id);
		$this->db->where('muser.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('muser',array('publish' => $status));
		return true;
	}
}
