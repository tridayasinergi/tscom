<?php

class Kategori_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select("kategori.*");
		$this->datatables->from('kategori');
		
		$this->datatables->add_column('action', '', 'id');
		// print_r($this->datatables->last_query());exit();
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		$this->kategori   	= $this->input->post('kategori'); 
		$this->db->insert('kategori', $this);
		return true;
	}
	
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$this->kategori   	= $this->input->post('kategori'); 
		
		$this->db->where('id',$id);
		$this->db->update('kategori',$this);
		return true;
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('kategori',array('publish' => $status));
		return true;
	}
	
	function get_kategori($id)
	{
		$this->db->select('kategori.*');
		$this->db->from('kategori');
		$this->db->where('kategori.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
}
