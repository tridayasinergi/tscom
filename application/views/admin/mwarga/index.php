<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)
	
?>
<?$uri=$this->uri->segment(1);
	 $uri2=$this->uri->segment(2);	?>
<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>				
				<a href="{base_url}admin/mwarga" class="btn" data-toggle="tooltip" title="" data-original-title="Refresh"> <i class="fa fa-refresh"></i></a>
			</li>
			<li>				
				<a href="{base_url}admin/mwarga/download" class="btn" data-toggle="tooltip" title="" data-original-title="Download"> <i class="fa fa-file-excel-o"></i></a>
			</li>
           <li>
				<? if (in_array($this->session->userdata('user_tipe_id'),array('1','2','3','4','5','6','7','8'))){?>
				<a href="{base_url}admin/mwarga/add_new" class="btn" data-toggle="tooltip" title="" data-original-title="Add Warga"> <i class="fa fa-plus"></i></a>
				<?}?>
			</li>
           
		</ul>
		<h3 class="block-title">{title}</h3>
	</div>
	<div class="block-content">
		<!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
		<?= ($this->agent->is_mobile())? '<div class="table-responsive">' : '' ?>
		
			<table width="100%" class="table table-bordered table-striped table-responsive" id="datatable_index">
				<thead>
					<tr>                                    
						<th width="5%">#</th>
						<th width="10%">No KK</th>
						<th width="10%">NIK</th>
						<th width="20%">Nama</th>
						<th width="30%">Alamat</th>
						<th width="10%">Status</th>
						<th width="10%">Tools</th>					
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		<?= ($this->agent->is_mobile())? '</div>' : '' ?>
	</div>
</div>
<input type="hidden" id="datatable_search" value="{datatable_search}"/>
<input type="hidden" id="datatable_page" value="{datatable_page}"/>
<input type="hidden" id="datatable_order" value="{datatable_order}"/>


