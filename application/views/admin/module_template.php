<!DOCTYPE html>
<!--[if IE 9]>          <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Tridaya Sinergi Indonesia | <?=strip_tags($title) ?></title>

        <meta name="description" content="Tridaya Sinergi Indonesia ">
        <meta name="keyword" content="Tridaya Sinergi Indonesia, ">
        <meta name="author" content="koersina18">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
       
        <meta name="theme-color" content="#ffffff">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700"> -->
		<link rel="shortcut icon" type="image/png" href="{assets_path}logo.png"/>
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{js_path}plugins/magnific-popup/magnific-popup.min.css">
        <link rel="stylesheet" href="{js_path}plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="{js_path}plugins/select2/select2.min.css">
        <link rel="stylesheet" href="{js_path}plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="{js_path}plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="{css_path}bootstrap.min.css">
		
		<link rel="stylesheet" href="{js_path}plugins/fullcalendar/fullcalendar.min.css">
		<link rel="stylesheet" id="css-main" href="{css_path}oneui.css">
		<script src="{js_path}core/jquery.min.js"></script>
        <script src="{js_path}core/bootstrap.min.js"></script>
        <script src="{js_path}core/jquery.slimscroll.min.js"></script>
        <script src="{js_path}core/jquery.scrollLock.min.js"></script>
        <script src="{js_path}core/jquery.appear.min.js"></script>
        <script src="{js_path}core/jquery.countTo.min.js"></script>
        <script src="{js_path}core/jquery.placeholder.min.js"></script>
        <script src="{js_path}core/js.cookie.min.js"></script>
        <script src="{js_path}plugins/magnific-popup/magnific-popup.min.js"></script>
		<link rel="stylesheet" href="{js_path}plugins/datatables/jquery.dataTables.min.css">
		 <link rel="stylesheet" href="{js_path}plugins/dropzonejs/dropzone.min.css">
		<!-- Page Custom JS n CSS DTtbl -->
        <link rel="stylesheet" href="{ajax}datatables.net-bs4/css/dataTables.bootstrap4.css">
        <!-- <script src="{ajax}datatables.net-bs4/js/dataTables.bootstrap4.js"></script> -->
        <script src="{ajax}datatables.net-bs4/jquery.dataTables.min.js"></script>
        <script src="{ajax}datatable.js"></script>
       
    </head>
    <body>
       
        <?php $menu = $this->uri->segment(1); ?>
        <?php $action = $this->uri->segment(2); ?>
        <div id="page-container" class="sidebar-l sidebar-o header-navbar-fixed  side-scroll ">

            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content" style="background-color: #fff;">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>

                            <a class="h5 text-white" href="{base_url}admin/dashboard">
                                <img src="{assets_path}logo@2x.png"alt="" style="width: 84px; margin-left: 0%;">
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content side-content-full">
                            <ul class="nav-main">
                              <?php $this->load->view('admin/module_navigation'); ?>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <?php if(($menu == 'digitalguestbook' && $action == 'qrcode') || ($menu == 'digitalguestbook' && $action == 'invitation') || $action  == 'guestboard'){ ?>
            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
            </header>
            <!-- END Header -->
            <?php }elseif($menu == 'digitalguestbook'){ ?>
            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
              <?php $this->load->view('admin/module_header_report');?>
            </header>
            <!-- END Header -->
            <?php }else{ ?>
            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
              <?php $this->load->view('admin/module_header');?>
            </header>
            <!-- END Header -->
            <?php } ?>

            <!-- Main Container -->
            <main id="main-container">
              <!-- Page Content -->
              <div class="content">

                <!-- Breadcrumb -->
                <ol class="breadcrumb push-15">
                    <?php echo BackendBreadcrum($breadcrum)?>
                </ol>
                <!-- END Breadcrumb -->

                <?php $this->load->view($template);?>
              <?php if($menu != 'dashboard' && $menu != 'digitalguestbook'){ ?>
              </div>
              <?php } ?>
              <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-left">
                    <a class="font-w600" href="#" target="_blank">Koersina18 &copy; 2020</span>
                </div>
                <div class="pull-right">
                    Created with <i class="fa fa-heart text-city"></i> by <a class="font-w600" href="https://kurvasoft.com" target="_blank">Koersina18</a>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
		
        
		
		

		
        <script src="{js_path}app.js"></script>
        <script src="{js_path}basic.js"></script>
        <script src="{js_path}plugins/datatables/jquery.dataTables.min.js"></script>
		
        <script src="{js_path}plugins/select2/select2.full.min.js"></script>
        <script src="{js_path}plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script src="{js_path}pages/base_tables_datatables.js"></script>
		<link rel="stylesheet" href="{js_path}plugins/sweetalert2/sweetalert2.min.css">
		<script src="{js_path}plugins/sweetalert2/es6-promise.auto.min.js"></script>
        <script src="{js_path}plugins/sweetalert2/sweetalert2.min.js"></script>
		<script src="{js_path}plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="{js_path}plugins/masked-inputs/jquery.maskedinput.min.js"></script>
        <script src="{js_path}infinite-scroll.pkgd.min.js"></script>
        <script src="{js_path}html2canvas.js"></script>
		
		<script src="{js_path}plugins/dropzonejs/dropzone.min.js"></script>
		<script src="{js_path}plugins/jquery-ui/jquery-ui.min.js"></script>
		<script src="{js_path}plugins/fullcalendar/moment.min.js"></script>
		<script src="{js_path}plugins/fullcalendar/fullcalendar.min.js"></script>
		<script src="{js_path}plugins/fullcalendar/gcal.min.js"></script>

		
		
     
		<!--
        <script src="https://momentjs.com/downloads/moment.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.js'></script>
        <link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" />
		-->
		
        
        <!-- Global site tag (gtag.js) - Google Analytics 
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-12391936-22"></script>
		-->
       
    </body>
</html>
