<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/mwarga/add_new" class="btn" data-toggle="tooltip" title="" data-original-title=""> <i class="fa fa-plus"></i></a>
			</li>
			<li>
				<a href="{base_url}admin/mwarga" class="btn"><i class="fa fa-reply"></i></a>
			</li>			
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('admin/mwarga/simpan',array('id'=>'form1','method'=>'post','class'=>'js-validation-bootstrap  form-horizontal push-10-t')); ?>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nokk">No.KK <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="nokk" id="nokk"  onkeypress="return hanyaAngka(event)"  placeholder="" value="{nokk}" />					
				</div>
								
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="status_kk">Hubungan Keluarga <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select name="status_kk" tabindex="1" id="status_kk" data-placeholder="Silahkan Pilih Hubungan Keluarga" class="form-control  input-sm">
						
						<option value="0" <?=($status_kk=='0'?'selected':'')?>>Silahkan Isi Hubungan Keluarga</option>
						<?foreach($list_hubungan as $row){?>
						<option value="<?=$row->id?>" <?=($status_kk==$row->id?'selected':'')?>><?=$row->hubungan_kk?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idkk">Nama Kepala Keluarga <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select name="idkk" tabindex="1" id="idkk" data-placeholder="Silahkan Pilih Nama Kepala Keluarga " class="form-control  input-sm">
						<option value="" <?=($idkk==''?'selected':'')?>>-Silahkan Pilih Nama KK-</option>
						<option value="0" <?=($idkk=='0'?'selected':'')?>>Atas Nama Sendiri</option>
						<?foreach($list_kk as $row){?>
						<option value="<?=$row->id?>" <?=($idkk==$row->id?'selected':'')?>><?=$row->nik.' - '.$row->nama?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group" id="div_nik">
				<label class="col-md-2 control-label" for="nik">NIK KTP <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="nik" id="nik" onkeypress="return hanyaAngka(event)" placeholder="" value="{nik}" />
					<div id="val-nik-error" class="help-block animated fadeInDown"></div>					
				</div>	
				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama">Nama <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="nama" id="nama" placeholder="" value="{nama}" />					
				</div>		
				<label class="col-md-2 control-label" for="jenis_kelamin">Jenis Kelamin <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="jenis_kelamin" name="jenis_kelamin" size="1">
						<option value="" <?=($jenis_kelamin==''?'selected':'')?>>- Jenis Kelamin -</option>
						<option value="Laki-Laki" <?=($jenis_kelamin=='Laki-Laki'?'selected':'')?>>Laki-Laki</option>
						<option value="Perempuan" <?=($jenis_kelamin=='Perempuan'?'selected':'')?>>Perempuan</option>
						
					</select>
				</div>
				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="tempat_lahir">Tempat Lahir <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="tempat_lahir" id="tempat_lahir" placeholder="" value="{tempat_lahir}" />					
				</div>	
				<label class="col-md-2 control-label" for="tgl_lahir">Tgl Lahir <span class="text-danger">*</span></label>				
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="tgl_lahir" id="tgl_lahir" placeholder="" value="{tgl_lahir}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_ayah">Nama Ayah <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<input type="text" class="form-control input-sm" name="nama_ayah" id="nama_ayah" placeholder="" value="{nama_ayah}" />					
				</div>	
				<label class="col-md-2 control-label" for="nama_ibu">Nama Ibu <span class="text-danger">*</span></label>				
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="nama_ibu" id="nama_ibu" placeholder="" value="{nama_ibu}" />	
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="status_nikah">Status Nikah <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="status_nikah" name="status_nikah" size="1">
						<option value="" <?=($status_nikah==''?'selected':'')?>>-Tanpa Status-</option>
						<option value="Kawin" <?=($status_nikah=='Kawin'?'selected':'')?>>Kawin</option>
						<option value="Belum Kawin" <?=($status_nikah=='Belum Kawin'?'selected':'')?>>Belum Kawin</option>
						<option value="Cerai Hidup" <?=($status_nikah=='Cerai Hidup'?'selected':'')?>>Cerai Hidup</option>
						<option value="Cerai Mati" <?=($status_nikah=='Cerai Mati'?'selected':'')?>>Cerai Mati</option>
						
					</select>
				</div>
				<label class="col-md-2 control-label" for="idagama">Agama <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select name="idagama" tabindex="1" id="idagama" data-placeholder="Agama " class="form-control  input-sm">
						<option value="" <?=($idagama==''?'selected':'')?>>- Agama -</option>
						<?foreach($list_agama as $row){?>
						<option value="<?=$row->id?>" <?=($idagama==$row->id?'selected':'')?>><?=$row->agama?></option>
						<?}?>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="tgl_nikah">Tanggal Nikah <span class="text-danger">*</span></label>
				<div class="col-md-3">
					<div class="js-datetimepicker input-group date">
						<input  type="text" class="js-datepicker form-control input-sm" data-date-format="dd-mm-yyyy" name="tgl_nikah" id="tgl_nikah" placeholder="" value="{tgl_nikah}" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idpendidikan">Pendidikan </label>
				<div class="col-md-4">
					<select name="idpendidikan" tabindex="1" id="idpendidikan" data-placeholder="Pekerjaan " class="form-control  input-sm">
						<option value="0" <?=($idpendidikan=='0'?'selected':'')?>>- Belum dilengkapi-</option>
						<?foreach($list_pendidikan as $row){?>
						<option value="<?=$row->id?>" <?=($idpendidikan==$row->id?'selected':'')?>><?=$row->pendidikan?></option>
						<?}?>
					</select>
				</div>	
				<label class="col-md-2 control-label" for="idpekerjaan">Pekerjaan </label>
				<div class="col-md-4">
					<select name="idpekerjaan" tabindex="1" id="idpekerjaan" data-placeholder="Pekerjaan " class="form-control  input-sm">
						<option value="" <?=($idpekerjaan==''?'selected':'')?>>- Pekerjaan -</option>
						<?foreach($list_pekerjaan as $row){?>
						<option value="<?=$row->id?>" <?=($idpekerjaan==$row->id?'selected':'')?>><?=$row->pekerjaan?></option>
						<?}?>
					</select>
				</div>				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idekonomi">Status Ekonomi </label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="idekonomi" name="idekonomi" size="1">
						<option value="0" <?=($idekonomi==''?'selected':'')?>>- Tentukan Status Ekonomi -</option>
						<?foreach($list_status_ekonomi as $row){?>
						<option value="<?=$row->id?>" <?=($idekonomi==$row->id?'selected':'')?>><?=$row->gol_ekonomi?></option>
						<?}?>
						
					</select>
				</div>
				<label class="col-md-2 control-label" for="gol_darah">Gol. Darah<span class="text-danger">*</span></label>				
				<div class="col-md-3">
					<input type="text" class="form-control input-sm" name="gol_darah" id="gol_darah" placeholder="" value="{gol_darah}" />	
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idkota">Kota <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<select name="idkota" tabindex="1" id="idkota" data-placeholder="Kota " class="form-control  input-sm">
						<? if($idkota){?>
							<option value="{idkota}"><?=$kota.' - '.$propinsi.'.'?></option>
						<?}?>
					</select>
				</div>				
			</div>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="idkecamatan">Kecamatan <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="idkecamatan" name="idkecamatan" size="1">
						<option value="" <?=($idkecamatan==''?'selected':'')?>>- Silahkan Pilih Kecamatan -</option>
						<?foreach($list_kecamatan as $row){?>
						<option value="<?=$row->id?>" <?=($idkecamatan==$row->id?'selected':'')?>><?=$row->kecamatan?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-2 control-label" for="iddesa">Desa/Kelurahan <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="iddesa" name="iddesa" size="1">
						<option value="" <?=($iddesa==''?'selected':'')?>>- Silahkan Pilih Desa -</option>
						<?foreach($list_desa as $row){?>
						<option value="<?=$row->id?>" <?=($iddesa==$row->id?'selected':'')?>><?=$row->desa?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="idrw">RW <span class="text-danger">*</span></label>
				<div class="col-md-2">
					<select class="form-control input-sm" id="idrw" name="idrw" size="1">
						<option value="" <?=($idrw==''?'selected':'')?>>- Silahkan Pilih RW -</option>
						<?foreach($list_rw as $row){?>
						<option value="<?=$row->id?>" <?=($idrw==$row->id?'selected':'')?>><?=$row->rw?></option>
						<?}?>
					</select>
				</div>
				<label class="col-md-1 control-label" for="idrt">RT <span class="text-danger">*</span></label>
				<div class="col-md-2">
					<select class="form-control input-sm" id="idrt" name="idrt" size="1">
						<option value="" <?=($idrt==''?'selected':'')?>>- Silahkan Pilih RT -</option>
						<?foreach($list_rt as $row){?>
						<option value="<?=$row->id?>" <?=($idrt==$row->id?'selected':'')?>><?=$row->rt?></option>
						<?}?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat">Blok / No Rumah <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="alamat" id="alamat" placeholder="" value="{alamat}" />					
					<input type="hidden" class="form-control input-sm" name="edit" id="edit" placeholder="" value="{edit}" />					
					<input type="hidden" class="form-control input-sm" name="valid_nik" id="valid_nik" placeholder="" value="1" />					
					<input type="hidden" class="form-control input-sm" name="nik_asal" id="nik_asal" placeholder="" value="{nik}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat_lengkap">Alamat Lengkap <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" readonly id="alamat_lengkap" name="alamat_lengkap" rows="3" placeholder="Alamat Lengkap..">{alamat_lengkap}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="status_warga_id">Status <span class="text-danger">*</span></label>
				<div class="col-md-4">
					<select class="form-control input-sm" id="status_warga_id" name="status_warga_id" size="1">
						<option value="" <?=($status_warga_id==''?'selected':'')?>>- Silahkan Pilih Status -</option>
						<?foreach($list_status as $row){?>
						<option value="<?=$row->id?>" <?=($status_warga_id==$row->id?'selected':'')?>><?=$row->status_warga?></option>
						<?}?>
					</select>
				</div>
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="alamat_ktp">Alamat KTP <span class="text-danger">*</span></label>
				<div class="col-md-10">
					<textarea class="form-control input-sm" id="alamat_ktp" name="alamat_ktp" rows="3" placeholder="Alamat KTP..">{alamat_ktp}</textarea>
				</div>					
			</div>
			<div class="form-group">
				<input type="hidden" class="form-control input-sm" name="st_lanjut" id="st_lanjut" placeholder="" value="{st_lanjut}" />					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" id="btn_simpan" type="submit">Simpan</button>
					<a href="{base_url}admin/mwarga" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>

