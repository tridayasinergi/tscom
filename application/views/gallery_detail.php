<section class="section-padding bg-light-white listing-detail">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="listing-detail-heading mb-xl-20">
					<h3 class="no-margin text-theme">Gallery Detail</h3>
				</div>
				<div class="image-sec-table padding-20 mb-xl-20">
					<div class="row">
						<div class="col-lg-6">
							<div class="images-wrapper mb-md-80">
								<div class="detail-page-slider-for mb-xl-20 magnific-gallery">
									<?foreach($list_gallery as $row){?>
									<div class="slide-item">
										<a href="<?=$gallery_path?><?=$row->image_path?>" class="popup">
											<img loading="lazy" src="<?=$gallery_path?><?=$row->image_path?>" class="img-fluid image-fit" alt="property img">
										</a>
									</div>
									<?}?>
									
								</div>
								<div class="detail-page-slider-nav">
									<?foreach($list_gallery as $row){?>
									<div class="slide-item">
										<a href="#">
											<img src="<?=$gallery_path?>thumbs/<?=$row->image_path?>" class="img-fluid image-fit" alt="property img">
										</a>
									</div>
									<?}?>
									
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="property-details-box">
								<div class="property-details-wrap">
									<div class="price-box mb-xl-20">
										<span class="price text-custom-blue"><?=$nama_gallery?></span>
										
									</div>
									<div class="details">
										<div class="text text-light-white">Tanggal Create: <span class="text-theme"><?=tsi_date($tanggal_create)?></span></div>
										<div class="text text-light-white">Tanggal Kegiatan: <span class="text-theme"><?=tsi_date($tanggal_gallery)?></span></div>
										<div class="text text-light-white">Upload: <span class="text-theme">Administrator</span></div>
										<div class="text text-light-white">Jumlah Foto: <span class="text-theme"><?=$jml_foto?> Photo</span></div>
										
									</div>
									<div class="details-2">
										<div class="text text-light-white">Keterangan: </div>
										<p>{keterangan}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
		
		</div>
	</div>
	<div class="container">
		<div class="section-header">
			<div class="section-heading">
				<h5 class="text-custom-blue">Other</h5>
				<h3 class="text-theme fw-700">Gallery</h3>
			</div>
			
		</div>
		<div class="row">
			<div class="col-12">
				<div class="swiper-container featured-property-slider">
					<div class="swiper-wrapper">
						<? foreach($list_other as $row){?>
						<div class="swiper-slide">
							<div class="property-grid-box">
								<div class="property-grid-wrapper">
									<div class="property-img animate-img">
										<a href="{base_url}page/gallery_detail/<?=$row->id.'/'.strtolower(rename_url($row->nama_gallery))?>">
											<img loading="lazy" src="<?=$gallery_path?><?=$row->image_path?>" class="img-fluid full-width" alt="#" style="width: 383px; height: 282px;">
										</a>
										<div class="property-address bg-custom-blue">
											<a href="{base_url}page/gallery_detail/<?=$row->id.'/'.strtolower(rename_url($row->nama_gallery))?>"><i class="flaticon-pin"></i><?=$row->nama_gallery?> </a>
										</div>
									</div>
									
									<div class="property-grid-footer">
										<a href="{base_url}page/gallery_detail/<?=$row->id.'/'.strtolower(rename_url($row->nama_gallery))?>" class="text-theme fs-14 fw-600">Lihat Gallery <span class="flaticon-right-arrow ml-1"></span></a>
										
									</div>
								</div>
							</div>
						</div>
						<?}?>
					</div>
				</div>
				<!-- Add Arrows -->
				<div class="slider-btn">
					<div class="featured-button-next swiper-button-next">
						<span class="flaticon-arrow-1"></span>
					</div>
					<div class="featured-button-prev swiper-button-prev">
						<span class="flaticon-left"></span>
					</div>
				</div>
				<!-- Add Pagination -->
				<div class="featured-pagination swiper-pagination"></div>
			</div>
		</div>
	</div>
</section>
<section class="section-padding">
	
</section>