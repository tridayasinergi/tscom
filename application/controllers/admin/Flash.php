<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flash extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('flash_model','flash_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Flash';
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("FLASH",'#'),
								array("List",'admin\flash')
								);
		$data['template'] = 'admin/flash/index';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
		$content     		= $this->input->post('content');
		
		$where='';
		if ($content !=''){
			$where .=" AND content LIKE '%".$content."%'";
		}
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *from flash 
				WHERE id is not null ".$where."
				ORDER BY urutan asc
			) as T ORDER BY urutan ASC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/flash/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            // $row[] = $no;
            $row[] = $r->urutan;
            $row[] = $r->content;
            $row[] = $r->url;

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Flash - Addnew');
		$data['id'] = "";
		$data['url'] = "#";
		$data['urutan'] = "";
		$data['content'] = "";
		$data['edit'] = "0";
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("TAMBAH",'#'),
								array("FLASH NEWS",'admin\flash'));
		$data['judul'] = 'Flash - Input Baru';
		$data['template'] = 'admin/flash/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		if ($edit){
		// print_r($this->input->post());exit();
			// print_r($this->input->post('noid'));exit();
			
			if ($this->flash_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/flash/editor/$id/");				
			}
		}else{		
			if ($this->flash_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/flash/add_new/");
			}
			
		}
		
	}
	
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'flash - Edit');
			if($row = $this->flash_model->get_flash($id))
			{
				$data['id'] = $id;
				$data['url'] = $row->url;
				$data['content'] = $row->content;
				$data['urutan'] = $row->urutan;
				
				$data['edit'] = 1;
			}
		
		if ($id){
			$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("FLASH NEWS",'admin\flash'));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'admin\flash',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['error'] = '';
		$data['judul'] = 'FLASH NEWS - Edit';
		$data['template'] = 'admin/flash/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->flash_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/flash");
		}
	}
}
?>