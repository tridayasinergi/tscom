<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/gallery" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open('admin/gallery/simpan_add','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_gallery">Judul<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" readonly class="form-control input-sm" name="nama_gallery" id="nama_gallery" placeholder="" value="{nama_gallery}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="nama_gallery">Keterangan<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" readonly class="form-control input-sm" name="ket" id="ket" placeholder="" value="{keterangan}" />					
				</div>					
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="des">Deskripsi<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="des" id="des" placeholder="" value="" />					
				</div>					
			</div>
			
			
			
			
			
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
		<?php echo form_close() ?>
			 <div class="row">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<div class="block">
						<form action="{base_url}admin/gallery/upload_image" enctype="multipart/form-data" class="dropzone" id="image-upload">
							<input type="hidden" class="form-control" name="gallery_id" id="gallery_id" value="{id}" readonly>
							<input type="hidden" class="form-control" name="keterangan" id="keterangan" value="" readonly>
							<input type="hidden" class="form-control" name="deskripsi" id="deskripsi" value="" readonly>
							<div>
							  <h5></h5>
							</div>
						  </form>
					</div>
				</div>
			</div>
			
	</div>
	<div class="block-content block-content-narrow">
       
	</div>
</div>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/gallery" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> LIST GALLERY</h3>
	</div>
	<div class="block-content">
			<div class="row items-push js-gallery-advanced">
				<?foreach($list_detail as $row){?>
				<div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn">
					<div class="img-container fx-img-rotate-r">
						<img class="img-responsive" src="{gallery_path}<?=$row->image_path?>" alt="">
						<div class="img-options">
							<div class="img-options-content">
								<a class="btn btn-sm btn-default img-lightbox" href="{gallery_path}<?=$row->image_path?>">
									<i class="fa fa-search-plus"></i> View
								</a>
								<div class="btn-group btn-group-sm">
									<a class="btn btn-default" href="{base_url}admin/gallery/hapus_photo/<?=$row->id?>/<?=$row->gallery_id?>"><i class="fa fa-times"></i> Delete</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?}?>
				
				
				
				
	</div>
</div>
</div>
<script src="{plugins_path}jquery-number/jquery.number.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
			// refresh_image();
			Dropzone.autoDiscover = false;
  
			var myDropzone = new Dropzone(".dropzone", { 
			   autoProcessQueue: true,
			   maxFilesize: 30,
			});
			
			// myDropzone.on("complete", function(file) {
			  
			  // // refresh_image();
			  // myDropzone.removeFile(file);
			  
			// });
			myDropzone.on("queuecomplete", function (file) {
				  // alert("All files have uploaded ");
				  // location.reload();
				  konfirmasi();
			  });
			// $('#uploadFile').click(function(){
			   // myDropzone.processQueue();
			// });
			// $('#reload').click(function(){
			   // location.reload();
			// });
    });
	function konfirmasi(){
		swal({
				title: "File Selesai Upload",
				text : "Reload Page ?",
				type : "success",
				showCancelButton: true,
				confirmButtonText: "Ya",
				confirmButtonColor: "#34a263",
				cancelButtonText: "Batalkan",
			}).then(function() {
				location.reload();
			});
	}
	$("#des").keyup(function(){
		$("#deskripsi").val($(this).val())	
	});
	$("#btn_refresh").click(function(){
		konfirmasi();
	});

</script>