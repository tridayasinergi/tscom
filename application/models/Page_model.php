<?php
class Page_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	
	function get_content_page($str_key){
		$this->db->from('pages');
		
		$this->db->where('pages.str_key',$str_key);
		$this->db->where('pages.publish',1);
		$query = $this->db->get();	
		return $query->row_array();
	}
	function get_kategori($str_key){
		$this->db->from('kategori');
		
		$this->db->where('kategori.id',$str_key);
		$this->db->where('kategori.publish',1);
		$query = $this->db->get();	
		return $query->row('kategori');
	}
	function count_berita($kategori){
		$where='';
		if ($kategori !='all'){
			$where=" AND news.kategori='$kategori'";
		}
		if ($this->input->post('cari_text')){
			$cari=$this->input->post('cari_text');
			$where=" AND (news.title LIKE '%".$cari."%' OR news.content LIKE '%".$cari."%')";
		}
		$q="SELECT COUNT(news.id) as jml from news
			WHERE news.publish='1' ".$where;
		return $this->db->query($q)->row('jml');
	}
	
	function list_berita($kategori,$limit='',$offset)
    {
		// print_r();exit();
		$str_limit='';
		if ($limit == ''){
			$str_limit=' LIMIT '.$offset;
		}else{
			$str_limit=' LIMIT '.$limit.','.$offset;
		}
		$where='';
		if ($kategori !='all'){
			$where=" AND B.kategori='$kategori'";
		}
		if ($this->input->post('cari_text')){
			$cari=$this->input->post('cari_text');
			$where=" AND (B.title LIKE '%".$cari."%' OR B.content LIKE '%".$cari."%')";
		}
		$q="SELECT B.id,B.title,B.sinopsis,B.created_at,K.kategori,A.author_ref,B.img_news from news B
				LEFT JOIN kategori K ON K.id=B.kategori
				LEFT JOIN mauthor A ON A.id=B.author
				WHERE B.publish='1' ".$where."
				ORDER BY B.created_at DESC
				".$str_limit;
		// print_r($q);exit();
		return $this->db->query($q)->result();		
	}
	function get_content_news($str_key){
		// $this->db->from('news');
		
		// $this->db->where('news.id',$str_key);
		// $this->db->where('news.publish',1);
		// $query = $this->db->get();	
		$q="SELECT B.id,B.title,B.sinopsis,B.created_at,K.kategori as kategori_berita,A.author_ref,B.img_news,B.content,B.jml_klik from news B
				LEFT JOIN kategori K ON K.id=B.kategori
				LEFT JOIN mauthor A ON A.id=B.author
				WHERE B.publish='1' AND B.id='$str_key'
		";
		return $this->db->query($q)->row_array();
	}
	function list_tag_berita($str_key){
		
		$q="SELECT H.id,M.tag FROM news_tag H
			LEFT JOIN mtags M ON M.id=H.idtag
			WHERE H.idberita='$str_key'
		";
		return $this->db->query($q)->result();
	}
	function list_all_tag(){
		
		$q="SELECT H.id,M.tag FROM news_tag H
			INNER JOIN mtags M ON M.id=H.idtag
			GROUP BY H.idtag

		";
		return $this->db->query($q)->result();
	}
	function list_gallery($limit=''){
		if ($limit !=''){
			$str_limit=' LIMIT '.$limit;
		}else{
			$str_limit='';
		}
		$q="SELECT G.id,G.tanggal_gallery,G.nama_gallery,G.keterangan,D.image_path,COUNT(D.id) as jml_foto from gallery G
				LEFT JOIN gallery_detail D ON D.gallery_id=G.id
				WHERE G.publish='1' AND G.deleted='0'
				GROUP BY G.id
				ORDER BY G.id DESC
				
				".$str_limit;
		return $this->db->query($q)->result();
	}
	function list_gallery2($limit='',$offset){
		if ($limit == ''){
			$str_limit=' LIMIT '.$offset;
		}else{
			$str_limit=' LIMIT '.$limit.','.$offset;
		}
		$q="SELECT G.id,G.tanggal_gallery,G.nama_gallery,G.keterangan,D.image_path,COUNT(D.id) as jml_foto from gallery G
				LEFT JOIN gallery_detail D ON D.gallery_id=G.id
				WHERE G.publish='1' AND G.deleted='0'
				GROUP BY G.id
				ORDER BY G.id DESC
				
				".$str_limit;
		return $this->db->query($q)->result();
	}
	function list_video($limit='',$offset){
		if ($limit == ''){
			$str_limit=' LIMIT '.$offset;
		}else{
			$str_limit=' LIMIT '.$limit.','.$offset;
		}
		$q="SELECT M.id,M.title,M.created_at,M.publish,K.kategori as nama_kategori,M.kategori, A.author_ref,M.str_key ,M.content,M.link
				from page_video M
				LEFT JOIN kategori K ON M.kategori=K.id
				LEFT JOIN mauthor A ON A.id=M.author
				WHERE M.publish='1'
				ORDER BY M.created_at DESC,M.id DESC
				".$str_limit;
		return $this->db->query($q)->result();
	}
	function list_testimoni($limit='',$offset){
		if ($limit == ''){
			$str_limit=' LIMIT '.$offset;
		}else{
			$str_limit=' LIMIT '.$limit.','.$offset;
		}
		$q="SELECT M.id,M.title,M.created_at,M.publish,K.kategori as nama_kategori,M.kategori, A.author_ref,M.str_key ,M.content,M.link
				from page_testimoni M
				LEFT JOIN kategori K ON M.kategori=K.id
				LEFT JOIN mauthor A ON A.id=M.author
				WHERE M.publish='1'
				ORDER BY M.created_at DESC,M.id DESC
				".$str_limit;
		return $this->db->query($q)->result();
	}
	function count_video(){
		
		$q="SELECT COUNT(page_video.id) as jml from page_video
			WHERE page_video.publish='1' ";
		return $this->db->query($q)->row('jml');
	}
	function count_testimoni(){
		
		$q="SELECT COUNT(page_testimoni.id) as jml from page_testimoni
			WHERE page_testimoni.publish='1' ";
		return $this->db->query($q)->row('jml');
	}
	function count_gallery(){
		
		$q="SELECT COUNT(gallery.id) as jml from gallery
			WHERE gallery.publish='1' AND gallery.deleted='0' ";
		return $this->db->query($q)->row('jml');
	}
	function list_other($id){
		
		$q="SELECT G.id,G.tanggal_gallery,G.nama_gallery,G.keterangan,D.image_path,COUNT(D.id) as jml_foto from gallery G
				LEFT JOIN gallery_detail D ON D.gallery_id=G.id
				WHERE G.publish='1' AND G.deleted='0' AND G.id !='$id' AND G.id < '$id'
				GROUP BY G.id
				ORDER BY G.id DESC LIMIT 10";
		return $this->db->query($q)->result();
	}
	function list_gallery_detail($id){
		
		$q="SELECT *from gallery_detail D
			WHERE D.gallery_id='$id'";
		return $this->db->query($q)->result();
	}
	function get_gallery($id){
		
		$q="SELECT G.id,G.tanggal_gallery,G.nama_gallery,G.keterangan,D.image_path,COUNT(D.id) as jml_foto,G.tanggal_create from gallery G
				LEFT JOIN gallery_detail D ON D.gallery_id=G.id
				WHERE G.publish='1' AND G.deleted='0' AND G.id='$id'
				GROUP BY G.id
				ORDER BY G.id DESC";
		return $this->db->query($q)->row_array();
	}
	function top_kategori(){
		$q="SELECT *FROM (
		SELECT kategori.kategori,COUNT(news.id) as jml,kategori.id from kategori
		LEFT JOIN news ON news.kategori=kategori.id AND news.publish='1'
		GROUP BY kategori.id )
		T WHERE T.jml > 0
		ORDER BY T.jml DESC";
		return $this->db->query($q)->result();
	}
	function top_populer(){
		$q="SELECT B.id,B.title,B.content,B.sinopsis,B.created_at,B.kategori,A.author_ref,B.img_news,K.kategori as nama_kategori from news B
			LEFT JOIN kategori K ON K.id=B.kategori
			LEFT JOIN mauthor A ON A.id=B.author

			WHERE B.publish='1'
			ORDER BY B.jml_klik DESC
			LIMIT 6
		";
		return $this->db->query($q)->result();
	}
	function recent_post($limit='5'){
		$q="SELECT B.id,B.title,B.content,B.sinopsis,B.created_at,B.kategori,A.author_ref,B.img_news,K.kategori as nama_kategori from news B
			LEFT JOIN kategori K ON K.id=B.kategori
			LEFT JOIN mauthor A ON A.id=B.author

			WHERE B.publish='1'
			ORDER BY B.id DESC
			LIMIT ".$limit."
		";
		return $this->db->query($q)->result();
	}
	
}

?>