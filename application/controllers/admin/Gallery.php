<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('gallery_model','gallery_model'); 
   }

	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'All Gallery';
		$data['template']    = 'admin/gallery/index';
		$data['ajax_url'] = 'admin/gallery/getDataList'; 
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("GALLERY",'#'),
								array("List",'admin/gallery')
								);
		
	
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	
	function get_index(){
		//:content,:title,kategori:,publish:publish,author:author,
		$content     		= $this->input->post('content');
		$title     		= $this->input->post('title');
		$publish     		= $this->input->post('publish');
		
		$where='';
		if ($content !=''){
			$where .=" AND H.keterangan LIKE '%".$content."%'";
		}
		if ($title !=''){
			$where .=" AND H.nama_gallery LIKE '%".$title."%'";
		}
		
		if ($publish !='#'){
			$where .=" AND H.publish = '".$publish."'";
		}
		
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT H.id,H.tanggal_gallery,H.nama_gallery,H.keterangan,H.publish,COUNT(D.id) jml from gallery H
			LEFT JOIN gallery_detail D ON D.gallery_id=H.id
			WHERE H.deleted='0' ".$where."
			GROUP BY H.id
			
			) as T ORDER BY id DESC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/gallery/');
        foreach ($list as $r) {
            $no++;
            $row = array();

            $row[] = $no;
            $row[] = $r->tanggal_gallery;
            $row[] = $r->nama_gallery;
            $row[] = $r->keterangan;
            $row[] = $r->jml.' Picture';

			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>';
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>';
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
				$aksi .= '<a href="'.$url.'add/'.$r->id.'" type="button"  title="Photo" class="btn btn-xs btn-info"><i class="fa fa-file-picture-o"></i></a>';		
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Unpublish" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
				// $aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('page', 'Gallery - Addnew');
		$data['id'] = "";
		$data['tanggal_gallery'] = "";
		$data['nama_gallery'] = "";
		$data['keterangan'] = "";
		
		$data['edit'] = "0";
		
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("Gallery",'#'),
								array("List",'admin/gallery')
								);
		
		$data['title'] = 'Gallery - Input Baru';
		$data['template'] = 'admin/gallery/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->gallery_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/gallery/editor/$id/");				
			}
		}else{		
			if ($this->gallery_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/gallery/add_new/");
			}
			
		}
		
	}
	function simpan_add()
	{	
		$id=$this->input->post('id');
			if ($this->gallery_model->add()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/gallery/add/$id");
			}
			
		
	}
	
	
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		// print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'gallery/manage';
		$data['list_tipe'] = $this->gallery_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->gallery_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'Gallery',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("Gallery",'Gallery',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Gallery - Edit');
			if($row = $this->gallery_model->get_gallery($id))
			{
				$data['id'] = $id;
				// $data['gallery_in'] = $row->gallery_in;
				$data['kategori'] = $row->kategori;
				$data['nama_gallery'] = $row->nama_gallery;
				$data['keterangan'] = $row->keterangan;
				$data['tanggal_gallery'] = HumanDateShort($row->tanggal_gallery);
				$data['created_at'] = date_format(date_create($row->created_at),'d-m-Y');
				// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
				$data['edit'] = 1;
				
			}
		
		// print_r($data);exit();
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("GALLERY",'#'),
								array("List",'admin\gallery')
								);
		$data['error'] = '';
		$data['title'] = 'Gallery - Edit';
		$data['template'] = 'admin/gallery/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function add($id=0)
	{
		$this->session->unset_userdata('searchkey');
		
			$this->session->set_userdata('page', 'Gallery - Edit');
			if($row = $this->gallery_model->get_gallery($id))
			{
				$data['id'] = $id;
				$data['keterangan'] = strip_tags($row->keterangan);
				$data['img'] = 'default.jpg';
				$data['nama_gallery'] = $row->nama_gallery;
				$data['created_at'] = date_format(date_create($row->created_at),'d-m-Y');
				// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
				$data['edit'] = 1;
				
			}
		
		// print_r($data);exit();
		$data['list_tipe'] = $this->gallery_model->list_tipe();
		$data['list_detail'] = $this->gallery_model->list_detail($id);
		// print_r($data['list_detail']);exit();
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("UPLOAD GALLERY",'#'),
								array("List",'berita')
								);
		$data['error'] = '';
		$data['title'] = 'Gallery - Upload';
		$data['template'] = 'admin/gallery/add';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->gallery_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/gallery");
		}
	}
	function hapus_photo($id,$gallery_id){
		
		if ($this->gallery_model->hapus_photo($id)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/gallery/add/".$gallery_id);
		}
	}
	function test_rename(){
		$tmpFile='c1 dalam Pernajalan .jpg';
			print_r(RemoveSpecialChar($tmpFile));
	}
	public function upload_image() {
       $uploadDir = './assets/img/gallery';
       $uploadDir_thumb = './assets/img/gallery/thumbs/';
		if (!empty($_FILES)) {
			 
			 $tmpFile2 = $_FILES['file']['tmp_name'];
			 $tmpFile = $_FILES['file']['tmp_name'];
			 $nama_asal=$_FILES['file']['name'];
			 $filename = $uploadDir.'/'.time().'-'.RemoveSpecialChar($nama_asal);
			 $file_name = time().'-'.RemoveSpecialChar($nama_asal);
			 // $filename2 = $uploadDir_thumb.'/'.time().'-'.RemoveSpecialChar($nama_asal);
			 move_uploaded_file($tmpFile,$filename);
			 
			 $this->load->library('myimage');	
			 
			 $this->myimage->load($filename);			
			 $this->myimage->resizeToWidth(80);
			 $this->myimage->save($uploadDir_thumb.$file_name);
			
			 // copy($uploadDir.'/'.$file_name,$uploadDir_thumb.$file_name);
			 // move_uploaded_file($tmpFile2,$uploadDir_thumb.$file_name);
			 
			// $compressed = compress_image($tmpFile, $upload, 70);
			
			$gallery_id = $this->input->post('gallery_id');
			$deskripsi = $this->input->post('deskripsi');
			$keterangan = $this->input->post('keterangan');
			 
			$detail 						= array();
			$detail['gallery_id'] 	= $gallery_id;
			$detail['keterangan'] 	= $keterangan;
			$detail['deskripsi'] 	= $deskripsi;
			$detail['image_path']	= $file_name;
			$this->db->insert('gallery_detail', $detail);
		}
    }
	function compress_image($source_url, $destination_url, $quality) {
		$info = getimagesize($source_url);
		if ($info['mime'] == 'image/jpeg'){$image = imagecreatefromjpeg($source_url);}
		else if ($info['mime'] == 'image/gif'){$image = imagecreatefromgif($source_url);}
		else if ($info['mime'] == 'image/png'){$image = imagecreatefrompng($source_url);}
		imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}
}
?>