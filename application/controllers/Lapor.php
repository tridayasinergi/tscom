<?php

class Lapor extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//permission_basic_admin($this->session);
		$this->load->model('lapor_model','lapor_model');
	}
	function index(){
		$data = array();	
		$data['template'] = 'lapor';			
		$data['email'] = '';			
		$data['nama'] = '';			
		$data['pertanyaan'] = '';			
		$data['img'] = '';			
		$data['perihal_list'] =$this->lapor_model->perihal_list();			
		$data['RT_list'] =$this->lapor_model->RT_list();			
		
	
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function data_lapor(){
		$data = array();	
		$data['template'] = 'lapor_detail';			
		$data['email'] = '';			
		$data['nama'] = '';			
		$data['pertanyaan'] = '';			
		$data['img'] = '';			
		$data['lapor_list'] =$this->lapor_model->lapor_list();			
		$data['RT_list'] =$this->lapor_model->RT_list();			
		
	
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function services(){
		$data['menu'] = 'menu.services';
		$data['template'] = 'services';
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function simpan(){
		// print_r($this->input->post());exit();
		$this->lapor_model->simpan();
		$this->session->set_flashdata('confirm', true);
		$this->session->set_flashdata('message_flash', 'data telah disimpan.');
		redirect("lapor/data_lapor");				
	}
	function services_info($services_id){
		$row = $this->lapor_model->get_services_detail($services_id);
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			$data['menu'] = 'menu.services';
			$data['template'] = 'service_detail';
			// $data['active'] = 'about us';
            
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}		
	}
	function project(){
		$data['template'] = 'project';
		$data['menu'] = 'menu.project';
		$data['services_list'] = $this->lapor_model->services_list();
		$data['project_list'] = $this->lapor_model->project_list();
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function project_info($id){
		$row = $this->lapor_model->get_project_detail($id);
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['template'] = 'project_detail';
			$data['menu'] = 'menu.project';
            
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}		
	}
	function news(){
		$data['template'] = 'news';
		$data['menu'] = 'menu.news';
		$data['news_list'] = $this->lapor_model->news_list();
		$data = array_merge($data,basic_info());
		$this->parser->parse('index',$data);
	}
	function news_read($id){
		$row = $this->lapor_model->get_news_detail($id);
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'created_at' => $row->created_at,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['template'] = 'news_detail';
			$data['menu'] = 'menu.news';
            $data['list_news'] = $this->lapor_model->list_news($id);
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}		
	}
	function contact(){
		$row = $this->lapor_model->contact();
		if(isset($row->id)){
			$data = array(
					'company_name' => $row->company_name,
					'building' => $row->building,
					'alamat' => $row->alamat,
					// 'img' => $row->img,
					'short_about_us_en' => $row->short_about_us_en,
					'short_about_us_in' => $row->short_about_us_in,
					'telepon' => $row->telepon,
					'fax' => $row->fax,
					'contact_person' => $row->contact_person,
					'telepon_cp' => $row->telepon_cp,
					'email_cp' => $row->email_cp,
					'email' => $row->email,
					);	
			$data['template'] = 'contact';
			$data['menu'] = 'menu.kontak';
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}		
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */