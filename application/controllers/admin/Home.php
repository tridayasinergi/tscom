<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
   {
	   print_r('SINI');exit();
   	  parent::__construct();
      if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
	  $this->load->model('dashboard_model','dashboard_model');
   }
   
	
	function index()
    {
        $data = array();
        $data['title']      = 'Dashboard';
        $data['template']    = 'admin/dashboard';
        $data['all']    = $this->dashboard_model->get_total_warga();
        $data['kk']    = $this->dashboard_model->get_total_warga('',1);
        $data['laki']    = $this->dashboard_model->get_total_warga('Laki-Laki','');
        $data['perempuan']    = $this->dashboard_model->get_total_warga('Perempuan','');
		$st=$this->dashboard_model->get_total_status();
		if ($st){
			$totalnya=$st->warga;
		// print_r($totalnya);exit();
			
			$data['tot']=$st->warga;
			$data['tot_persen']=$st->warga / $totalnya * 100;
			$data['tot_ktp']=$st->ktp;
			$data['ktp_persen']=$st->ktp / $totalnya * 100;
			$data['tot_tanpa_ktp']=$st->tanpa_ktp;
			$data['tanpa_ktp_persen']=$st->tanpa_ktp / $totalnya * 100;
			$data['tot_kontrak']=$st->kontrak;
			$data['kontrak_persen']=$st->kontrak / $totalnya * 100;
			
			
		}
		
		$pie=array(array('label'=>'Domisili KTP','data'=>$data['tot_ktp']),
					array('label'=>'Domisili Tanpa KTP','data'=>$data['tot_tanpa_ktp']),
					array('label'=>'Kontrak','data'=>$data['tot_kontrak']));
        $data['data_pie']		= $pie;
		
		$bar_W=array();
		$bar_P=array();
		$bar_L=array();
		$awal2=2;
		$awal1=1;
		$row_bar=$this->dashboard_model->get_data_bar();
		foreach($row_bar as $row){
			$bar_W[]=array($awal2,$row->nama_wilayah);
			$bar_P[]=array($awal2,$row->P);
			$bar_L[]=array($awal1,$row->L);
			$awal2=$awal2+3;
			$awal1=$awal1+3;
		}
		$data['data_W']		= $bar_W;
		$data['data_L']		= $bar_L;
		$data['data_P']		= $bar_P;
		// print_r($data['data_L']);exit();
        $data['ajax_url']		= 'admin/dashboard/getDataList'; 
        $data['breadcrum']  = array(
                                array("Dashboard",'#')
                              );
		
        $data = array_merge($data, backend_info());
        $this->parser->parse('admin/module_template', $data);
    }
	function getDataList()
    {
        $query = $this->dashboard_model->data_list(0);
		// print_r($query);exit();
        $this->output->set_output($query);
    }
}
