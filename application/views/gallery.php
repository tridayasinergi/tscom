<section class="section-padding bg-light-white browse-listing-gallery">
	<div class="container">
		<div class="section-header">
			<div class="section-heading">
				<h5 class="text-custom-blue">GALLERY</h5>
				<h3 class="text-theme fw-500">Kegiatan Tridaya Sinergi Indonesia</h3>
			</div>
			
		</div>
		<div class="row">
			<div class="col-12">
				<div class="browse-gallery">
					<div class="row">
						<?foreach($list_gallery as $row){?>
						<div class="col-md-4">
							<div class="gallery-img mb-sm-20">
								<a href="#">
									<img loading="lazy" src="<?=$gallery_path?><?=$row->image_path?>" class="img-fluid full-width" alt="image" style="width: 383px; height: 282px;">
								</a>
								<div class="img-overlay"></div>
								<div class="gallery-content text-center transform-center">
									<h6 class="text-custom-white fw-600 text-uppercase"><?=$row->nama_gallery?></h6>
									<span class="text-custom-white d-block mb-xl-20"><?=$row->keterangan?></span>
									<a href="{base_url}page/gallery_detail/<?=$row->id.'/'.strtolower(rename_url($row->nama_gallery))?>" class="btn-first btn-border"><?=$row->jml_foto?> Image</a>
								</div>
							</div>
						</div>
						<?}?>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<nav class="section-padding-top">
						{pagination}
				</nav>
			</div>
		</div>
	</div>
</section>