<?php

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        
		//permission_basic_admin($this->session);
		$this->load->model('home_model','home_model');
		$this->load->model('page_model','page_model');
		// $this->load->model('nautica_model','nautica_model');
		// $this->lang->load('home');
	}
	
	function index()
	{
		//print_r($this->lang->lang());exit();
        // $data = array();
		
		
		// $row=$this->page_model->get_content(12);
		// $data = array(
					// 'id' => $row->id,
					// 'title_in' => $row->title_in,
					// 'content_in' => $row->content_in,
					// 'author' => $row->author,
					// 'img' => $row->img,
					// );	
		$data['template'] = 'home';			
		$data['slider_list']=$this->home_model->slider_list();
		$data['gallery_list'] =$this->home_model->gallery_list();
		$data['index_page']=$this->home_model->get_index_page();
		$data['recent_post'] =$this->page_model->recent_post();
		$data['list_gallery']=$this->page_model->list_gallery(9);
		// print_r($data['recent_post']);exit();
		// $data['news_list'] =$this->home_model->news_list();
		
		
		// $data['why_list']=$this->home_model->why_list();
		// $data['client_list']=$this->home_model->client_list();
		// $data['project_list']=$this->home_model->project_list();
		// $data['team_list']=$this->home_model->team_list();
		// $data['news_list']=$this->home_model->news_list();
		// print_r($data);exit();
		$data['breadcrum'] = array(array("Portal ADMIN .COM",'home'),array("Dashboard",''));
	
		$data = array_merge($data,basic_info());
			// print_r(basic_info());exit();
		$this->parser->parse('index',$data);
		
	}	
    
    
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */