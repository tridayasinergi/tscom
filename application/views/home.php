<? if (count($flash_info)>0){ ?>
<div class="acme-news-ticker" style="z-index: 10;">
    <div class="acme-news-ticker-label">Flash Info :</div>
    <div class="acme-news-ticker-box">
        <ul class="my-news-ticker">
			<?foreach($flash_info as $row){?>
				<li><a href="<?=$row->url?>"><?=$row->content?></a></li>
			<?}?>
        </ul>
    </div>
	<!--
    <div class="acme-news-ticker-controls acme-news-ticker-vertical-controls">
	
        <button class="acme-news-ticker-arrow acme-news-ticker-prev"></button>
        <button class="acme-news-ticker-arrow acme-news-ticker-next"></button>
    </div> -->
</div>
<?}?>
<div class="with-sidebar-wrapper">
	<section id="content-section-1">
		<div class="greennature-full-size-wrapper gdlr-show-all no-skin" style="padding-bottom: 0px;  background-color: #ffffff; ">
			<div class="greennature-master-slider-item greennature-slider-item greennature-item" style="margin-bottom: 0px;">
				<!-- MasterSlider -->
				<div id="P_slider_1" class="master-slider-parent ms-parent-id-1">

					<!-- MasterSlider Main -->
					<div id="slider_1" class="master-slider ms-skin-default">
						<?foreach($slider_list as $r){?>
						
						<div class="ms-slide" data-delay="7" data-fill-mode="fill">
							<img loading="lazy" src="<?=$assets_path?>plugins/masterslider/public/assets/css/blank.gif" alt="" title="" data-src="<?=$slider_path?><?=$r->img?>" />
							<?if ($r->url){?>
							<a href="<?=$r->url?>" target="_self" class="ms-layer ms-btn ms-btn-round ms-btn-n msp-preset-btn-159" data-effect="t(true,n,n,-500,n,n,n,n,n,n,n,n,n,n,n)" data-duration="400" data-delay="987" data-ease="easeOutQuint" data-type="button" data-offset-x="1" data-offset-y="208" data-origin="ml" data-position="normal">Selengkapnya</a>
							<?}?>
							<?if ($r->kata3){?>
							<div class="ms-layer  msp-cn-1-3" style="" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="437" data-delay="625" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="105" data-origin="ml" data-position="normal">
								<?=$r->kata3?></div>
							<?}?>
							<?if ($r->kata1){?>
							<div class="ms-layer  msp-cn-1-2" style="" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="425" data-delay="325" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-5" data-origin="ml" data-position="normal">
								<?=$r->kata2?></div>
							<?}?>
							<?if ($r->kata1){?>
							<div class="ms-layer  msp-cn-1-1" style="" data-effect="t(true,150,n,n,n,n,n,n,n,n,n,n,n,n,n)" data-duration="350" data-ease="easeOutQuint" data-offset-x="0" data-offset-y="-100" data-origin="ml" data-position="normal">
								<?=$r->kata1?>
							</div>
							<?}?>
						</div>
						<?}?>

					</div>
					<!-- END MasterSlider Main -->

				</div>
				<!-- END MasterSlider -->


			</div>
		</div>
	</section>
</div>
<!-- End Banner -->
<!-- Intro -->
<section class="intro-service-3">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="intro-widget">
						<div class="intro-widget-wrapper">
							<div class="intro-widget-block">
								<?=$info1?>
							</div>
							<div class="intro-widget-shape"></div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-widget">
						<div class="intro-widget-wrapper">
							<div class="intro-widget-block">
								<?=$info2?>
							</div>
							<div class="intro-widget-shape"></div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-widget">
						<div class="intro-widget-wrapper">
							<div class="intro-widget-block">
								<?=$info3?>
							</div>
							<div class="intro-widget-shape"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<!-- Intro -->
<!-- Start About us -->
<section class="section-padding about-us bg-light-white">
	<div class="container">
		<div class="row">
			<div class="col-xl-7 col-lg-6 align-self-center mb-md-80">
				<div class="about-left-side">
					<div class="section-header">
						<div class="section-heading">
							<h2><?=$index_page['title']?></h2>
							
						</div>
					</div>
					<div class="about-desc">
						<?=$index_page['content']?>
					</div>					
					
				</div>
			</div>
			<div class="col-xl-5 col-lg-6">
				<div class="about-right-side">
					<img src="<?=$page_path.'/'.$index_page['img_pages']?>" class="img-fluid full-width" alt="about">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End About us -->
<!-- Start Recent Garden -->

<!-- End Recent Garden -->
<!-- Start Featured Garden -->
<section class="section-padding ">
	<div class="container">
		<div class="section-header">
			<div class="section-heading">
				<h5 class="text-custom-blue">Update</h5>
				<h3 class="text-theme fw-700">Informasi</h3>
			</div>
			<div class="section-description">
				<p class="text-light-white">Informasi Resmi dari PT. Tridaya Sinergi Indonesia<a class="btn-first btn-submit" href="{base_url}page/berita">Tampilkan Lebih Banyak <span class="flaticon-arrow-1"></span></a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="swiper-container featured-property-slider">
					<div class="swiper-wrapper">
					<?foreach($recent_post as $row){?>
						<div class="swiper-slide">
							<div class="property-grid-box">
								<div class="property-grid-wrapper">
									<div class="property-img animate-img">
										<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>">
											<img src="{berita_path}<?=$row->img_news?>" class="img-fluid full-width" alt="#" style="width: 383px; height: 200px;">
										</a>
										<div class="property-type">
											<a href="#" class="property-tag-2 bg-custom-blue text-custom-white"><?=tsi_date($row->created_at)?></a>
										</div>
										<div class="property-address bg-custom-blue">
											<a href="#"><i class="flaticon-pin"></i><?=$row->nama_kategori?></a>
										</div>
										
									</div>
									<div class="property-grid-caption padding-20">
										<h5><a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="text-theme"><?=strip_tags($row->title)?></a></h5>
										<p class="text-light-white no-margin"><?=substr(strip_tags($row->sinopsis),0,160)?></p>
									</div>
									<div class="property-grid-footer">
										<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="text-theme fs-14 fw-600">Selengkapnya <span class="flaticon-right-arrow ml-1"></span></a>										
									</div>
								</div>
							</div>
						</div>
					<?}?>
						
						
						
						
					</div>
				</div>
				<!-- Add Arrows -->
				<div class="slider-btn">
					<div class="featured-button-next swiper-button-next">
						<span class="flaticon-arrow-1"></span>
					</div>
					<div class="featured-button-prev swiper-button-prev">
						<span class="flaticon-left"></span>
					</div>
				</div>
				<!-- Add Pagination -->
				<div class="featured-pagination swiper-pagination"></div>
			</div>
		</div>
	</div>
</section>
<!-- End Featured Garden -->
<!-- Start For Sale -->
<section class="section-padding bg-light-white browse-listing-gallery">
	<div class="container">
		<div class="section-header">
			<div class="section-heading">
				<h5 class="text-custom-blue">Gallery</h5>
				<h3 class="text-theme fw-700">Update</h3>
			</div>
			<div class="section-description">
				<p class="text-light-white">Berikut adalah liputan kegiatan-kegiatan PT Tridaya Sinergi Indonesia. </p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="browse-gallery">
					<div class="row">
						<?foreach($list_gallery as $row){?>
						<div class="col-md-4">
							<div class="gallery-img mb-sm-20">
								<a href="#">
									<img loading="lazy" src="<?=$gallery_path?><?=$row->image_path?>" class="img-fluid full-width" alt="image" style="width: 383px; height: 282px;">
								</a>
								<div class="img-overlay"></div>
								<div class="gallery-content text-center transform-center">
									<h6 class="text-custom-white fw-600 text-uppercase"><?=$row->nama_gallery?></h6>
									<span class="text-custom-white d-block mb-xl-20"><?=$row->keterangan?></span>
									<a href="{base_url}page/gallery_detail/<?=$row->id.'/'.strtolower(rename_url($row->nama_gallery))?>" class="btn-first btn-border"><?=$row->jml_foto?> Image</a>
								</div>
							</div>
						</div>
						<?}?>
						
					</div>
				</div>
				<p class="text-center"><a class="btn-first btn-submit" href="{base_url}page/gallery">Tampilkan Lebih Banyak <span class="flaticon-arrow-1"></span></a></p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
			
			</div>
		</div>
	</div>
</section>
<!-- End For Sale -->
<!-- Start For Rent -->

<!-- End Agent -->
<!-- Start Blog -->

<!-- End Blog -->
<!-- Start Why choose us / testimonials-->
<section class="section-padding">
	<div class="container">
		<div class="row">
			<div class="col-xl-12 col-lg-12">
				<div class="why-choose-box mb-md-80">
					<div class="section-header">
						<div class="section-heading">
							<h5 class="text-custom-blue">TSI</h5>
							<h3 class="text-theme fw-700">Update</h3>
						</div>
						<div class="section-description">
							<p class="text-light-white">Berikut beberapa pengumuman penting yang harus diperhatikan oleh Mitra TSI</p>
						</div>
					</div>
					<div id="accordion" class="custom-accordion">
						<div class="card">
							<div class="card-header" id="headingOne">
								<button class="collapsebtn" data-toggle="collapse" data-target="#collapseOne">
								  SEGERA HADIR MP21.
								</button>
							</div>
							<div id="collapseOne" class="collapse show" data-parent="#accordion">
							  <div class="card-body">
								<p class="text-theme no-margin">
									PT Tridaya Sinergi Indonesia memiliki Marketing Plan yang disesuaikan dengan Regulasi Pemerintah. Kami tidak bertanggungjawab terhadap Sistem Lain yang dikembangkan oleh Member diluar Marketing Plan yang telah ditetapkan Perusahaan.
								</p>
							  </div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingTwo">
								<button class="collapsebtn collapsed" data-toggle="collapse" data-target="#collapseTwo">
								 ACARA RESMI TRIDAYA SINERGI
								</button>
							</div>
							<div id="collapseTwo" class="collapse" data-parent="#accordion">
							  <div class="card-body">
								<p class="text-theme no-margin">
									Setiap Penyelenggaraan Acara Resmi yang berkaitan dengan promosi, sosialisasi, motivasi atau sejenisnya yang menghadirkan KH. R. Abdul Malik dan atau Perwakilan Perusahaan, akan diumumkan secara resmi dan terbuka di website ini.
								</p>
							  </div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingThree">
								<button class="collapsebtn collapsed" data-toggle="collapse" data-target="#collapseThree">
								  PELATIHAN BERJENJANG TRIDAYA SINERGI
								</button>
							</div>
							<div id="collapseThree" class="collapse" data-parent="#accordion">
							  <div class="card-body">
								<p class="text-theme no-margin">
									PT Tridaya Sinergi Indonesia membuka Sinergi Institute sebagai Program Pelatihan Berjenjang bagi Mitra baik Stokis maupun Member TSI dengan ketentuan yang telah ditetapkan.
								</p>
							  </div>
							</div>
						</div>
						<div class="card">
							<div class="card-header" id="headingFour">
								<button class="collapsebtn collapsed" data-toggle="collapse" data-target="#collapseFour">
								  PROMOSI DISCLAIMER</button>
							</div>
								<div id="collapseFour" class="collapse" data-parent="#accordion">
								  <div class="card-body">
									<p>							
								Kami PT Tridaya Sinergi Indonesia, adalah Perusahaan Direct Selling Network Marketing bernomor Izin SIUPL : Nomor  197/SIPT/SIUPL/12/2020, yang memasarkan 14 (empat belas) variant SIN diantaranya : SIN Platinum TSI, SIN Kujang Mas TSI, SIN Provost 19 TSI, SIN Sinergi Mind, SIN Sinergi MIND Menthol,SIN Platinum Filter, SIN Kujang Mas Filter, SIN Sapu Jagat, SIN TRUST, SIN TRUST Menthol, SIN Krakatau, SIN New Normal Menthol, SIN New Normal Mind, SIN New Normal ORG.
								<br><br>Menyatakan bahwa aktifitas iklan dan promosi yang kami lakukan telah menyesuaikan dengan aturan iklan dan Peraturan Pemerintah Republik Indonesia Nomor 19 Tahun 2010 Tentang Pengamanan Rokok Bagi Kesehatan, dan telah di revisi menggunakan Peraturan Pemerintah Republik Indonesia Nomor 109 Tahun 2012 Tentang Pengamanan Bahan yang Mengandung Zat Adiktif Berupa Produk Tembakau bagi Kesehatan.
								<br><br>Adapun aktifitas pelanggaran iklan dan promosi yang dilakukan oleh konsumen produk 14 Variant SIN yang dipasarkan PT Tridaya Sinergi Indonesia dan khususnya  Member PT Tridaya Sinergi Indonesia adalah diluar tanggung jawab kami.
								<br><br>Demikian, atas perhatiannya kami ucapkan terima kasih.<br><br></p>
								<p><strong>Management TSI</strong></p>
							  </div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<!-- End Why choose us / testimonials -->
<!-- Start Cost estimate -->
<section class="section-padding parallax bg-estimate-cost">
	<div class="container">
		<div class="row justify-content-between">
		
			<div class="col-lg-6 align-text-center">
				<div class="estimate-text">
					<h2 class="text-custom-white fw-700">Kanal Youtube Resmi PT Tridaya Sinergi Indonesia</h2>
					<h1 class="text-custom-white fw-700">SINERGI TV</h1>
					<div class="btns">
						<a href="{per_youtube}" class="btn-first btn-submit btn-height">Link Youtube <span class="flaticon-arrow-1"></span></a>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Cost estimate -->
<!-- Start partners -->
<div id="myModal" class="modal" style="<?=$judul_info !='' ?'display:inline-flex;':'display:none;'?>z-index: 10;">

  <!-- Modal content -->
  <div class="modal-content" style="height:auto;width:auto;">
    <div class="modal-header">
      <h5><?=$judul_info?></h5>
      <span class="close">&times;</span>
    </div>
    <div class="modal-body">
		<div class="row">
		
		<div class="col-sm-12">
		<?=$content_info?>
	  
		</div>
	  </div>
      
    </div>
    
  </div>

</div>



<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="<?=$assets_path?>js/acmeticker.js"></script>
<script>
	var modal = document.getElementById("myModal");

window.onclick = function(event) {
  if (event.target == modal) {
	modal.style.display = "none";
  }
}
jQuery(document).ready(function ($) {
	$('.my-news-ticker').AcmeTicker({
		type:'vertical',/*vertical/horizontal/Marquee/type*/
		direction: 'right',/*up/down/left/right*/
		speed:600,/*true/false/number*/ /*For vertical/horizontal 600*//*For marquee 0.05*//*For typewriter 50*/
		controls: {
			prev: $('.acme-news-ticker-prev'),/*Can be used for vertical/horizontal/typewriter*//*not work for marquee*/
			next: $('.acme-news-ticker-next'),/*Can be used for vertical/horizontal/typewriter*//*not work for marquee*/
			toggle: $('.acme-news-ticker-pause')/*Can be used for vertical/horizontal/marquee/typewriter*/
		}
	});
})

function show(){
	// modal.style.display = "block";
	modal.style.display = "inline-flex;";
}
$(document).on("click",".close",function(){	
	modal.style.display = "none";
});
</script>