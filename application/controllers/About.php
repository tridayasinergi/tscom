<?php

class About extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//permission_basic_admin($this->session);
		$this->load->model('about_us_model','about_us_model');
		$this->load->model('home_model','home_model');
		$this->load->model('services_model','services_model');
		
	}
	function index(){
		$row = $this->about_us_model->get_about('about_us');
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['template'] = 'about_us';
			$data['menu'] = 'menu.we';
            $data['services_list']=$this->home_model->services_list();
			$data['client_list']=$this->home_model->client_list();
			
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
	}

	function services($id,$url){
		$row = $this->about_us_model->get_service_detail($id);
		if(isset($row->id)){
			$data = array(
					'title_in' => $row->title_in,
					'title_en' => $row->title_en,
					'img' => $row->img,
					'content_in' => $row->content_in,
					'content_en' => $row->content_en,
					);	
			// $data['header'] = ;
			$data['image_list'] = $this->services_model->get_services_gallery($id);
			$data['services_list']=$this->home_model->services_list($id);
			$data['template'] = 'service_detail';
			$data['menu'] = 'menu.we';
            // print_r($data);exit();
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}
		
	}
	function why_choose_us(){

			$data['template'] = 'why_us';
			$data['menu'] = 'menu.aboutwhy';
			$data['why_list'] = $this->about_us_model->why_list();
            $data['active'] = 'Why Choose us';
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		
		
	}
	function our_team($jabatan=0){

			$data['template'] = 'our_team';
			$data['menu'] = 'menu.aboutteam';
			$data['posisi_list'] = $this->about_us_model->posisi_list();
			$data['team_list'] = $this->about_us_model->team_list($jabatan);
            $data['id'] = $jabatan;
			if ($jabatan <> 0){
				$row=$this->about_us_model->get_jabatan($jabatan);			
				$data['jabatan_en'] = $row->jabatan_en ;
				$data['jabatan_in'] = $row->jabatan_in ;
			}
            $data['filter'] = $jabatan ;
            $data['active'] = 'Why Choose us';
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		
		
	}	
	function profile($id){
		$row = $this->about_us_model->get_profile($id);
		if(isset($row->id)){
			$data = array(
					'nama' => $row->nama,
					'general_in' => $row->general_in,
					'general_en' => $row->general_en,
					'img' => $row->img,
					'spesialis_in' => $row->spesialis_in,
					'spesialis_en' => $row->spesialis_en,
					'jabatan_in' => $row->jabatan_in,
					'jabatan_en' => $row->jabatan_en,
					);	
			// $data['header'] = ;
			$data['template'] = 'our_team_detail';
			$data['menu'] = 'menu.aboutteam';
            $data['list_team'] = $this->about_us_model->list_team($id);
			$data = array_merge($data,basic_info());
			$this->parser->parse('index',$data);
		
		}		
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */