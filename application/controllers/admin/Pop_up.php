<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pop_up extends CI_Controller {

   function __construct()
   {
   	 	parent::__construct();
		 if ( $this->session->userdata('logged_in')!=TRUE ) {
   	       redirect('admin/login');
      }
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<label>', '</label>');
		$this->load->model('pop_up_model','pop_up_model'); 
   }
	
	function index()
	{	
		// print_r('sini');exit();
		$data = array();
		$data['error'] 			= '';
		$data['title'] 			= 'Pop Up';
		$data['user_tipe_id'] 		= $this->session->userdata('user_tipe_id');
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("PAGES",'#'),
								array("List",'pop_up')
								);
		$data['template'] = 'admin/pop_up/index';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
		
	}
	function get_index(){
		//:content,:title,prioritas:,publish:publish,author:author,
		$content     		= $this->input->post('content');
		$title     		= $this->input->post('title');
		$prioritas     		= $this->input->post('prioritas');
		$publish     		= $this->input->post('publish');
		
		$where='';
		if ($content !=''){
			$where .=" AND content LIKE '%".$content."%'";
		}
		if ($title !=''){
			$where .=" AND judul LIKE '%".$title."%'";
		}
		if ($prioritas !='#'){
			$where .=" AND prioritas = '".$prioritas."'";
		}
		if ($publish !='#'){
			$where .=" AND publish = '".$publish."'";
		}
		
		$this->select = array();
		$this->join 	= array();
		$this->where  = array();
		
		
		$from="(SELECT *from pop_up WHERE id IS NOT NULL ".$where."
			) as T ORDER BY publish DESC,prioritas DESC,tgl_1 DESC";
		// print_r($from);exit(); 
		$this->order  = array();
		$this->group  = array();
		$this->from   = $from;

        $this->column_search   = array('title');
        $this->column_order    = array('title');

        $list = $this->datatable->get_datatables(true);
        $data = array();
        $no = $this->input->post('start');
		$url        = site_url('admin/pop_up/');
        foreach ($list as $r) {
            $no++;
            $row = array();
			$st_expired='';
			if ($r->tgl_2 < date('Y-m-d')){
				$st_expired=' <span class="label label-warning">EXPIRED</span>';
			}
			
			
            $row[] = $no;
            $row[] = $r->judul;
            $row[] =$this->status_pop_up($r->prioritas);
            $row[] = HumanDateShort($r->tgl_1);
            $row[] = HumanDateShort($r->tgl_2).$st_expired;
			if ($r->publish=='1'){
				$row[] = '<span class="label label-success">PUBLISHED</span>'.$st_expired;
			}else{
				$row[] = '<span class="label label-danger">UNPUBLISH</span>'.$st_expired;
			}
			$aksi       = '<div class="btn-group">';
			$aksi .= '<a href="'.$url.'editor/'.$r->id.'" type="button"  title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>';	
			if ($r->publish=='1'){			
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/0" type="button"  title="Hapus" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>';		
			}else{
				$aksi .= '<a href="'.$url.'update_status/'.$r->id.'/1" type="button"  title="Aktifkan" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Publish</a>';	
			}			
			$aksi.='</div>';
            $row[] = $aksi;
         
            
			$data[] = $row;
        }
        $output = array(
          "draw" => $this->input->post('draw'),
          "recordsTotal" => $this->datatable->count_all(true),
          "recordsFiltered" => $this->datatable->count_all(true),
          "data" => $data
        );
        echo json_encode($output);
    
	}
	function status_pop_up($id){
		$status='';
		if ($id=='1'){
			$status='<span class="label label-success">BIASA</span>';
		}elseif($id=='2'){
			$status='<span class="label label-primary">PENTING</span>';
		}elseif($id=='3'){
			$status='<span class="label label-danger">SANGAT PENTING</span>';
		}
		return $status;
	}
	function add_new(){
		$this->session->unset_userdata('searchkey');		
		$this->session->set_userdata('pop_up', 'Pop Up - Addnew');
		$data['id'] = "";
		$data['prioritas'] = "1";
		$data['judul'] = "";
		$data['content'] = "";
		// $data['created_at'] = "";
		$data['publish'] = "0";
		$data['updated_at'] = "";
		$data['error'] = "";
		$data['tgl_1'] = date('d-m-Y');
		$data['tgl_2'] = date('d-m-Y');
		$data['edit'] = "0";
		
		$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("TAMBAH",'#'),
								array("PAGES",'admin\pop_up'));
		$data['judul_page'] = 'Pop Up - Input Baru';
		$data['template'] = 'admin/pop_up/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	function simpan()
	{	
		$edit=$this->input->post('edit');
		$id=$this->input->post('id');
		// print_r($this->input->post());exit();
		if ($edit){
			// print_r($this->input->post('noid'));exit();
			
			if ($this->pop_up_model->update_record($id)){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');
				redirect("admin/pop_up/editor/$id/");		
				// redirect("admin/berita/editor/$id/");	
			}
		}else{		
				// print_r('SINI');exit();
			if ($this->pop_up_model->add_record()){
				$this->session->set_flashdata('confirm', true);
				$this->session->set_flashdata('message_flash', 'data telah disimpan.');				
				redirect("admin/pop_up/add_new/");
			}
			
		}
		
	}
	function failed_save($id,$image=false){
		// permission_user_logged_in($this->session);

		$data = $this->input->post();
		print_r($data);exit();
		$data['avatar'] = '';
		$data['error'] = validation_errors();
		$data['template'] = 'pop_up/manage';
		$data['list_tipe'] = $this->pop_up_model->list_tipe();
		$data['list_marketing'] = $this->tbooking_model->list_marketing();
		$data['list_operator'] = $this->tbooking_model->list_operator();
		$data['list_gedung'] = $this->tbooking_model->list_gedung();
		$data['list_wo'] = $this->tbooking_model->list_wo();
		if($image) $data['error'] .= $this->pop_up_model->error_message;
		if($id==''){
			$data['title'] = 'Add Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'pop_up',"point_right"),array("Input Baru",'#',"active"));
		}else{
			$data['title'] = 'Edit Users';
			$data['breadcrum'] = array(array("Master","#","frames"),array("pop_up",'pop_up',"point_right"),array("Edit User",'#',"active"));
		}

		$data = array_merge($data,backend_info());
		$this->parser->parse('module_template',$data);
	}
	function editor($id=0)
	{
		$this->session->unset_userdata('searchkey');
		$data=array();
		$this->session->set_userdata('pop_up', 'pop_up - Edit');
		$data=$this->pop_up_model->get_pop_up($id);
		$data['tgl_1'] = date_format(date_create($data['tgl_1']),'d-m-Y');	
		$data['tgl_2'] = date_format(date_create($data['tgl_2']),'d-m-Y');	
		if ($id){
			$data['breadcrum'] 	= array(
								array("ADMIN .COM",'#'),
								array("EDIT",'#'),
								array("PAGES",'admin\pop_up'));
		}else{
			$data['breadcrum'] = array(array("Master","#","frames"),array("User",'pop_up',"point_right"),array("Input Baru",'#',"active"));
		}
		$data['edit'] = '1';
		$data['error'] = '';
		$data['judul_page'] = 'PAGES - Edit';
		$data['template'] = 'admin/pop_up/manage';
		$data = array_merge($data, backend_info());
		$this->parser->parse('admin/module_template', $data);
	}
	
	function update_status($id,$status){
		
		if ($this->pop_up_model->update_status($id,$status)){
			$this->session->set_flashdata('confirm', true);
			$this->session->set_flashdata('message_flash', 'data telah diupdate.');
			redirect("admin/pop_up");
		}
	}
}
?>