<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>

<div class="block block-themed">
	<div class="block-header bg-smooth-dark">
		<ul class="block-options">
			<li>
				<a href="{base_url}admin/slider" class="btn"><i class="fa fa-reply"></i></a>
			</li>
		</ul>
		<h3 class="block-title"><i class="si si-users"></i> {title}</h3>
	</div>
	<div class="block-content block-content-narrow">
		<?php echo form_open_multipart('admin/slider/simpan','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
			
			
			<?php 
				if($img == '' ){ 
					$img='default.jpg';							
				}
				// $a=file_exists($slider_path.$img);
				// echo $a;
				if(file_exists('./assets/upload/slider/'.$img)) {
					
				}else{
					$img='default.jpg';							
				}
			?>
			<div class="form-group">
			<label class="col-md-2 control-label" for=""> </label>
				<div class="col-md-4">
					<img src="{slider_path}<?php echo $img; ?>" id="output_img" class="img-polaroid"  style="width:400px;height:200px;"/>
				</div>
			</div>
			<div class="form-group">
			<label class="col-md-2 control-label" for="img">Image </label>
				<div class="col-md-4">
					<input type="file" class="fileinput"  accept="image/*" onchange="loadFile_foto(event)" name="img" value="{img}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="title_in">Judul<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="title_in" id="title_in" placeholder="" value="{title_in}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kata1">Tulisan Baris ke-1<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="kata1" id="kata1" placeholder="" value="{kata1}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kata1">Tulisan Baris ke-2<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="kata2" id="kata2" placeholder="" value="{kata2}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kata3">Tulisan Baris ke-3<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="kata3" id="kata3" placeholder="" value="{kata3}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="kata3">Link Url<span class="text-danger">*</span></label>
				<div class="col-md-10">
					<input type="text" class="form-control input-sm" name="url" id="url" placeholder="" value="{url}" />
					
				</div>	
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label" for="urutan">Urutan Tampil<span class="text-danger">*</span></label>
				<div class="col-md-2">
					<input type="text" class="form-control input-sm" name="urutan" id="urutan" placeholder="" value="{urutan}" />
					
				</div>	
				
			</div>
			
			<div class="form-group">
				
			</div>
			<div class="form-group">
				<label class="col-md-2 control-label"></label>
				<div class="col-md-10">
					<button class="btn btn-success" type="submit" id="btn_simpan">Simpan</button>
					<a href="{base_url}admin/slider" class="btn btn-default" type="reset"><i class="pg-close"></i> Batal</a>
				</div>
			</div>
			<?php echo form_hidden('edit', $edit); ?>
			<?php echo form_hidden('id', $id); ?>
			
			<?php echo form_close() ?>
	</div>
</div>
<script type="text/javascript">
jQuery(function () {
	// Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
	App.initHelpers(['datepicker']);
	
});
var loadFile_foto = function(event) {
	var output_img = document.getElementById('output_img');
	output_img.src = URL.createObjectURL(event.target.files[0]);
  };
$(document).ready(function(){
	$('.number').number(true, 0);
	$('#user_tipe_id').select2();
	$(document).on("click","#btn_simpan",function(){
		// alert('KESNI');
		if(!validasi_save_final()) return false;		
		
		

		$("#form1").submit();
	});
});
</script>