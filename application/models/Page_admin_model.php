<?php

class Page_admin_model extends CI_Model {
	function __construct()
	{
		
   	}
	function data_list($baru=0)
    {
		
        $this->datatables->select('pages.*,kategori.kategori');
		$this->datatables->from('pages');
		$this->datatables->join('kategori','kategori.id=pages.kategori');
		
		$this->datatables->add_column('action', '', 'id');
        return $this->datatables->generate();
    }
	
	function add_record() 
	{

		// $this->urutan   		= $this->input->post('urutan'); 
		$this->title   	        = $this->input->post('title'); 
		$this->sinopsis   	    = $this->input->post('sinopsis'); 
		$this->str_key   	    = $this->get_key($this->input->post('kategori')); 
		
		$this->content   	    = replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	    = $this->input->post('kategori'); 
		$this->created_at   	= Format_YMD($this->input->post('created_at'));
		// $this->created_at   	= date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->publish   		= 0; 
		$this->created_by   	= $this->session->userdata('user_id');
		$this->author   		= $this->input->post('author');
		if($this->upload_img_pages(false)){ 
			$this->db->insert('pages', $this);
			return true;
		}else{
			// print_r('sini');
			return false;
		}
	}
	function get_key($kategori){
		$q="SELECT SUBSTR(kategori FROM 1 FOR 1) as str_key FROM kategori WHERE id='$kategori'";
		$row=$this->db->query($q)->row('str_key');
		if ($row){
			$huruf=strtolower($row."-");				
		}else{
			$huruf="z-";			
		}
		$q="SELECT max(`str_key`) as akhir FROM pages WHERE str_key LIKE '".$huruf."%'";
		$row=$this->db->query($q)->row('akhir');
		if ($row){
			$akhir=$row;		
		}else{
			$akhir=$huruf.'000';				
		}		
		$urutan = (int) substr($akhir, 2, 3) + 1;
		$key = $huruf . sprintf("%03s", $urutan);
		return $key;
		// print_r($key);exit();
	}
	function upload_img_pages ($update = false){
		// print_r($_FILES['img_pages']);exit();
		if(isset($_FILES['img_pages'])){
			if($_FILES['img_pages']['name'] != ''){
				// $config['upload_path'] = './assets/images/pages/original/';
				$config['upload_path'] = './assets/upload/pages/';
				$config['allowed_types'] = 'gif|jpg|png|bmp';
				$config['encrypt_name']  = FALSE;
				$config['overwrite']  = FALSE;
				// print_r($config);exit();
				// print_r($_FILES['img_pages']['name']);exit();
				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				$this->load->library('myimage');	
				
				if ($this->upload->do_upload('img_pages')){
					$image_upload = $this->upload->data();
					$this->img_pages  = $image_upload['file_name']; 
					// print_r($this);exit();
					// $this->myimage->load('./assets/upload/pages/'.$this->img_pages );			
					// $this->myimage->resize(1920,800);
					// $this->myimage->save('./assets/upload/pages/'.$this->img_pages );
					// $this->myimage->resize(798,370);
					// $this->myimage->save('./assets/upload/pages/thumbs/'.$this->img_pages );
					
					if($update == true) $this->remove_image($this->input->post('id'));
					
						return true;
				}else{
					$this->error_message = $this->upload->display_errors();
					print_r($this->error_message );exit();
					return false;
				}
			}else{
				// print_r('sini');
				return true;
			}
		}else{
			
				print_r('Sahabat');exit();
			return false;
		}
		
	}
	function remove_image($id){
		$row = $this->detail($id);
		// print_r($row->img_pages );exit();
		if(file_exists('./assets/upload/pages/'.$row->img_pages ) && $row->img_pages  !='') {
			unlink('./assets/upload/pages/'.$row->img_pages );
			// unlink('./assets/upload/pages/thumbs/'.$row->img_pages );
		}
		
	}
	function detail($id)
    {
		$this->db->where('id',$id);
		$query = $this->db->get('pages');
        return $query->row();
    }
	function update_record($id) 
	{	
		// print_r($this->input->post());exit();
		$id                 = $this->input->post('id'); 
		$this->title   	    = $this->input->post('title'); 
		$this->sinopsis   	= $this->input->post('sinopsis'); 
		$this->content   	= replace_image_url_asset($this->input->post('content',false)); 
		$this->kategori   	= $this->input->post('kategori'); 
		$this->author   	= $this->input->post('author');
		$this->created_at   = Format_YMD($this->input->post('created_at'));
		$this->updated_at   = date_format(date_create($this->input->post('created_at')),'Y-m-d');
		$this->updated_by   = $this->session->userdata('userid'); 
		// $this->author   		= $this->session->userdata('tipe_user'); 
		// print_r($this);exit();
		if($this->upload_img_pages (true)){ 
			$this->db->where('id',$id);
			$this->db->update('pages',$this);
			return true;
		}else{
			return false;
		}
	}
	function update_status($id,$status)
	{
		$this->db->where('id',$id);
		$this->db->update('pages',array('publish' => $status));
		return true;
	}
	
	function list_tipe()
	{
		$this->db->select('kategori.*');
		$this->db->from('kategori');
		// $this->db->where('kategori.id',$this->session->userdata('user_tipe_id'));
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());exit();
		return $query->result();
	}
	function list_author()
	{
		$this->db->select('mauthor.*');
		$this->db->from('mauthor');
		$this->db->where('publish',1);
		$this->db->order_by('author_ref','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	function get_pages($id)
	{
		$this->db->select('pages.*');
		$this->db->from('pages');
		$this->db->where('pages.id',$id);
		$query = $this->db->get();
		return $query->row();
	}
	
	function cek_pages($id)
	{
		$this->db->select('pages.*');
		$this->db->from('pages');
		$this->db->where('pages.username',$id);
		$this->db->where('pages.publish',1);
		$query = $this->db->get();
		$a=$query->row();
		if ($a){
			return false;
		}else{
			return true;
		}
		
	}
	
}
