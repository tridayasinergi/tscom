
<section class="section-padding bg-light-white our_articles">
        <div class="container">
			<div class="section-header">
				<div class="section-heading">
					<h5 class="text-custom-blue">TESTIMONI</h5>
					<h3 class="text-theme fw-500">Chanel Youtube </h3>
				</div>
				
			</div>
            <div class="row">
                <?foreach($list_gallery as $row){?>
                <!-- article -->
                <article class="post col-lg-6 col-md-12 mb-xl-20">
                    <div class="post-wrapper">
                        <div class="blog-img animate-img">
                           <video class="video-js vjs-theme-fantasy vjs-fluid" style="height:326px" data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "<?=$row->link?>"}], "youtube": { "iv_load_policy": 1 } }' controls></video>
                            <div class="post-date">
                                <a href="blog-single.html"><?=tsi_date($row->created_at)?></a>
                            </div>
                            
                        </div>
                        <div class="blog-meta bg-custom-white padding-20">
                            <h2 class="post-title"><a href="<?=$row->link?>" class="text-theme"><?=$row->title?></a></h2>
                            <p class="text-light-white no-margin"><?=substr(strip_tags($row->content),0,160)?>...</p>
                        </div>
                        <div class="blog-footer-meta bg-custom-white padding-20">
                            <div class="post-author">
                                <div class="author-img">
                                    <a href="blog-single.html">
                                        <img src="{assets_path}tsi.png" class="rounded-circle" alt="#">
                                    </a>
                                </div>
                                <span class="text-theme fs-14">By <a href="#" class="text-theme fw-500"><?=$row->author_ref?></a></span>
                            </div>
                            <div class="post-link">
                                <a href="<?=replace_watch($row->link)?>" target="_blank" class="link-btn text-theme fw-600 fs-14">Detail</a>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- article -->
               <?}?>
                <!-- article -->
            </div>
			<div class="row">
				<div class="col-12">
					<nav class="section-padding-top">
							{pagination}
					</nav>
				</div>
			</div>
        </div>
    </section>