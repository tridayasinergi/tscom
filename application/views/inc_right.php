<aside class="col-lg-4">
	<div class="sidebar_wrap mb-md-80">
		<div class="sidebar">
			<div class="sidebar_widgets mb-xl-30">
				<div class="widget_title">
					<h5 class="no-margin text-theme">Search <?=$kategori_nama?> <?=($cari_text!=''?' - '.$cari_text : '')?></h5>
				</div>
			   <?php echo form_open('page/berita/{kategori}','class="js-validation-bootstrap  form-horizontal push-10-t"') ?>
					<div class="form-group" >
						<input type="hidden" name="kategori" class="form-control form-control-custom" value="{kategori}" placeholder="keywords">
						<input type="text" name="cari_text" class="form-control form-control-custom" placeholder="keywords" value="{cari_text}">
					</div>
					<button type="submit" class="btn-first btn-submit full-width">Search</button>
				</form>
			</div>
			<div class="sidebar_widgets mb-xl-30">
				<div class="widget_title">
					<h5 class="no-margin text-theme">Top Kategori</h5>
				</div>
				<ul class="custom categories">
					<? 
					$total=0;
					foreach($top_kategori as $row) {
						$total +=$row->jml;
						?>
					<li>
						<a href="{base_url}page/berita/<?=$row->id?>" class="text-theme fs-14"><?=$row->kategori?> <span class="text-light-white">(<?=$row->jml?>)</span></a>
					</li>
					<?}?>
					<li>
						<a href="{base_url}page/berita/all" class="text-theme fs-14">All <span class="text-light-white">(<?=$total?>)</span></a>
					</li>
				</ul>
			</div>
			<div class="sidebar_widgets mb-xl-30">
				<div class="widget_title">
					<h5 class="no-margin text-theme">Popular Post</h5>
				</div>
				<ul class="custom popular_post">
					<? foreach($top_populer as $row) {?>
					<li class="mb-xl-20">
						<div class="post">
							<div class="post-wrapper">
								<div class="popular_post_img animate-img">
									<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>">
										<img src="{berita_path}thumbs/<?=$row->img_news?>" class="img-fluid image-fit" alt="#">
									</a>
								</div>
								<div class="popular_post_title">
									<h6><a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="text-custom-black"><?=$row->title?></a></h6>
									<span class="date text-custom-blue fw-600 fs-14"><?=tsi_date($row->created_at)?></span>
								</div>
							</div>
						</div>
					</li>
					<?}?>
					
				</ul>
			</div>
			<div class="sidebar_widgets mb-xl-30">
				<div class="widget_title">
					<h5 class="no-margin text-theme">Berita Terbaru</h5>
				</div>
				<ul class="custom popular_post">
					<? foreach($recent_post as $row) {?>
					<li class="mb-xl-20">
						<div class="post">
							<div class="post-wrapper">
								<div class="popular_post_img animate-img">
									<a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>">
										<img src="{berita_path}thumbs/<?=$row->img_news?>" class="img-fluid image-fit" alt="#">
									</a>
								</div>
								<div class="popular_post_title">
									<h6><a href="{base_url}page/berita_detail/<?= strtolower(substr(rename_url($row->title),0,1)).'-'.$row->id.'/'.strtolower(rename_url($row->title))?>" class="text-custom-black"><?=$row->title?></a></h6>
									<span class="date text-custom-blue fw-600 fs-14"><?=tsi_date($row->created_at)?></span>
								</div>
							</div>
						</div>
					</li>
					<?}?>
					  
				</ul>
			</div>
			<div class="sidebar_widgets">
				<div class="widget_title">
					<h5 class="no-margin text-theme">Tags</h5>
				</div>
				<div class="tags">
					<?foreach ($list_all_tag as $r){?>
						<a href="#"><?=$r->tag?></a>
					<?}?>
					
				</div>
			</div>
		</div>
	</div>
</aside>